#include "lxw2.h"
#include "ffg.h"
#include "xd2.h"
#include "yasb.h"
#include <iostream>
#include <memory>
#include <fstream>
#include <stdio.h>
#include <sstream>

#define CLR_DEF "\e[0m"
#define CLR_TXT "\e[0;37m"
#define CLR_UNR "\e[0;90m"
#define CLR_SRC "\e[0;36m"
#define CLR_OK  "\e[0;32m"
#define CLR_ERR "\e[0;31m"
#define CLR_MSS "\e[0;35m"

enum class DataSrc : uint8_t{
  NONE = 0,
  FFG  = 1,
  XD2  = 2,
  YASB = 4,
  ALL  = 7
};
DataSrc operator| (const DataSrc& a, const DataSrc& b) { return static_cast<DataSrc>(static_cast<uint8_t>(a) | static_cast<uint8_t>(b)); }
DataSrc operator|=(      DataSrc& a, const DataSrc& b) { a = a | b; return a; }
bool    operator&&(const DataSrc& a, const DataSrc& b) { return (static_cast<uint8_t>(a) & static_cast<uint8_t>(b)) > 0; }

struct XDVOptions {
  bool includeUnreleased;
  bool printAll;
  bool diff;
  bool html;
  DataSrc sources;
};



// ***
// *** PrintResults() - text output 
// ***
void PrintResults(const std::unique_ptr<xdv::lxw2::LXW2DS>& mine, const std::vector<std::unique_ptr<xdv::DataSrc>>& theirs, const XDVOptions& opts) {
  printf("*** Ships ***\n");
  // for each ship...
  for(const std::pair<const libxwing2::Shp, xdv::DataPoints>& pShp : mine->GetShps()) {
    bool hasError = false;
    libxwing2::Shp shp = pShp.first;
    // no unrelease filtering on ships (if a ship only has pilots from a conversion kit, is it 'released'?)
    std::stringstream ss;
    ss << CLR_TXT << libxwing2::Ship::GetShip(shp).GetName() << "... ";
    // for each data source...
    for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
      ss << CLR_SRC << ds->GetName() << ":";
      if(ds->GetShpDps().size() == 0) {
	ss << CLR_UNR << "[NI]";
      } else {
	if(!ds->Has(shp)) {
	  hasError = true;
	  ss << CLR_MSS << "[Missing]";
	} else {
	  // for each data point...
	  bool dsHasError = false;
	  for(const std::pair<const xdv::DP, std::string>& pDp : pShp.second) {
	    xdv::DP dp = pDp.first;
	    std::string mVal = pDp.second;
	    if(ds->GetDp(shp, dp)) {
	      std::string tVal = *(ds->GetDp(shp, dp));
	      if(mVal != tVal) {
		dsHasError = true;
		hasError = true;
		// mismatch
		if(opts.diff) {
		  ss << CLR_ERR << std::endl <<"["<< GetStr(dp) <<"]"<< std::endl <<" M: '"<< mVal <<"'"<< std::endl <<" T: '"<< tVal <<"'"<< std::endl;
		} else {
		  ss << CLR_ERR << "[" << GetStr(dp) << "]";
		}
	      }
	    }
	  }
	  if(!dsHasError) {
	    ss << CLR_OK << "OK";
	  }
	}
      }
      ss << "  ";
    }
    if(hasError || opts.printAll) {
      std::cout << ss.str() << std::endl;
    }
  }

  printf("\n%s", CLR_DEF);
  printf("*** Pilots ***\n");
  // for each pilot...
  for(const std::pair<const libxwing2::Plt, xdv::DataPoints>& pPlt : mine->GetPlts()) {
    bool hasError = false;
    libxwing2::Plt plt = pPlt.first;
    if(!opts.includeUnreleased && libxwing2::Pilot::GetPilot(plt).IsUnreleased()) { continue; }
    std::stringstream ss;
    {
      libxwing2::Pilot pilot = libxwing2::Pilot::GetPilot(plt);
      ss << (pilot.IsUnreleased() ? CLR_UNR : CLR_TXT) << "[" << pilot.GetShip().GetName() << "] " << pilot.GetName() << "... ";
    }
    // for each data source...
    for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
      ss << CLR_SRC << ds->GetName() << ":";
      if(ds->GetPltDps().size() == 0) {
	ss << CLR_UNR << "[NI]";
      } else {
	if(!ds->Has(plt)) {
	  hasError = true;
	  ss << CLR_MSS << "[Missing]";
	} else {
	  // for each data point...
	  bool dsHasError = false;
	  for(const std::pair<const xdv::DP, std::string>& pDp : pPlt.second) {
	    xdv::DP dp = pDp.first;
	    std::string mVal = pDp.second;
	    if(ds->GetDp(plt, dp)) {
	      std::string tVal = *(ds->GetDp(plt, dp));
	      if(mVal != tVal) {
		dsHasError = true;
		hasError = true;
		// mismatch
		if(opts.diff) {
		  ss << CLR_ERR << std::endl <<"["<< GetStr(dp) <<"]"<< std::endl <<" M: '"<< mVal <<"'"<< std::endl <<" T: '"<< tVal <<"'"<< std::endl;
		} else {
		  ss << CLR_ERR << "[" << GetStr(dp) << "]";
		}
	      }
	    }
	  }
	  if(!dsHasError) {
	    ss << CLR_OK << "OK";
	  }
	}
      }
      ss << "  ";
    }
    if(hasError || opts.printAll) {
      std::cout << ss.str() << std::endl;
    }
  }

  printf("\n%s", CLR_DEF);
  printf("*** Upgrades ***\n");
  // for each upgrade...
  for(const std::pair<const libxwing2::Upg, xdv::DataPoints>& pUpg : mine->GetUpgs()) {
    bool hasError = false;
    libxwing2::Upg upg = pUpg.first;
    if(!opts.includeUnreleased && libxwing2::Upgrade::GetUpgrade(upg).IsUnreleased()) { continue; }
    std::stringstream ss;
    ss << (libxwing2::Upgrade::GetUpgrade(upg).IsUnreleased() ? CLR_UNR : CLR_TXT) << "[" << libxwing2::Upgrade::GetUpgrade(upg).GetUpgradeType().GetShortName() << "] " << libxwing2::Upgrade::GetUpgrade(upg).GetName() << "... ";
    // for each data source...
    for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
      ss << CLR_SRC << ds->GetName() << ":";
      if(ds->GetUpgDps().size() == 0) {
	ss << CLR_UNR << "[NI]";
      } else {
	if(!ds->Has(upg)) {
	  hasError = true;
	  ss << CLR_MSS << "[Missing]";
	} else {
	  // for each data point...
	  bool dsHasError = false;
	  for(const std::pair<const xdv::DP, std::string>& pDp : pUpg.second) {
	    xdv::DP dp = pDp.first;
	    std::string mVal = pDp.second;
	    if(ds->GetDp(upg, dp)) {
	      std::string tVal = *(ds->GetDp(upg, dp));
	      if(mVal != tVal) {
		dsHasError = true;
		hasError = true;
		// mismatch
		if(opts.diff) {
		  ss << CLR_ERR << std::endl <<"["<< GetStr(dp) <<"]"<< std::endl <<" M: '"<< mVal <<"'"<< std::endl <<" T: '"<< tVal <<"'"<< std::endl;
		} else {
		  ss << CLR_ERR << "[" << GetStr(dp) << "]";
		}
	      }
	    }
	  }
	  if(!dsHasError) {
	    ss << CLR_OK << "OK";
	  }
	}
      }
      ss << "  ";
    }
    if(hasError || opts.printAll) {
      std::cout << ss.str() << std::endl;
    }
  }
#ifdef INC_QUICKBUILDS
  printf("\n%s", CLR_DEF);
  printf("*** QuickBuilds ***\n");
  // for each quickbuild...
  for(const std::pair<const libxwing2::QB, xdv::DataPoints>& pQB : mine->GetQBs()) {
    bool hasError = false;
    libxwing2::QB qb = pQB.first;
    std::stringstream ss;
    ss << CLR_TXT << libxwing2::QuickBuild::GetQuickBuild(qb).GetName() << "... ";
    // for each data source...
    for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
      ss << CLR_SRC << ds->GetName() << ":";
      if(ds->GetQbDps().size() == 0) {
	ss << CLR_UNR << "[NI]";
      } else {
	if(!ds->Has(qb)) {
	  hasError = true;
	  ss << CLR_MSS << "[Missing]";
	} else {
	  // for each data point...
	  bool dsHasError = false;
	  for(const std::pair<const xdv::DP, std::string>& pDp : pQB.second) {
	    xdv::DP dp = pDp.first;
	    std::string mVal = pDp.second;
	    if(ds->GetDp(qb, dp)) {
	      std::string tVal = *(ds->GetDp(qb, dp));
	      if(mVal != tVal) {
		dsHasError = true;
		hasError = true;
		// mismatch
		if(opts.diff) {
		  ss << CLR_ERR << std::endl <<"["<< GetStr(dp) <<"]"<< std::endl <<" M: '"<< mVal <<"'"<< std::endl <<" T: '"<< tVal <<"'"<< std::endl;
		} else {
		  ss << CLR_ERR << "[" << GetStr(dp) << "]";
		}
	      }
	    }
	  }
	  if(!dsHasError) {
	    ss << CLR_OK << "OK";
	  }
	}
      }
      ss << "  ";
    }
    if(hasError || opts.printAll) {
      std::cout << ss.str() << std::endl;
    }
  }
#endif
}



// ***
// *** CreateHtmlResults() - html output 
// ***
#define HTML_OK()        "<td class=\"ok\">OK</td>"
#define HTML_NI()        "<td class=\"ni\" title=\"Not Implemented\">NI</td>"
#define HTML_MI(missing) "<td class=\"er\" title=\"" << missing << " Not Found\">MISSING</td>"
#define HTML_ER(info)    "<td class=\"er\" title=\"" << info << "\">ERROR</td>"
#define HTML_IS(i, u, t) "<td class=\"" << (i ? "er" : "ok") << "\" title=\"" << i << " " << t << " with issues (" << std::to_string(u) << " unreleased with issues)" <<  "\">" << i << " (" << u << ")</td>"

void CreateHtmlResults(const std::unique_ptr<xdv::lxw2::LXW2DS>& mine, const std::vector<std::unique_ptr<xdv::DataSrc>>& theirs, const XDVOptions& opts) {
  // build each section in a separate strstream so we can count issues
  // then combine them at the end
  std::map<std::string, std::pair<int,int>> shipIssues;
  std::map<std::string, std::pair<int,int>> pilotIssues;
  std::map<std::string, std::pair<int,int>> upgradeIssues;
  std::map<std::string, std::pair<int,int>> quickbuildIssues;

  // *** ships ***
  std::string shipInfo;
  {
    std::stringstream si;
    si << "<a name=\"ships\">Ships</a>" << std::endl;
    si << "<table>" << std::endl;
    si << "<tr>" << std::endl;
    si << "<th>Ship</th>" << std::endl;
    for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
      si << "<th>" << ds->GetName() << "</th>" << std::endl;    
    }
    si << "</tr>" << std::endl;
    // for each ship
    for(const std::pair<const libxwing2::Shp, xdv::DataPoints>& pShp : mine->GetShps()) {
      libxwing2::Ship ship = libxwing2::Ship::GetShip(pShp.first);
      si << "<tr style=\"background-color:"  "\">" << std::endl;
      si << "<td>" << ship.GetName() << "</td>" << std::endl;
      // for each data source...
      for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
	if(ds->GetShpDps().size() == 0) {
	  si << HTML_NI() << std::endl;
	  shipIssues[ds->GetName()] = {-1,-1};
	} else {
	  if(!ds->Has(ship.GetShp())) {
	    si << HTML_MI("Ship") << std::endl;
	    shipIssues[ds->GetName()].first++;
	  } else {
	    // for each data point...
	    std::stringstream ss;
	    for(const std::pair<const xdv::DP, std::string>& pDp : pShp.second) {
	      xdv::DP dp = pDp.first;
	      std::string mVal = pDp.second;
	      if(ds->GetDp(ship.GetShp(), dp)) {
		std::string tVal = *(ds->GetDp(ship.GetShp(), dp));
		if(mVal != tVal) {
		  ss << "["<< GetStr(dp) <<"]"<< std::endl <<" M: '"<< mVal <<"'"<< std::endl <<" T: '"<< tVal <<"'"<< std::endl;
		}
	      }
	    }
	    std::string issues = ss.str();
	    if(issues.size() == 0) {
	      si << HTML_OK() << std::endl;
	    } else {
	      si << HTML_ER(issues) << std::endl;
	      shipIssues[ds->GetName()].first++;
	    }
	  }
	}
      }
      si << "</tr>" << std::endl;
    }
    si << "</table>" << std::endl;
    shipInfo = si.str();
  }

  // *** pilots ***
  std::string pilotInfo;
  {
    std::stringstream pi;
    pi << "<a name=\"pilots\">Pilots</a>" << std::endl;
    pi << "<table>" << std::endl;
    pi << "<tr>" << std::endl;
    pi << "<th>Pilot</th>" << std::endl;
    for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
      pi << "<th>" << ds->GetName() << "</th>" << std::endl;    
    }
    pi << "</tr>" << std::endl;
    // for each pilot
    for(const std::pair<const libxwing2::Plt, xdv::DataPoints>& pPlt : mine->GetPlts()) {
      libxwing2::Pilot pilot = libxwing2::Pilot::GetPilot(pPlt.first);
      pi << "<tr class=\"" << (pilot.IsUnreleased() ? "unr" : "rel") << "\">" << std::endl;
      pi << "<td>[" <<pilot.GetShip().GetDialId() << "] "  << pilot.GetName() << "</td>" << std::endl;
      // for each data source...
      for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
	if(ds->GetPltDps().size() == 0) {
	  pi << HTML_NI() << std::endl;
	  pilotIssues[ds->GetName()] = {-1,-1};
	} else {
	  if(!ds->Has(pilot.GetPlt())) {
	    pi << HTML_MI("Pilot") << std::endl;
	    pilot.IsUnreleased() ? pilotIssues[ds->GetName()].second++ : pilotIssues[ds->GetName()].first++;
	  } else {
	    // for each data point...
	    std::stringstream ss;
	    for(const std::pair<const xdv::DP, std::string>& pDp : pPlt.second) {
	      xdv::DP dp = pDp.first;
	      std::string mVal = pDp.second;
	      if(ds->GetDp(pilot.GetPlt(), dp)) {
		std::string tVal = *(ds->GetDp(pilot.GetPlt(), dp));
		if(mVal != tVal) {
		  ss << "["<< GetStr(dp) <<"]"<< std::endl <<" M: '"<< mVal <<"'"<< std::endl <<" T: '"<< tVal <<"'"<< std::endl;
		}
	      }
	    }
	    std::string issues = ss.str();
	    if(issues.size() == 0) {
	      pi << HTML_OK() << std::endl;
	    } else {
	      pi << HTML_ER(issues) << std::endl;
	      pilot.IsUnreleased() ? pilotIssues[ds->GetName()].second++ : pilotIssues[ds->GetName()].first++;
	    }
	  }
	}
      }
      pi << "</tr>" << std::endl;
    }
    pi << "</table>" << std::endl;
    pilotInfo = pi.str();
  }

  // *** upgrades ***
  std::string upgradeInfo;
  {
    std::stringstream ui;
    ui << "<a name=\"upgrades\">Upgrades</a>" << std::endl;
    ui << "<table>" << std::endl;
    ui << "<tr>" << std::endl;
    ui << "<th>Upgrade</th>" << std::endl;
    for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
      ui << "<th>" << ds->GetName() << "</th>" << std::endl;    
    }
    ui << "</tr>" << std::endl;
    // for each upgrade
    for(const std::pair<const libxwing2::Upg, xdv::DataPoints>& pUpg : mine->GetUpgs()) {
      libxwing2::Upgrade upgrade = libxwing2::Upgrade::GetUpgrade(pUpg.first);
      ui << "<tr class=\"" << (upgrade.IsUnreleased() ? "unr" : "rel") << "\">" << std::endl;
      ui << "<td>"  << upgrade.GetName() << "</td>" << std::endl;
      // for each data source...
      for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
	if(ds->GetUpgDps().size() == 0) {
	  ui << HTML_NI() << std::endl;
	  upgradeIssues[ds->GetName()] = {-1,-1};
	} else {
	  if(!ds->Has(upgrade.GetUpg())) {
	    ui << HTML_MI("Upgrade") << std::endl;
	    upgrade.IsUnreleased() ? upgradeIssues[ds->GetName()].second++ : upgradeIssues[ds->GetName()].first++;
	  } else {
	    // for each data point...
	    std::stringstream ss;
	    for(const std::pair<const xdv::DP, std::string>& pDp : pUpg.second) {
	      xdv::DP dp = pDp.first;
	      std::string mVal = pDp.second;
	      if(ds->GetDp(upgrade.GetUpg(), dp)) {
		std::string tVal = *(ds->GetDp(upgrade.GetUpg(), dp));
		if(mVal != tVal) {
		  ss << "["<< GetStr(dp) <<"]"<< std::endl <<" M: '"<< mVal <<"'"<< std::endl <<" T: '"<< tVal <<"'"<< std::endl;
		}
	      }
	    }
	    std::string issues = ss.str();
	    if(issues.size() == 0) {
	      ui << HTML_OK() << std::endl;
	    } else {
	      ui << HTML_ER(issues) << std::endl;
	      upgrade.IsUnreleased() ? upgradeIssues[ds->GetName()].second++ : upgradeIssues[ds->GetName()].first++;
	    }
	  }
	}
      }
      ui << "</tr>" << std::endl;
    }
    ui << "</table>" << std::endl;
    upgradeInfo = ui.str();
  }
#ifdef INC_QUICKBUILDS
  // *** quickbuilds ***
  std::string quickbuildInfo;
  {
    std::stringstream qi;
    qi << "<a name=\"quickbuilds\">QuickBuilds</a>" << std::endl;
    qi << "<table>" << std::endl;
    qi << "<tr>" << std::endl;
    qi << "<th>QuickBuilds</th>" << std::endl;
    for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
      qi << "<th>" << ds->GetName() << "</th>" << std::endl;    
    }
    qi << "</tr>" << std::endl;
    // for each quickbuild
    for(const std::pair<const libxwing2::QB, xdv::DataPoints>& pQB : mine->GetQBs()) {
      libxwing2::QuickBuild quickbuild = libxwing2::QuickBuild::GetQuickBuild(pQB.first);
      qi << "<tr>" << std::endl;
      qi << "<td>" << quickbuild.GetName() << "</td>" << std::endl;
      // for each data source...
      for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
	if(ds->GetQbDps().size() == 0) {
	  qi << HTML_NI() << std::endl;
	  quickbuildIssues[ds->GetName()] = {-1,-1};
	} else {
	  if(!ds->Has(quickbuild.GetQB())) {
	    qi << HTML_MI("QuickBuild") << std::endl;
	    quickbuildIssues[ds->GetName()].first++;
	  } else {
	    // for each data point...
	    std::stringstream ss;
	    for(const std::pair<const xdv::DP, std::string>& pDp : pQB.second) {
	      xdv::DP dp = pDp.first;
	      std::string mVal = pDp.second;
	      if(ds->GetDp(quickbuild.GetQB(), dp)) {
		std::string tVal = *(ds->GetDp(quickbuild.GetQB(), dp));
		if(mVal != tVal) {
		  ss << "["<< GetStr(dp) <<"]"<< std::endl <<" M: '"<< mVal <<"'"<< std::endl <<" T: '"<< tVal <<"'"<< std::endl;
		}
	      }
	    }
	    std::string issues = ss.str();
	    if(issues.size() == 0) {
	      qi << HTML_OK() << std::endl;
	    } else {
	      qi << HTML_ER(issues) << std::endl;
	      quickbuildIssues[ds->GetName()].first++;
	    }
	  }
	}
      }
      qi << "</tr>" << std::endl;
    }
    qi << "</table>" << std::endl;
    quickbuildInfo = qi.str();
  }
#endif

  // *** make the document ***  
  std::ofstream f("./xdv.html");
  // start
  f << "<!DOCTYPE html>" << std::endl;
  f << "<html>" << std::endl;
  // head
  f << "<head>" << std::endl;
  f << "<title>xdv results</title>" << std::endl;
  f << "<style>" << std::endl;
  // css
  f << "body  { background-color:black; color:white; }" << std::endl;
  f << "table { background-color:black; border-collapse:collapse; border:1px solid white; color:white; margin:5px; }" << std::endl;
  f << "th    { background-color:green; color:black; border:1px solid darkgreen; }" << std::endl;
  f << "td    { border: 1px solid darkgreen; }" << std::endl;
  f << "tr    { background-color:#001100; }" << std::endl;
  f << "hr    { border: none; border-top: 1px dashed #004400; width: 600px; margin-left: 0px; }" << std::endl;
  f << "a:link    { color: white; }" << std::endl;
  f << "a:visited { color: grey; }" << std::endl;
  f << ".rel { background-color:#001100;}" << std::endl;
  f << ".unr { background-color:#330000; color:#AAAAAA; }" << std::endl;
  f << ".ok  { text-align:center; color:#00FF00; }" << std::endl;
  f << ".ni  { text-align:center; color:#00AAAA; }" << std::endl;
  f << ".er  { text-align:center; color:#FF0000; }" << std::endl;
  f << "</style>" << std::endl;
  f << "</head>" << std::endl;
  // body
  f << "<body>" << std::endl;
  // header
  f << "<p>Generated by <a href=\"https://gitlab.com/sirjorj/xdv\">xdv<a> on " << __DATE__ << " @ " << __TIME__ << " using:</p>" << std::endl;
  f << "<ul>" << std::endl;
  f << "<li>" << mine->GetName() << " (" << mine->GetVersion() << ")" << "</li>" << std::endl;
  for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
    f << "<li>" << ds->GetName() << " (" << ds->GetVersion() << ")" << "</li>" << std::endl;
  }
  f << "</ul>" << std::endl;
  f << "<p>Mouse over errors to see the discrepancies.  'M:' is mine (lxw2), 'T:' is theirs (see column).</p>" << std::endl;

  // *** index ***
  f << "<table>" << std::endl;
  f << "<tr>" << std::endl;
  f << "<th>Section</th>" << std::endl;
  for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
    f << "<th>" << ds->GetName() << "</th>" << std::endl;    
  }
  // ships
  f << "<tr>" << std::endl;
  f << "<td><a href=\"#ships\">Ships</a></td>" << std::endl;
  for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
    if(shipIssues[ds->GetName()].first == -1 && shipIssues[ds->GetName()].second == -1) {
      f << HTML_NI() << std::endl;
    }
    else {
      f << HTML_IS(shipIssues[ds->GetName()].first, shipIssues[ds->GetName()].second, "ships") << std::endl;
    }
  }
  f << "</tr>" << std::endl;
  // pilots
  f << "<tr>" << std::endl;
  f << "<td><a href=\"#pilots\">Pilots</a></td>" << std::endl;
  for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
    if(pilotIssues[ds->GetName()].first == -1 && pilotIssues[ds->GetName()].second == -1) {
      f << HTML_NI() << std::endl;
    }
    else {
      f << HTML_IS(pilotIssues[ds->GetName()].first, pilotIssues[ds->GetName()].second, "pilots") << std::endl;
    }
  }
  f << "</tr>" << std::endl;
  // upgrades
  f << "<tr>" << std::endl;
  f << "<td><a href=\"#upgrades\">Upgrades</a></td>" << std::endl;
  for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
    if(upgradeIssues[ds->GetName()].first == -1 && upgradeIssues[ds->GetName()].second == -1) {
      f << HTML_NI() << std::endl;
    }
    else {
      f << HTML_IS(upgradeIssues[ds->GetName()].first, upgradeIssues[ds->GetName()].second, "upgrades") << std::endl;
    }
  }
  f << "</tr>" << std::endl;
#ifdef INC_QUICKBUILDS
  // quickbuilds
  f << "<tr>" << std::endl;
  f << "<td><a href=\"#quickbuilds\">QuickBuilds</a></td>" << std::endl;
  for(const std::unique_ptr<xdv::DataSrc>& ds : theirs) {
    if(quickbuildIssues[ds->GetName()].first == -1) {
      f << HTML_NI() << std::endl;
    }
    else {
      f << HTML_IS(quickbuildIssues[ds->GetName()].first, 0, "quickbuilds") << std::endl;
    }
  }
  f << "</tr>" << std::endl;
#endif
  f << "</table>" << std::endl;

  // add the actual status info here
  f << "<hr>" << std::endl;
  f << shipInfo << std::endl;
  f << "<hr>" << std::endl;
  f << pilotInfo << std::endl;
  f << "<hr>" << std::endl;
  f << upgradeInfo << std::endl;
#ifdef INC_QUICKBUILDS
  f << "<hr>" << std::endl;
  f << quickbuildInfo << std::endl;
#endif

  // end
  f << "</body>" << std::endl;
  f << "</html>" << std::endl;
  f.close();
}



std::string GetDPStr(std::vector<xdv::DP> dps) {
  std::stringstream ss;
  for(xdv::DP dp : dps) {
    ss << "[" << GetStr(dp) << "]";
  }
  return ss.str();
}

void LoadDataSource(xdv::DataSrc& ds) {
    printf("%s...\n", ds.GetName().c_str()); fflush(stdout);
    std::vector<std::string> errs = ds.Load();
    printf("  Version: %s\n", ds.GetVersion().c_str());
    printf("  %3d Ships:       %s\n", ds.GetShipCount(),       GetDPStr(ds.GetShpDps()).c_str());
    printf("  %3d Pilots:      %s\n", ds.GetPilotCount(),      GetDPStr(ds.GetPltDps()).c_str());
    printf("  %3d Upgrades:    %s\n", ds.GetUpgradeCount(),    GetDPStr(ds.GetUpgDps()).c_str());
    printf("  %3d QuickBuilds: %s\n", ds.GetQuickBuildCount(), GetDPStr(ds.GetQbDps()).c_str());
    if(errs.size()) {
      printf("  Errors:\n");
      for(const std::string& e : errs) {
	printf("    -%s\n", e.c_str());
      }
    }
    //s->Dump();
}



int main(int argc, char *argv[]) {
  // *** options ***
  XDVOptions opts = {};
  for(int i=1; i<argc; i++) {
    std::string arg = std::string(argv[i]);
    if(arg == "-help") {
      printf("Options:\n");
      printf("  -unreleased\n");
      printf("  -all\n");
      printf("  -diff\n");
      printf("  -html\n");
      printf("data sources (if none are specified, all will be used)\n");
      printf("  -ffg\n");
      printf("  -xd2\n");
      printf("  -yasb\n");
      return 0;
    }
    if(0) {}
    else if(arg == "-unreleased") { opts.includeUnreleased = true; }
    else if(arg == "-all")        { opts.printAll = true; }
    else if(arg == "-diff")       { opts.diff = true; }
    else if(arg == "-html")       { opts.html = true; }
    else if(arg == "-ffg")        { opts.sources |= DataSrc::FFG; }
    else if(arg == "-xd2")        { opts.sources |= DataSrc::XD2; }
    else if(arg == "-yasb")       { opts.sources |= DataSrc::YASB; }
    else {
      printf("Invalid option '%s'\n", arg.c_str());
      return 1;
    }
  }

  if(opts.sources == DataSrc::NONE) { opts.sources = DataSrc::ALL; }

  // *** build the data sources ***
  // mine
  std::unique_ptr<xdv::lxw2::LXW2DS> lxw2 { new xdv::lxw2::LXW2DS() };
  // theirs
  std::vector<std::unique_ptr<xdv::DataSrc>> srcs;
  if((opts.sources && DataSrc::FFG))  { srcs.emplace_back(new xdv::ffg::FFGDS());   }
  if((opts.sources && DataSrc::XD2))  { srcs.emplace_back(new xdv::xd2::XD2DS());   }
  if((opts.sources && DataSrc::YASB)) { srcs.emplace_back(new xdv::yasb::YASBDS()); }

  // *** loading ***
  printf("Loading\n");
  // mine
  LoadDataSource(*lxw2);

  // theirs
  for(auto &s : srcs) {
    LoadDataSource(*s);
  }

  printf("\n");

  // results
  if(opts.html) {
    CreateHtmlResults(lxw2, srcs, opts);
  } else {
    PrintResults(lxw2, srcs, opts);
  }

  return 0;
}

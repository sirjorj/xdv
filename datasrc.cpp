#include "datasrc.h"
#include <sstream>

namespace xdv {

const char* GetStr(DP dp) {
  switch(dp) {
  case DP::Abil:  return "Has Ability";
  case DP::Abil2: return "Has Ability (S2)";
  case DP::Acti:  return "Actions";
  case DP::Agil:  return "Agility";
  case DP::ArtC:  return "Art-Card";
  case DP::ArtC2: return "Art-Card (S2)";
  case DP::ArtI:  return "Art-Image";
  case DP::ArtI2: return "Art-Image (S2)";
  case DP::Atck:  return "Attack";
  case DP::Chrg:  return "Charge";
  case DP::Cost:  return "Cost";
  case DP::Enga:  return "Engagement";
  case DP::Hull:  return "Hull";
  case DP::Fact:  return "Faction";
  case DP::Ffg:   return "FFG ID";
  case DP::Ffg2:  return "FFG ID (S2)";
  case DP::Forc:  return "Force";
  case DP::Hype:  return "Hyperspace";
  case DP::Init:  return "Initiative";
  case DP::Kwds:  return "Keywords";
  case DP::Limi:  return "Limited";
  case DP::Manv:  return "Maneuvers";
  case DP::Name:  return "Name";
  case DP::QuBu:  return "Quick Build";
  case DP::ShAb:  return "Ship Ability";
  case DP::Ship:  return "Ship";
  case DP::Shld:  return "Shield";
  case DP::Size:  return "Size";
  case DP::Slot:  return "Slots";
  case DP::Slot2: return "Slots (S2)";
  case DP::Subt:  return "Subtitle";
  case DP::Text:  return "Text";
  case DP::Text2: return "Text (S2)";
  case DP::Titl:  return "Title";
  case DP::Titl2: return "Title (S2)";
  case DP::UBar:  return "Upgrade Bar";
  case DP::UpTy:  return "Upgrade Type";
  case DP::UpTy2: return "Upgrade Type (S2)";
  case DP::Xws:   return "XWS ID";
  case DP::Yasb:  return "YASB ID";
  default:        return "????";
  }
}



bool SortActions(const std::list<libxwing2::SAct>& a, const std::list<libxwing2::SAct>& b) {
  std::list<libxwing2::SAct>::const_iterator ia = a.begin();
  std::list<libxwing2::SAct>::const_iterator ib = b.begin();
  while(true) {
    if     (ia == a.end()) { return true;  }
    else if(ib == b.end()) { return false; }
    if(ia->action == ib->action) {
      ia++;
      ib++;
    }
    else {
      return ia->action < ib->action;
    }
  }
}

bool SortKeywords(const libxwing2::KWd& a, const libxwing2::KWd& b) {
  return libxwing2::Keyword::GetKeyword(a).GetName() < libxwing2::Keyword::GetKeyword(b).GetName();
}



std::optional<libxwing2::QB>  GetQB(std::pair<int, std::vector<libxwing2::QBPlt>> param, std::vector<std::string>& err) {
  //DBGV("***** QB - GetQB(%d, %s", param.first, libxwing2::Pilot::GetPilot(param.second[0].plt).GetName().c_str());
  //DBGV("%s%s", (param.second.size() > 1) ? ", " : "", (param.second.size() > 1) ? libxwing2::Pilot::GetPilot(param.second[1].plt).GetName().c_str() : "");
  //DBG(")\n");
  //int threat = param.first;
  std::vector<libxwing2::QBPlt> qbPlts = param.second;
  if(qbPlts.size()) {
    libxwing2::Plt plt = qbPlts[0].plt;
    std::vector<libxwing2::QuickBuild> hits = libxwing2::QuickBuild::GetQuickBuilds(qbPlts[0].plt);
    if(hits.size() == 1) {
      //DBG("  QB - 1 Match\n");
      return hits[0].GetQB();
    }
    else {
      if(hits.size() > 1) {
	//DBG("  multiple matches...\n");
	// ok... multiple hits...
	std::vector<libxwing2::QuickBuild> temp1, temp2;
	// first, check by number of pilots...
	temp1 = hits;
	temp2.clear();
	for(const libxwing2::QuickBuild& t : temp1) {
	  if(qbPlts.size() == t.GetQBPlts().size()) {
	    temp2.push_back(t);
	  }
	}
	if(temp2.size() == 1) {
	  //DBG("  Match based on pilot count\n");
	  return { temp2[0].GetQB() } ;
	}
	// then check by number of upgrades (on first pilot)
	temp1 = temp2;
	temp2.clear();
	for(const libxwing2::QuickBuild& t : temp1) {
	  if(qbPlts[0].upgs.size() == t.GetQBPlts()[0].upgs.size()) {
	    temp2.push_back(t);
	  }
	}
	if(temp2.size() == 1) {
	  //DBG("  Match based on number of upgrades\n");
	  return { temp2[0].GetQB() } ;
	}
	// then look at upgrades
	temp1 = temp2;
	temp2.clear();
	std::vector<libxwing2::Upg> targetUpg = qbPlts[0].upgs;
	std::sort(targetUpg.begin(), targetUpg.end());
	for(const libxwing2::QuickBuild& t : temp1) {
	  std::vector<libxwing2::Upg> tempUpg = t.GetQBPlts()[0].upgs;
	  std::sort(tempUpg.begin(), tempUpg.end());
	  bool match = true;
	  for(int i=0; i<targetUpg.size(); i++) {
	    if(targetUpg[i] != tempUpg[i]) {
	      match = false;
	      break;
	    }
	  }
	  if(match) {
	    temp2.push_back(t);
	  }
	}
	if(temp2.size() == 1) {
	  //DBG("  Match based on upgrades\n");
	  return { temp2[0].GetQB() } ;
	}
      }
      err.push_back("QB: unable to match pilot " + libxwing2::Pilot::GetPilot(plt).GetName());
    }
  }
  return std::nullopt;
}



std::string GetStr(const libxwing2::ActionBar& ab) {
  std::stringstream ss;
  libxwing2::ActionBar sab = ab;
  std::sort(sab.begin(), sab.end(), SortActions);
  for(auto a : sab) {
    bool first = true;
    ss << "[";
    for(libxwing2::SAct sa : a) {
      if(first) { first = false; }
      else      { ss << " > "; }
      ss << "[" << libxwing2::Difficulty::GetDifficulty(sa.difficulty).GetName().substr(0,1) << "|" << libxwing2::Action::GetAction(sa.action).GetName() << "]";
    }
    ss << "]";
  }
  return ss.str();
}

std::string GetStr(const libxwing2::Chargeable& ch) {
  std::stringstream ss;
  ss << "[" << std::to_string(ch.GetCapacity()) << "|" << std::to_string(ch.GetRecurring()) << "]";
  return ss.str();
}

std::string GetStr(const libxwing2::Force& fo) {
  std::stringstream ss;
  ss << "[" << std::to_string(fo.GetCapacity()) << "|" << std::to_string(fo.GetRecurring()) << "|" << fo.GetForceAffiliation().GetName() << "]";
  return ss.str();
}


std::string GetStr(const libxwing2::KeywordList& kw) {
  std::stringstream ss;
  libxwing2::KeywordList skw = kw;
  std::sort(skw.begin(), skw.end(), SortKeywords);
  for(const libxwing2::KWd& k : skw) {
    ss << "[" << libxwing2::Keyword::GetKeyword(k).GetName() << "]";
  }
  return ss.str();
}
  
std::string GetStr(const libxwing2::Maneuvers& maneuvers) {
  std::stringstream ss;
  for(int s=5; s>-2; s--) {
    for(const libxwing2::Bearing& b : libxwing2::Bearing::GetAllBearings()) {
      for(libxwing2::Maneuver m : maneuvers) {
	if((m.speed == s) && (m.bearing == b.GetBrn())) {
	  ss << "[" << std::to_string(s) << "|" << b.GetShortName() << "|" << libxwing2::Difficulty::GetDifficulty(m.difficulty).GetName().substr(0,1) << "]";
	}
      }
    }
  }
  return ss.str();
}

std::string GetStr(const libxwing2::PriAttacks& pas) {
  std::stringstream ss;
  for(const libxwing2::PriAttack& pa : pas) {
    ss << "[" << libxwing2::FiringArc::GetFiringArc(pa.arc).GetName() << "|" << std::to_string(pa.value) << "]";
  }
  return ss.str();
}

std::string GetStr(const int threat, const std::vector<libxwing2::QBPlt>& qbps) {
  std::stringstream ss;
  ss << std::to_string(threat);
  for(const libxwing2::QBPlt& qPlt : qbps) {
    ss << " [";
    ss << libxwing2::Pilot::GetPilot(qPlt.plt).GetName();
    ss << " - ";
    std::map<std::string, std::vector<std::string>> upgrades;
    for(libxwing2::Upg upg : qPlt.upgs) {
      libxwing2::Upgrade upgrade = libxwing2::Upgrade::GetUpgrade(upg);
      upgrades[upgrade.GetUpgradeType().GetShortName()].push_back(upgrade.GetShortName());
    }
    bool first = true;
    for(std::pair<std::string, std::vector<std::string>> up : upgrades) {
      std::sort(up.second.begin(), up.second.end());
      for(std::string u : up.second) {
	if(first) { first = false; }
	else      { ss << ", ";    }
	ss << up.first << ":" << u;
      }
    }
    ss << "]";
  }
  return ss.str();
}

std::string GetStr(const libxwing2::ShipAbility& sa) {
  return sa.GetName() + " - " + sa.GetText().GetCleanText();
}

std::string GetStr(const std::optional<libxwing2::Cost> c) {
  if(!c) { return "???"; }
  bool first = true;
  std::ostringstream ret;
  switch(c->GetCsT()) {
  case libxwing2::CsT::Const:                 break;
  case libxwing2::CsT::Base: ret << "Base: "; break;
  case libxwing2::CsT::Agi:  ret << "Agi: ";  break;
  case libxwing2::CsT::Init: ret << "Init: "; break;
  }

  const std::vector<int16_t> costs = c->GetCosts();
  for(int16_t c : costs) {
    if(first) { first = false; }
    else      { ret << "/"; }
    ret << std::to_string(c);
  }
  return ret.str();
}

std::string GetStr(const libxwing2::UpgradeBar& u) {
  std::stringstream ss;
  // copy and sort so order doesnt matter
  libxwing2::UpgradeBar sBar = u;
  std::sort(sBar.begin(), sBar.end(), [](const libxwing2::UpgradeSlot& s1, const libxwing2::UpgradeSlot& s2) { return s1.GetTypes()[0] < s2.GetTypes()[0]; } );

  for(const libxwing2::UpgradeSlot &s : sBar) {
    // if pilots ever get slots that can take multiple types, this will break hard!
    ss << "[" << libxwing2::UpgradeType::GetUpgradeType(s.GetTypes()[0]).GetShortName() << "]";
  }
  return ss.str();
}

std::string GetStr(const std::vector<libxwing2::UpT>& u) {
  // this one is for slots on upgrades
  std::stringstream ss;
  std::vector<libxwing2::UpT> UpTs = u;
  std::sort(UpTs.begin(), UpTs.end(), [](const libxwing2::UpT& ut1, const libxwing2::UpT& ut2) { return ut1 > ut2; } );
  for(libxwing2::UpT t : UpTs) {
    ss << "[" << libxwing2::UpgradeType::GetUpgradeType(t).GetShortName() << "]";
  }
  return ss.str();
}



DataSrc::DataSrc(std::string n) : name(n) {}
DataSrc::~DataSrc() {}
std::string DataSrc::GetName() { return this->name; }

std::string DataSrc::GetVersion() { return this->version; }

int DataSrc::GetShipCount()       { return this->shpDb.size(); }
int DataSrc::GetPilotCount()      { return this->pltDb.size(); }
int DataSrc::GetUpgradeCount()    { return this->upgDb.size(); }
int DataSrc::GetQuickBuildCount() { return this->qbDb.size();  }

std::vector<DP> DataSrc::GetShpDps() const { return this->shpDs; }
bool DataSrc::Has(libxwing2::Shp s) const { return (this->shpDb.find(s) != this->shpDb.end()); }
std::optional<std::string> DataSrc::GetDp(libxwing2::Shp s, DP dp) const {
  if(this->Has(s)) {
    if(this->shpDb.at(s).find(dp) != this->shpDb.at(s).end()) {
      return this->shpDb.at(s).at(dp);
    }
  }
  return std::nullopt;
}

std::vector<DP> DataSrc::GetPltDps() const { return this->pltDs; }
bool DataSrc::Has(libxwing2::Plt p) const { return (this->pltDb.find(p) != this->pltDb.end()); }
std::optional<std::string> DataSrc::GetDp(libxwing2::Plt p, DP dp) const {
  if(this->Has(p)) {
    if(this->pltDb.at(p).find(dp) != this->pltDb.at(p).end()) {
      return this->pltDb.at(p).at(dp);
    }
  }
  return std::nullopt;
}

std::vector<DP> DataSrc::GetUpgDps() const { return this->upgDs; }
bool DataSrc::Has(libxwing2::Upg u) const { return (this->upgDb.find(u) != this->upgDb.end()); }
std::optional<std::string> DataSrc::GetDp(libxwing2::Upg u, DP dp) const {
  if(this->Has(u)) {
    if(this->upgDb.at(u).find(dp) != this->upgDb.at(u).end()) {
      return this->upgDb.at(u).at(dp);
    }
  }
  return std::nullopt;
}



std::vector<DP> DataSrc::GetQbDps() const { return this->qbDs; }
bool DataSrc::Has(libxwing2::QB q) const { return (this->qbDb.find(q) != this->qbDb.end()); }
std::optional<std::string> DataSrc::GetDp(libxwing2::QB q, DP dp) const {
  if(this->Has(q)) {
    if(this->qbDb.at(q).find(dp) != this->qbDb.at(q).end()) {
      return this->qbDb.at(q).at(dp);
    }
  }
  return std::nullopt;
}

  
  
  
void DataSrc::PopulateDps() {
  { // ships
    std::map<DP,int> map;
    for(std::pair<libxwing2::Shp, DataPoints> db : this->shpDb) {
      for(std::pair<DP,std::string> dp : db.second) {
	map[dp.first] = 0;
      }
    }
    for(std::pair<DP,int> m : map) {
      this->shpDs.push_back(m.first);
    }
  }

  { // pilots
    std::map<DP,int> map;
    for(std::pair<libxwing2::Plt, DataPoints> db : this->pltDb) {
      for(std::pair<DP,std::string> dp : db.second) {
	map[dp.first] = 0;
      }
    }
    for(std::pair<DP,int> m : map) {
      this->pltDs.push_back(m.first);
    }
  }

  { // upgrades
    std::map<DP,int> map;
    for(std::pair<libxwing2::Upg, DataPoints> db : this->upgDb) {
      for(std::pair<DP,std::string> dp : db.second) {
	map[dp.first] = 0;
      }
    }
    for(std::pair<DP,int> m : map) {
      this->upgDs.push_back(m.first);
    }
  }

  { // quickbuilds
    std::map<DP,int> map;
    for(std::pair<libxwing2::QB, DataPoints> db : this->qbDb) {
      for(std::pair<DP,std::string> dp : db.second) {
	map[dp.first] = 0;
      }
    }
    for(std::pair<DP,int> m : map) {
      this->qbDs.push_back(m.first);
    }
  }
}

}

#pragma once
#include "datasrc.h"

namespace xdv::xd2 {

class XD2DS : public DataSrc {
 public:
  XD2DS();
  virtual std::vector<std::string> Load() override;
};

}

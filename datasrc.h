#pragma once
#include <libxwing2/pilot.h>
#include <libxwing2/quickbuild.h>

namespace xdv {

// datapoints
enum class DP {
  Abil,
  Abil2,
  Acti,
  Agil,
  ArtC,
  ArtC2,
  ArtI,
  ArtI2,
  Atck,
  Chrg,
  Cost,
  Enga,
  Fact,
  Ffg,
  Ffg2,
  Forc,
  Hull,
  Hype,
  Init,
  Kwds,
  Limi,
  Manv,
  Name,
  QuBu,
  ShAb,
  Ship,
  Shld,
  Size,
  Slot,
  Slot2,
  Subt,
  Text,
  Text2,
  Titl,
  Titl2,
  UBar,
  UpTy,
  UpTy2,
  Xws,
  Yasb,
};
const char* GetStr(DP dp);

std::optional<libxwing2::QB> GetQB(std::pair<int, std::vector<libxwing2::QBPlt>> param, std::vector<std::string>& err);

std::string GetStr(const libxwing2::ActionBar& ab);
std::string GetStr(const libxwing2::Chargeable& ch);
std::string GetStr(const libxwing2::Force& fo);
std::string GetStr(const libxwing2::KeywordList& kw);
std::string GetStr(const libxwing2::Maneuvers& maneuvers);
std::string GetStr(const libxwing2::PriAttacks& pas);
std::string GetStr(const int threat, const std::vector<libxwing2::QBPlt>& qbps);
std::string GetStr(const libxwing2::ShipAbility& sa);
std::string GetStr(const std::optional<libxwing2::Cost> c);
std::string GetStr(const libxwing2::UpgradeBar& u);
std::string GetStr(const std::vector<libxwing2::UpT>& u);

typedef std::map<DP, std::string> DataPoints;
typedef std::map<const libxwing2::Shp, DataPoints> ShpDB;
typedef std::map<const libxwing2::Plt, DataPoints> PltDB;
typedef std::map<const libxwing2::Upg, DataPoints> UpgDB;
typedef std::map<const libxwing2::QB,  DataPoints> QBDB;


class DataSrc {
 public:
  DataSrc(std::string n);
  virtual ~DataSrc();
  std::string GetName();
  virtual std::vector<std::string> Load()=0;
  std::string GetVersion();
  int GetShipCount();
  int GetPilotCount();
  int GetUpgradeCount();
  int GetQuickBuildCount();

  std::vector<DP> GetShpDps() const;
  bool Has(libxwing2::Shp) const;
  std::optional<std::string> GetDp(libxwing2::Shp, DP dp) const;

  std::vector<DP> GetPltDps() const;
  bool Has(libxwing2::Plt) const;
  std::optional<std::string> GetDp(libxwing2::Plt, DP dp) const;

  std::vector<DP> GetUpgDps() const;
  bool Has(libxwing2::Upg) const;
  std::optional<std::string> GetDp(libxwing2::Upg, DP dp) const;

  std::vector<DP> GetQbDps() const;
  bool Has(libxwing2::QB) const;
  std::optional<std::string> GetDp(libxwing2::QB, DP dp) const;

 protected:
  std::string name;
  std::string version;

  ShpDB shpDb;
  std::vector<DP> shpDs;

  PltDB pltDb;
  std::vector<DP> pltDs;
  
  UpgDB upgDb;
  std::vector<DP> upgDs;

  QBDB qbDb;
  std::vector<DP> qbDs;

  void PopulateDps();
};

}

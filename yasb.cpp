#include "yasb.h"
#include "strhlp.h"
#include <libxwing2/converter.h>
#include <algorithm>
#include <array>
#include <map>
#include <regex>
#include <sstream>


namespace xdv::yasb {

static std::string SafeGetXws(libxwing2::Plt plt) {
  try { return libxwing2::converter::xws::GetPilot(plt); }
  catch(libxwing2::converter::xws::PilotNotFound &pnf) { return ""; }
}
static std::string SafeGetXws(libxwing2::Upg upg) {
  try { return libxwing2::converter::xws::GetUpgrade(upg); }
  catch(libxwing2::converter::xws::UpgradeNotFound &unf) { return ""; }
}

static std::vector<std::string> GetText(std::string filename) {
  char *line = 0;
  size_t len = 0;
  FILE *f = fopen(filename.c_str(), "r");
  std::vector<std::string> ret;
  while(getline(&line, &len, f) != -1) {
    ret.push_back(line);
  }
  fclose(f);
  if(line) { free(line); }
  return ret;
}

static int GetNumValue(std::string s) {
  std::vector<std::string> toks = strhlp::Tokenize(s, ':');
  if(toks[1] == "\"*\"") { return 0; }
  else                   { return std::stoi(strhlp::Trim(toks[1])); }
}

static std::string Canonicalize(std::string name) {
  std::transform(name.begin(), name.end(), name.begin(), ::tolower);
  std::stringstream ss;
  for(int i=0; i<name.size(); i++) {
    char c = name[i];
    if(((c>=48) && (c<=57)) || ((c>=97) && (c<=122))) {
      ss << c;
    }
  }
  return ss.str();
}

static std::string GetStrValue(std::string s) {
  std::vector<std::string> toks = strhlp::Tokenize(s, ':');
  std::string value = (toks.size() > 1) ? toks[1] : toks[0];
  if((value[0]=='\"' && value[value.size()-1]=='\"') || (value[0]=='\'' && value[value.size()-1]=='\'')) {
    // trim the encasing double quotes
    // unless the word is supposed to have double quotes, in which case the double quotes are encased in single quotes
    value = value.substr(1, value.size()-2);
  }
  return strhlp::Trim(value);
}

libxwing2::Cost GetCost(const std::vector<uint16_t>& p, libxwing2::CsT type) {
  switch(type) {
  case libxwing2::CsT::Const:
    return libxwing2::Cost::ByCost(p[0]);

  case libxwing2::CsT::Agi:
    return libxwing2::Cost::ByAgi(p[0], p[1], p[2], p[3]);

  case libxwing2::CsT::Base:
    if     (p.size() == 3) { return libxwing2::Cost::ByBase(p[0], p[1], p[2]); }
    else if(p.size() == 4) { return libxwing2::Cost::ByBase(p[0], p[1], p[2], p[3]); }

  case libxwing2::CsT::Init:
    if(p.size() == 7) { return libxwing2::Cost::ByInit(p[0], p[1], p[2], p[3], p[4], p[5], p[6]); }
    if(p.size() == 9) { return libxwing2::Cost::ByInit(p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]); }    

  default: return libxwing2::Cost::ByCost(0);
  }
}

bool HasKeyword(const libxwing2::KeywordList& kwl, libxwing2::KWd kwd) {
  if(std::find(kwl.begin(), kwl.end(), kwd) == kwl.end()) {
    return false;
  } else {
    return true;
  }
}
  
static std::string AdjustName(std::string n) {
  if(false) { }
  else if(n == "upsilonclasscommandshuttle") { return "upsilonclassshuttle"; }
  else if(n == "tieinterceptor")             { return "tieininterceptor"; }
  else { return n; }
}

static void AddAction(libxwing2::ActionBar& ab, std::string aStr) {
  bool isLinked = false;
  libxwing2::Dif dif = libxwing2::Dif::White;

  if(aStr == "Slam") { aStr = "SLAM"; }

  if(strhlp::StartsWith(aStr, "R-> ")) {
    isLinked = true;
    dif = libxwing2::Dif::Red;
    aStr = aStr.substr(4);
  }
  else if(strhlp::StartsWith(aStr, "R-")) {
    dif = libxwing2::Dif::Red;
    aStr = aStr.substr(2);
  } 
 else if(strhlp::StartsWith(aStr, "> ")) {
    isLinked = true;
    dif = libxwing2::Dif::White;
    aStr = aStr.substr(2);
  }
  else if(strhlp::StartsWith(aStr, "F-")) {
    dif = libxwing2::Dif::Purple;
    aStr = aStr.substr(2);
  }

  for(const libxwing2::Action& action : libxwing2::Action::GetAllActions()) {
    if(aStr == action.GetName()) {
      if(isLinked) {
	ab.back().push_back({dif,action.GetAct()});
      } else {
	ab.push_back({{{dif,action.GetAct()}}});
      }
    }
  }
}

static void AddUpgrade(libxwing2::UpgradeBar& slots, std::string uStr) {
  if     (uStr == "Force")  { uStr = "Force Power"; }
  else if(uStr == "Device") { uStr = "Payload"; }
  for(const libxwing2::UpgradeType& ut : libxwing2::UpgradeType::GetAllUpgradeTypes()) {
    if(ut.GetName() == uStr) {
      slots.push_back({ut.GetUpT()});
    }
  }
}

libxwing2::KeywordList GetKeywordList(std::string line) {
  std::string kwStr = GetStrValue(line);
  kwStr = strhlp::ReplaceStr(kwStr, "\\[", "");
  kwStr = strhlp::ReplaceStr(kwStr, "\\]", "");
  std::vector<std::string> toks = strhlp::Tokenize(kwStr, ',');
  libxwing2::KeywordList keywords;
  for(const std::string& t : toks) {
    std::string ks = t.substr(1,t.size()-2);
    for(const libxwing2::Keyword& keyword : libxwing2::Keyword::GetAllKeywords()) {
      if(keyword.GetName() == ks) {
	keywords.push_back(keyword.GetKWd());
	break;
      }
    }
  }
  return keywords;
}

  /*
static libxwing2::QBPlt GetQbPlt(QbInfo &qbInfo) {
  for(std::pair<const int,QbInfo>& qbp : qbInfo) {
    if(!qbp.second.skip) {
      std::optional<libxwing2::Plt> plt;
      std::vector<libxwing2::Upg> upgs;
      for(const std::pair<libxwing2::Plt,PilotInfo> &pi : pilotInfo) {
	if(pi.second.yasbName == qbp.second.pilot) {
	  plt = pi.first;
	  break;
	}
      }
      if(!plt) { throw std::runtime_error(qbp.second.pilot + " - wtf?"); }
      for(const std::string &upgrade : qbp.second.upgrades) {
	std::optional<libxwing2::Upg> upg;
	for(const std::pair<libxwing2::Upg,UpgradeInfo> &ui : upgradeInfo) {
	  if(ui.second.yasbName == upgrade) {
	    upg = ui.first;
	    break;
	  }
	}
	if(!upg) { throw std::runtime_error(qbp.second.pilot + " -> " + upgrade + " - wtf2?"); }
	else { upgs.push_back(*upg); }
      }

      return {*plt, upgs};
    }
  }
}
  */


  


YASBDS::YASBDS()
  : DataSrc("YASB"){
}

std::vector<std::string> YASBDS::Load() {
  std::vector<std::string> ret;

  //enum class 
  const char* PATH = "./yasb/coffeescripts/content/cards-common.coffee";
  std::vector<std::string> lines = GetText(PATH);

  // data mappings (these orders are important!)
  static const std::array<const libxwing2::Brn,13> bearings = { libxwing2::Brn::LTurn,
								libxwing2::Brn::LBank,
								libxwing2::Brn::Straight,
								libxwing2::Brn::RBank,
								libxwing2::Brn::RTurn,
								libxwing2::Brn::KTurn,
								libxwing2::Brn::LSloop,
								libxwing2::Brn::RSloop,
								libxwing2::Brn::LTroll,
								libxwing2::Brn::RTroll,
								libxwing2::Brn::RevLBank,
								libxwing2::Brn::RevStraight,
								libxwing2::Brn::RevRBank };

  static const std::array<const libxwing2::Dif,5> difficulties = { libxwing2::Dif::Red, // zero means it doesn't have the maneuver so it will be ignored anyway
								   libxwing2::Dif::Blue,
								   libxwing2::Dif::White,
								   libxwing2::Dif::Red,
								   libxwing2::Dif::Purple };

  // structures
  enum class SectionType {
    Unknown,
    Ship,
    Pilot,
    Upgrade,
    Condition,
    QuickBuild,
    HyperspaceShipInclude,
    HyperspacePilotExclude,
    HyperspaceUpgradeExclude,
  };

  enum class State {
    None,
    Actions,
    Maneuvers,
    Slots,
    Conf,
    Upgrades,
  };

  struct ShipInfo {
    // things about the ship that will be needed later
    DataPoints dps;
    std::string yasbName; // need this to match pilots to ships
    libxwing2::PriAttacks attack;
    int8_t agility;
    int8_t hull;
    int8_t shield;
    int8_t shieldRec;
    libxwing2::ActionBar actions;
    bool hyperspace;
    libxwing2::KeywordList keywords;
  };
  

  struct PilotInfo {
    DataPoints dps;
    std::string yasbName; // needed?
    libxwing2::Shp shp;
    std::pair<uint8_t, int8_t> charge;
    struct {
      uint8_t value;
      bool recurring;
      libxwing2::FAf affiliation;
    } force;
    //std::pair<uint8_t, bool> force;
    libxwing2::ActionBar actions;
    libxwing2::UpgradeBar slots;
    libxwing2::KeywordList keywords;
    bool skip;
  };

  struct UpgradeInfo {
    DataPoints dps;
    std::string yasbName; // needed?
    std::vector<std::string> ship;
    std::vector<int16_t> points;
    libxwing2::CsT costType;
    bool skip;
  };

  struct QbInfo {
    DataPoints dps;
    int id;
    std::string faction;
    std::string pilot;
    std::string suffix;
    int linkedId;
    std::string ship;
    std::string threat;
    std::vector<std::string> upgrades;
    bool skip;
  };

  // state variables:
  // ...global
  SectionType sectionType = SectionType::Unknown;
  SectionType switchTo = SectionType::Unknown;
  State state = State::None;

  // ...ship
  std::map<libxwing2::Shp, ShipInfo> shipInfo;
  ShipInfo curShpInfo;
  std::optional<libxwing2::Maneuvers> curManeuvers;
  int8_t curSpeed;
  // ...pilot
  std::map<libxwing2::Plt, PilotInfo> pilotInfo;
  PilotInfo curPltInfo;
  // ...upgrade
  std::map<libxwing2::Upg, UpgradeInfo> upgradeInfo;
  UpgradeInfo curUpgInfo;
  // ...quickbuild
  std::map<int,QbInfo> qbInfo;
  QbInfo curQbInfo;

  for(int i=0; i<lines.size(); i++) {
    std::string line = strhlp::Trim(lines[i]);
    std::string next = ((i+1)<lines.size()) ? lines[i+1] : "";

    // entering a main section
    if     (line == "ships:")                                    { sectionType = SectionType::Ship;                     state = State::None; continue; }
    else if(line == "pilotsById: [")                             { switchTo    = SectionType::Pilot;                    state = State::None; continue; }
    else if(line == "upgradesById: [")                           { sectionType = SectionType::Upgrade;                  state = State::None; continue; }
    else if(line == "conditionsById: [")                         { sectionType = SectionType::Condition;                state = State::None; continue; }
    else if(line == "quickbuildsById: [")                        { sectionType = SectionType::QuickBuild;               state = State::None; continue; }
    else if(line == "exportObj.hyperspaceShipInclusions = [")    { sectionType = SectionType::HyperspaceShipInclude;    state = State::None; continue; }
    else if(line == "exportObj.hyperspacePilotExclusions = [")   { sectionType = SectionType::HyperspacePilotExclude;   state = State::None; continue; }
    else if(line == "exportObj.hyperspaceUpgradeExclusions = [") { sectionType = SectionType::HyperspaceUpgradeExclude; state = State::None; continue; }

    //
    // *** Ships ***
    // Manv, Size, Xws get added to shpDb (Name is not valid here)
    // several other things go to shipInfo because pilots need them
    //
    if(sectionType == SectionType::Ship) {
      // start of new record
      if(strhlp::StartsWith(line, "name:") || switchTo == SectionType::Pilot) {
	// finish the previous one
	try {
	  libxwing2::Shp shp = libxwing2::converter::xws::GetShip(curShpInfo.dps[DP::Xws]);
	  libxwing2::Ship ship = libxwing2::Ship::GetShip(shp);
	  this->shpDb[ship.GetShp()] = curShpInfo.dps;
	  shipInfo[ship.GetShp()] = curShpInfo;
	}
	catch(libxwing2::converter::xws::ShipNotFound& snf) {
	  ret.push_back("Ship not found: " + curShpInfo.dps[DP::Xws]);
	}
	// prepare for the new one
	curShpInfo = ShipInfo();
	if(switchTo != SectionType::Unknown) {
	  sectionType = switchTo;
	  switchTo = SectionType::Unknown;
  	} else {
	  std::string strName = GetStrValue(line);
	  curShpInfo.yasbName = strName;
	}
      }

      // parsing the maneuver list
      else if(state == State::Maneuvers) {
	if(line == "]") {
	  state = State::None;
	  curShpInfo.dps[DP::Manv] = GetStr(*curManeuvers);
	}
	else {
	  // parse the maneuver line... (again...)
	  std::string mStr = line;
	  mStr = strhlp::ReplaceStr(mStr, "\\[", "");
	  mStr = strhlp::ReplaceStr(mStr, "\\]", "");
	  mStr = strhlp::Trim(mStr);
	  std::vector<std::string> toks = strhlp::Tokenize(mStr, ',');
	  for(int i=0; i<toks.size(); i++) {
	    int d = std::stoi(strhlp::Trim(toks[i]));
	    if(d) {
	      curManeuvers->push_back({ curSpeed, (curSpeed==0 && bearings[i]==libxwing2::Brn::Straight)
		    ? libxwing2::Brn::Stationary
		    : bearings[i], difficulties[d] });
	    }
	  }
	  curSpeed++;
	}
      }
      // parsing the actions
      else if(state == State::Actions) {
	if(line == "]") {
	  state = State::None;
	}
	else {
	  std::string aStr = line.substr(1, line.size()-2);
	  AddAction(curShpInfo.actions, aStr);
 	}
      }
      else if(strhlp::StartsWith(line, "actions:")) {
	state = State::Actions;
      }
      // shpDb stuff...
      else if(strhlp::StartsWith(line, "xws:")) {
	std::string realName = GetStrValue(line);
	realName = realName.substr(1, realName.find("\"", 1)-1);
	//curShpInfo.dps[DP::Name] = realName; // not valid for tie interceptor or upsilon
	curShpInfo.dps[DP::Xws]  = AdjustName(Canonicalize(realName));
	curShpInfo.dps[DP::Size] = "Small";
      }
      else if(strhlp::StartsWith(line, "maneuvers:")) {
	state = State::Maneuvers;
	curManeuvers = libxwing2::Maneuvers();
	curSpeed = 0;
      }
      else if(strhlp::StartsWith(line, "medium:")) {
	curShpInfo.dps[DP::Size] = "Medium";
      }
      else if(strhlp::StartsWith(line, "large:")) {
	curShpInfo.dps[DP::Size] = "Large";
      }
      else if(strhlp::StartsWith(line, "huge:")) {
	curShpInfo.dps[DP::Size] = "Huge";
      }
      // shipInfo stuff for pilot
      else if(strhlp::StartsWith(line, "attack:")) {
	curShpInfo.attack.push_back({libxwing2::Arc::Front, (uint8_t)GetNumValue(line)});
      }
      else if(strhlp::StartsWith(line, "attackb:")) {
	curShpInfo.attack.push_back({libxwing2::Arc::Rear, (uint8_t)GetNumValue(line)});
      }
      else if(strhlp::StartsWith(line, "attackl:")) {
	curShpInfo.attack.push_back({libxwing2::Arc::Left, (uint8_t)GetNumValue(line)});
      }
      else if(strhlp::StartsWith(line, "attackr:")) {
	curShpInfo.attack.push_back({libxwing2::Arc::Right, (uint8_t)GetNumValue(line)});
      }
      else if(strhlp::StartsWith(line, "attackf:")) {
	curShpInfo.attack.push_back({libxwing2::Arc::FullFront, (uint8_t)GetNumValue(line)});
      }
      else if(strhlp::StartsWith(line, "attackt:")) {
	curShpInfo.attack.push_back({libxwing2::Arc::SingleTurret, (uint8_t)GetNumValue(line)});
      }
      else if(strhlp::StartsWith(line, "attackdt:")) {
	curShpInfo.attack.push_back({libxwing2::Arc::DoubleTurret, (uint8_t)GetNumValue(line)});
      }
      else if(strhlp::StartsWith(line, "attackbull:")) {
	curShpInfo.attack.push_back({libxwing2::Arc::Bullseye, (uint8_t)GetNumValue(line)});
      }
      else if(strhlp::StartsWith(line, "agility:")) {
	curShpInfo.agility = GetNumValue(line);
      }
      else if(strhlp::StartsWith(line, "hull:")) {
	curShpInfo.hull = GetNumValue(line);
      }
      else if(strhlp::StartsWith(line, "shields:")) {
	curShpInfo.shield = GetNumValue(line);
      }
      else if(strhlp::StartsWith(line, "shieldrecurr:")) {
	curShpInfo.shieldRec = GetNumValue(line);
      }
      else if(strhlp::StartsWith(line, "keyword:")) {
	curShpInfo.keywords = GetKeywordList(line);
      }
    }



    // *** Pilots ***
    if(sectionType == SectionType::Pilot) {
      // new record
      if(strhlp::StartsWith(line, "{")) {
	curPltInfo = PilotInfo();
      }

      // end of record
      else if(strhlp::StartsWith(line, "}")) {
	if(!curPltInfo.skip) {
	  bool found = false;
	  for(const libxwing2::Pilot& pilot : libxwing2::Pilot::GetAllPilots()) {
	    if(SafeGetXws(pilot.GetPlt()) == curPltInfo.dps[DP::Xws]) {
      	      libxwing2::ActionBar ab;
	      {
		PilotInfo pi = curPltInfo;
		ShipInfo si = shipInfo[curPltInfo.shp];
		// curPltInfo.dps[DP::Acti] = curPltInfo.actions.size() ? GetStr(curPltInfo.actions) : GetStr(shipInfo[curPltInfo.shp].actions);
		if(pi.actions.size())    { ab.insert(ab.end(), pi.actions.begin(),    pi.actions.end());    }
		else                     { ab.insert(ab.end(), si.actions.begin(),    si.actions.end());    }
		// if its a droid, switch focus to calculate
		if(HasKeyword(pi.keywords, libxwing2::KWd::Droid)) {
		  for(std::list<libxwing2::SAct>& lsact : ab) {
		    for(libxwing2::SAct& sact : lsact) {
		      if(sact.action == libxwing2::Act::Focus) {
			sact.action = libxwing2::Act::Calculate;
		      }
		    }
		  }
		}
		// figure out force affiliation
		if     (HasKeyword(pi.keywords, libxwing2::KWd::LightSide)) { curPltInfo.force.affiliation = libxwing2::FAf::Light; }
		else if(HasKeyword(pi.keywords, libxwing2::KWd::DarkSide))  { curPltInfo.force.affiliation = libxwing2::FAf::Dark; }
	      }

	      //std::optional<ShipInfo> si;
	      curPltInfo.dps[DP::Atck] = GetStr(shipInfo[curPltInfo.shp].attack);
	      curPltInfo.dps[DP::Agil] = std::to_string(shipInfo[curPltInfo.shp].agility);
	      curPltInfo.dps[DP::Hull] = std::to_string(shipInfo[curPltInfo.shp].hull);
	      curPltInfo.dps[DP::Shld] = "[" + std::to_string(shipInfo[curPltInfo.shp].shield) + "|" + std::to_string(shipInfo[curPltInfo.shp].shieldRec) + "]"; // i treat shields as chargeables for huge ships
	      curPltInfo.dps[DP::Chrg] = GetStr(libxwing2::Chargeable(curPltInfo.charge.first, curPltInfo.charge.second));
	      curPltInfo.dps[DP::Forc] = GetStr(libxwing2::Force(curPltInfo.force.value, curPltInfo.force.recurring, curPltInfo.force.affiliation));
	      curPltInfo.dps[DP::Acti] = GetStr(ab);
	      curPltInfo.dps[DP::UBar] = GetStr(curPltInfo.slots);
	      curPltInfo.dps[DP::Hype] = "0";
	      libxwing2::KeywordList kws;
	      for(const libxwing2::KWd& kwd : shipInfo[curPltInfo.shp].keywords) {
		// yasb assigned the TIE keyword - i don't because it is on the card and not the point sheet
		if((curPltInfo.shp == libxwing2::Shp::NimbusClass) && (kwd == libxwing2::KWd::TIE)) { continue; }
		kws.push_back(kwd);
	      }
	      for(const libxwing2::KWd& kwd : curPltInfo.keywords)               { kws.push_back(kwd); }
	      if(shipInfo[curPltInfo.shp].yasbName.find("TIE")       != std::string::npos) { kws.push_back(libxwing2::KWd::TIE); }
	      if(shipInfo[curPltInfo.shp].yasbName.find("A-Wing")    != std::string::npos) { kws.push_back(libxwing2::KWd::Awing); }
	      if(shipInfo[curPltInfo.shp].yasbName.find("B-Wing")    != std::string::npos) { kws.push_back(libxwing2::KWd::Bwing); }
	      if(shipInfo[curPltInfo.shp].yasbName.find("X-Wing")    != std::string::npos) { kws.push_back(libxwing2::KWd::Xwing); }
	      if(shipInfo[curPltInfo.shp].yasbName.find("Y-Wing")    != std::string::npos) { kws.push_back(libxwing2::KWd::Ywing); }
	      if(shipInfo[curPltInfo.shp].yasbName.find("YT-1300")   != std::string::npos) { kws.push_back(libxwing2::KWd::YT1300); }
	      if(shipInfo[curPltInfo.shp].yasbName.find("Droid")     != std::string::npos) { kws.push_back(libxwing2::KWd::Droid); }
	      if(shipInfo[curPltInfo.shp].dps[DP::Xws].find("freighter") != std::string::npos) { kws.push_back(libxwing2::KWd::Freighter); }
	      std::sort(kws.begin(), kws.end()); kws.erase(std::unique(kws.begin(), kws.end()), kws.end()); // make unique
	      curPltInfo.dps[DP::Kwds] = GetStr(kws);
	      this->pltDb[pilot.GetPlt()] = curPltInfo.dps;
	      pilotInfo[pilot.GetPlt()] = curPltInfo;
	      found = true;
	      break;
	    }
	  }
	  if(!found) {
	    ret.push_back("Pilot not found: " + curPltInfo.dps[DP::Xws] + " (line " + std::to_string(i) + ")");
	  }
	}
      }

      else if(strhlp::StartsWith(line, "name:")) {
	std::string strName = GetStrValue(line);
	curPltInfo.yasbName = strName;
	curPltInfo.dps[DP::Xws] = Canonicalize(strName); // some use this
      }

      else if(strhlp::StartsWith(line, "canonical_name:")) {
	if(curPltInfo.dps.find(DP::Xws) == curPltInfo.dps.end()) {
	  std::string name = GetStrValue(line);
	  name = name.substr(1, name.find("\'", 1)-1);
	  curPltInfo.dps[DP::Xws] = Canonicalize(name); // some use this
	}
      }

      else if(strhlp::StartsWith(line, "charge:")) {
	curPltInfo.charge.first = GetNumValue(line);
      }
      else if(strhlp::StartsWith(line, "recurring:")) {
	curPltInfo.charge.second = GetNumValue(line);
      }

      else if(strhlp::StartsWith(line, "force:")) {
	curPltInfo.force.value = GetNumValue(line);
	curPltInfo.force.recurring = 1;
	// since 'lightside' isnt used, assume it is unless 'darkside' is specified
	//if(curPltInfo.force.affiliation == libxwing2::FAf::None) {
	//  curPltInfo.force.affiliation = libxwing2::FAf::Light;
	//}
      }

      //else if(strhlp::StartsWith(line, "darkside:")) {
      //	curPltInfo.force.affiliation = libxwing2::FAf::Dark;
      //}

      else if(strhlp::StartsWith(line, "faction:")) {
	curPltInfo.dps[DP::Fact] = GetStrValue(line);
      }

      else if(strhlp::StartsWith(line, "id:")) {
	curPltInfo.dps[DP::Yasb] = std::to_string(GetNumValue(line));
      }

      else if(strhlp::StartsWith(line, "unique:")) {
	curPltInfo.dps[DP::Limi] = "1";
      }

      else if(strhlp::StartsWith(line, "xws:")) {
	std::string xws = GetStrValue(line);
	curPltInfo.dps[DP::Xws]  = xws; // some use this
      }

      else if(strhlp::StartsWith(line, "ship:")) {
	std::string strShip = GetStrValue(line);
	for(const std::pair<const libxwing2::Shp, ShipInfo> &p : shipInfo) {
	  if(p.second.yasbName == strShip) {
	    const libxwing2::Ship ship = libxwing2::Ship::GetShip(p.first);
	    curPltInfo.shp = ship.GetShp();
	    curPltInfo.dps[DP::Ship] = ship.GetName();
	  }
	}
      }

      else if(strhlp::StartsWith(line, "skill:")) {
	curPltInfo.dps[DP::Init] = std::to_string(GetNumValue(line));
      }

      else if(strhlp::StartsWith(line, "points:")) {
	curPltInfo.dps[DP::Cost] = std::to_string(GetNumValue(line));
      }

      else if(strhlp::StartsWith(line, "keyword:")) {
	curPltInfo.keywords = GetKeywordList(line);
      }

      // parsing the actions
      else if(state == State::Actions) {
	if(line == "]") {
	  state = State::None;
	}
	else {
	  std::string aStr = line.substr(1, line.size()-2);
	  AddAction(curPltInfo.actions, aStr);
 	}
      }
      else if(strhlp::StartsWith(line, "actions:")) {
	state = State::Actions;
      }

      //upgrade bar
      else if(state == State::Slots) {
	if(line == "]") {
	  state = State::None;
	}
	else {
	  std::string uStr = line.substr(1, line.size()-2);
	  if((uStr == "Hardpoint") && ( (curPltInfo.dps[DP::Ship] == "T-70 X-wing") || (curPltInfo.dps[DP::Ship] == "M3-A Interceptor"))) {
	    // skip the hardpoint slots on T-70s and M-3s
	  } else {
	    AddUpgrade(curPltInfo.slots, uStr);
	  }
 	}
      }
      else if(strhlp::StartsWith(line, "slots: [")) {
	state = State::Slots;
      }

      // since a "skip: true" can invalidate the record or the name may not match,
      // only add properties if we have a pilot to add them to
      else if(strhlp::StartsWith(line, "skip:")) {
	curPltInfo.skip = true;
      }
    }



    // *** Upgrades ***
    if(sectionType == SectionType::Upgrade) {

      // confersAddons have { and } so dont trigger on those
      if(state == State::Conf) {
	if(line == "]") {
	  state = State::None;
	}
      }

      // new record
      else if(strhlp::StartsWith(line, "{")) {
	curUpgInfo = UpgradeInfo();
      }

      // end of record
      else if(strhlp::StartsWith(line, "}")) {
	{ // finish cost
	  if(curUpgInfo.points.size()) {
	    std::optional<libxwing2::Cost> cost;
	    switch(curUpgInfo.costType) {
	    case libxwing2::CsT::Const:
	      cost = libxwing2::Cost::ByCost(curUpgInfo.points[0]);
	      break;
	    case libxwing2::CsT::Agi:
	      cost = libxwing2::Cost::ByAgi(curUpgInfo.points[0], curUpgInfo.points[1], curUpgInfo.points[2], curUpgInfo.points[3]);
	      break;
	    case libxwing2::CsT::Base:
	      if(curUpgInfo.points.size() == 3) {
		cost = libxwing2::Cost::ByBase(curUpgInfo.points[0], curUpgInfo.points[1], curUpgInfo.points[2]);
	      }
	      else if(curUpgInfo.points.size() == 4) {
		cost = libxwing2::Cost::ByBase(curUpgInfo.points[0], curUpgInfo.points[1], curUpgInfo.points[2], curUpgInfo.points[3]);
	      }
	      break;
	    case libxwing2::CsT::Init:
	      if(curUpgInfo.points.size() == 7) {
		cost = libxwing2::Cost::ByInit(curUpgInfo.points[0], curUpgInfo.points[1], curUpgInfo.points[2], curUpgInfo.points[3],
					       curUpgInfo.points[4], curUpgInfo.points[5], curUpgInfo.points[6]);
	      }
	      else if(curUpgInfo.points.size() == 9) {
		cost = libxwing2::Cost::ByInit(curUpgInfo.points[0], curUpgInfo.points[1], curUpgInfo.points[2], curUpgInfo.points[3], curUpgInfo.points[4],
					       curUpgInfo.points[5], curUpgInfo.points[6], curUpgInfo.points[7], curUpgInfo.points[8]);
	      }
	      break;
	    }
	    if(cost) { curUpgInfo.dps[DP::Cost] = GetStr(*cost); }
	  }
	}
	if(!curUpgInfo.skip) {
	  bool found = false;
	  for(const libxwing2::Upgrade& upgrade : libxwing2::Upgrade::GetAllUpgrades()) {
	    if(SafeGetXws(upgrade.GetUpg()) == curUpgInfo.dps[DP::Xws]) {
	      // add more stuff here probably...
	      curUpgInfo.dps[DP::Hype] = "1";
	      // should probably do this sanity check for the other categories too
	      if(this->upgDb.find(upgrade.GetUpg()) == this->upgDb.end()) {
		this->upgDb[upgrade.GetUpg()] = curUpgInfo.dps;
		upgradeInfo[upgrade.GetUpg()] = curUpgInfo;
		found = true;
	      } else {
		ret.push_back("DUPE - " + curUpgInfo.yasbName);
	      }
	      break;
	    }
	  }
	  if(!found) {
	    ret.push_back("Upgrade not found: " + curUpgInfo.dps[DP::Xws] + " (line " + std::to_string(i) + ")");
	  }
	}
      }

      else if(strhlp::StartsWith(line, "name:")) {
	std::string strName = GetStrValue(line);
	curUpgInfo.yasbName = strName;
	curUpgInfo.dps[DP::Xws] = Canonicalize(strName); // some use this
      }

      else if(strhlp::StartsWith(line, "canonical_name:")) {
	std::string name = GetStrValue(line);
	name = name.substr(1, name.find("\'", 1)-1);
	curUpgInfo.dps[DP::Xws] = Canonicalize(name); // some use this
      }

      else if(strhlp::StartsWith(line, "xws:")) {
	std::string xws = GetStrValue(line);
	curUpgInfo.dps[DP::Xws]  = xws; // some use this
      }

      else if(strhlp::StartsWith(line, "ship:")) {
	std::string sStr = GetStrValue(line);
	sStr = strhlp::ReplaceStr(sStr, "\\[", "");
	sStr = strhlp::ReplaceStr(sStr, "\\]", "");
	sStr = strhlp::ReplaceStr(sStr, "\"", "");
	std::vector<std::string> toks = strhlp::Tokenize(sStr, ',');
	for(const std::string& tok : toks) {
	  curUpgInfo.ship.push_back(tok);
	}
      }

      else if(strhlp::StartsWith(line, "id:")) {
	curUpgInfo.dps[DP::Yasb] = std::to_string(GetNumValue(line));
      }

      else if(strhlp::StartsWith(line, "confersAddons: [")) {
	state = State::Conf;
      }

      else if(strhlp::StartsWith(line, "points:")) {
	std::string cost = GetStrValue(line);
	//if(cost != "'*'") {
	//  curUpgInfo.dps[DP::Cost] = cost;
	//}
	curUpgInfo.points.push_back(std::stoi(cost));
	curUpgInfo.costType = libxwing2::CsT::Const;
      }

      else if(strhlp::StartsWith(line, "pointsarray:")) {
	std::string costStr = GetStrValue(line);
	costStr = strhlp::ReplaceStr(costStr, "\\[", "");
	costStr = strhlp::ReplaceStr(costStr, "\\]", "");
	std::vector<std::string> c = strhlp::Tokenize(costStr, ',');
	for(const std::string &s : c) {
	  curUpgInfo.points.push_back(std::stoi(s));
	}
	
	/*
	std::optional<libxwing2::Cost> cost;
	switch(c.size()) {
	case 3: cost = libxwing2::Cost(std::stoi(c[0]), std::stoi(c[1]), std::stoi(c[2])); break;
	case 4: cost = libxwing2::Cost(std::stoi(c[0]), std::stoi(c[1]), std::stoi(c[2]), std::stoi(c[3])); break;
	case 7: cost = libxwing2::Cost(std::stoi(c[0]), std::stoi(c[1]), std::stoi(c[2]), std::stoi(c[3]), std::stoi(c[4]), std::stoi(c[5]), std::stoi(c[6])); break;
	}
	if(cost) {
	  curUpgInfo.dps[DP::Cost] = GetStr(*cost);
	}
	*/
      }

      else if(line ==  "variableagility: true") {
	curUpgInfo.costType = libxwing2::CsT::Agi;
      }
      else if(line ==  "variablebase: true") {
	curUpgInfo.costType = libxwing2::CsT::Base;
      }
      else if(line ==  "variableinit: true") {
	curUpgInfo.costType = libxwing2::CsT::Init;
      }
      else if(strhlp::StartsWith(line, "skip:")) {
	curUpgInfo.skip = true;
      }
    }


#ifdef INC_QUICKBUILDS
    // *** quickbuilds ***
    if(sectionType == SectionType::QuickBuild) {
      // end of quickbuild section
      if((state == State::None) && (line == "]")) {
	// take all the read data and build the qb entries
	std::function<libxwing2::QBPlt(QbInfo& qbp)> fn = [&](QbInfo& qbp)->libxwing2::QBPlt {
	  std::optional<libxwing2::Plt> plt;
	  std::vector<libxwing2::Upg> upgs;
	  for(const std::pair<const libxwing2::Plt,PilotInfo> &pi : pilotInfo) {
	    if(pi.second.yasbName == qbp.pilot) {
	      plt = pi.first;
	      break;
	    }
	  }
	  if(!plt) { printf("%s - wtf?\n", qbp.pilot.c_str()); } //throw std::runtime_error(qbp.pilot + " - wtf?"); }
	  for(const std::string &upgrade : qbp.upgrades) {
	    std::optional<libxwing2::Upg> upg;
	    for(const std::pair<const libxwing2::Upg,UpgradeInfo> &ui : upgradeInfo) {
	      if(ui.second.yasbName == upgrade) {
		upg = ui.first;
		break;
	      }
	    }
	    if(!upg) { printf("%s -> %s - wtf2?\n", qbp.pilot.c_str(), upgrade.c_str()); } //throw std::runtime_error(qbp.pilot + " -> " + upgrade + " - wtf2?"); }
	    else { upgs.push_back(*upg); }
	  }
	  return {*plt, upgs};
	};

	for(std::pair<const int,QbInfo>& qbp : qbInfo) {
	  if(!qbp.second.skip) {
	    std::pair<int, std::vector<libxwing2::QBPlt>> quickbuild;
	    if(qbp.second.threat == "*") {
	      printf("skipping %s because threat is '*'\n", qbp.second.pilot.c_str());
	      continue;
	    }
	    quickbuild.first = std::stoi(qbp.second.threat);
	    quickbuild.second.push_back(fn(qbp.second));
	    if(qbp.second.linkedId) {
	      quickbuild.second.push_back(fn(qbInfo[qbp.second.linkedId]));
	      qbInfo[qbp.second.linkedId].skip = true;
	    }
	    std::optional<libxwing2::QB> qb;
	    try {
	      qb = GetQB(quickbuild, ret); 
	    }
	    catch(libxwing2::PilotNotFound &pnf) {
	      ret.push_back(std::string("QB ERROR: ") + pnf.what());
	    }
	    if(qb) {
	      this->qbDb[*qb][DP::QuBu] = GetStr(quickbuild.first, quickbuild.second);
	      //printf("adding one\n");
	    } else {
	      //printf("no\n");
	    }
	  }
	}
      }

      // handle upgrades section (end section or add upgrade)
      else if(state == State::Upgrades) {
	if(line == "]") { state = State::None; }
	else { curQbInfo.upgrades.push_back(GetStrValue(line)); }
      }

      // new record
      else if(strhlp::StartsWith(line, "{")) {
	curQbInfo = QbInfo();
      }

      // end of record
      else if(strhlp::StartsWith(line, "}")) {
	if(!curQbInfo.skip) {
	  qbInfo[curQbInfo.id] = curQbInfo;
	}
      }

      // fields to read
      else if(strhlp::StartsWith(line, "id:"))         { curQbInfo.id = GetNumValue(line); }
      else if(strhlp::StartsWith(line, "faction:"))    { curQbInfo.faction = GetStrValue(line); }
      else if(strhlp::StartsWith(line, "pilot:"))      { curQbInfo.pilot = GetStrValue(line); }
      else if(strhlp::StartsWith(line, "suffix:"))     { curQbInfo.suffix = GetStrValue(line); }
      else if(strhlp::StartsWith(line, "linkedId:"))   { curQbInfo.linkedId = GetNumValue(line); }
      else if(strhlp::StartsWith(line, "ship:"))       { curQbInfo.ship = GetStrValue(line); }
      else if(strhlp::StartsWith(line, "threat:"))     { curQbInfo.threat = GetStrValue(line); }
      else if(strhlp::StartsWith(line, "upgrades: [")) { state = State::Upgrades; }
      else if(strhlp::StartsWith(line, "skip:"))       { curQbInfo.skip = true; }
      // i dont currently support the Epic Battles quickbuilds, so just skip them
      else if(strhlp::StartsWith(line, "wingmate:"))   { curQbInfo.skip = true; }
      else if(strhlp::StartsWith(line, "wingmates:"))  { curQbInfo.skip = true; }
      else if(strhlp::StartsWith(line, "wingleader:")) { curQbInfo.skip = true; }
    }
#endif


    // *** hyperspace ships ***
    if(sectionType == SectionType::HyperspaceShipInclude) {
      if(line == "]") {
	sectionType = SectionType::Unknown;
      }
      else {
	std::string shipStr;
	std::string factionStr;
	{
	  std::string l = line;
	  if(l[l.size()-1] == ',') { l = l.substr(0, l.size()-1); }
	  l = strhlp::ReplaceStr(l, "\\{", "");
	  l = strhlp::ReplaceStr(l, "\\}", "");
	  std::vector<std::string> toks = strhlp::Tokenize(l, ',');
	  std::vector<std::string> sToks = strhlp::Tokenize(toks[0], ':');
	  shipStr = strhlp::Trim(sToks[1]);
	  shipStr = strhlp::ReplaceStr(shipStr, "'", "");
	  std::vector<std::string> fToks = strhlp::Tokenize(toks[1], ':');
	  factionStr = strhlp::Trim(fToks[1]);
	  factionStr = strhlp::ReplaceStr(factionStr, "'", "");
	}

	std::optional<libxwing2::Ship> ship;
	for(std::pair<const libxwing2::Shp, ShipInfo>& si : shipInfo) {
	  if(si.second.yasbName == shipStr) {
	    ship = libxwing2::Ship::GetShip(si.first);
	    si.second.hyperspace = true;
	    break;
	  }
	}

	// enable pilot
	if(ship) {
	  for(auto & dps : this->pltDb) {
	    if((ship->GetName() == dps.second.at(DP::Ship)) && (factionStr == dps.second.at(DP::Fact))) {
	      dps.second.at(DP::Hype) = "1";
	    }
	  }
	}
      }
    }

    // *** hyperspace exclude pilots ***
    if(sectionType == SectionType::HyperspacePilotExclude) {
      if(line == "]") {
	sectionType = SectionType::Unknown;
      }
      else {
	std::string pilotStr;
	{
	  pilotStr = line;
	  pilotStr = strhlp::ReplaceStr(pilotStr, ",", "");
	  if(pilotStr.length() > 2) {
	    pilotStr = pilotStr.substr(1, pilotStr.length()-2);
	  }
	}
	std::optional<libxwing2::Plt> plt;	
	for(std::pair<const libxwing2::Plt, PilotInfo>& pi : pilotInfo) {
  	  if(pi.second.yasbName == pilotStr) {
	    plt = pi.first;
	    break;
	  }
	}
	if(plt) {
	  this->pltDb.at(*plt).at(DP::Hype) = "0";
	}
      }
    }

    // *** hyperspace exclude upgrades ***
    if(sectionType == SectionType::HyperspaceUpgradeExclude) {
      if(line == "]") {
	sectionType = SectionType::Unknown;
      }
      else {
	if(line=="" || line[0] == '#') { continue; }
	std::string upgradeStr;
	{
	  upgradeStr = line;
	  upgradeStr = strhlp::ReplaceStr(upgradeStr, ",", "");
	  if(upgradeStr.length() > 2) {
	    upgradeStr = upgradeStr.substr(1, upgradeStr.length()-2);
	  }
	}
	std::optional<libxwing2::Upg> upg;	
	for(std::pair<const libxwing2::Upg, UpgradeInfo>& ui : upgradeInfo) {
	  if(ui.second.yasbName == upgradeStr) {
	    upg = ui.first;
	    break;
	  }
	}
	if(upg) {
	  this->upgDb.at(*upg).at(DP::Hype) = "0";
	}
      }
    }
  }

  // disable hyperspace for 'ship:' cards on non-hyperspace ships
  for(std::pair<const libxwing2::Upg, UpgradeInfo>& ui : upgradeInfo) {
    if(ui.second.ship.size()) {
      bool isHyper = false;
      for(const std::string& shp : ui.second.ship) {
    	for(std::pair<const libxwing2::Shp, ShipInfo>& si : shipInfo) {
	  if((shp == si.second.yasbName) && si.second.hyperspace){
	    isHyper = true;
	    break;
	  }
	}
      }
      if(!isHyper) {
	this->upgDb.at(ui.first).at(DP::Hype) = "0";
      }
    }
    // while we are looping through them, disable hyperspace on all huge/epic upgrades
    if(libxwing2::Upgrade::GetUpgrade(ui.first).IsEpicOnly()) {
      this->upgDb.at(ui.first).at(DP::Hype) = "0";
    }
  }

  this->PopulateDps();
  return ret;
}

}

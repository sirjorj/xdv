#include "xd2.h"
#include "strhlp.h"
#include "json/json.h"
#include <libxwing2/converter.h>
#include <fstream>
#include <iostream>
#include <regex>
#include <dirent.h>

#if 0
#define LINECOLOR "\e[0;97m\u001b[44m"
#define NORMCOLOR "\e[0m"
#define DBG(fmt)       printf(fmt)
#define DBGV(fmt, ...) printf(fmt, __VA_ARGS__)
#else
#define DBG(fmt)
#define DBGV(fmt, ...)
#endif

namespace xdv::xd2 {

static std::string ConvertCardText(std::string text) {
  std::string t = text;
  std::vector<std::pair<std::string,std::string>> conv = {
    { "\\. ",                     ".  "               },
    { "Action:  ",                "Action: "          },
    { "\\.\" ",                   ".\"  "             },  //stupid Moldy Crow formatting...
    { "^Bomb ",                   "Bomb  "            },
    { "^Mine ",                   "Mine  "            },
    { "^Offline ",                "Offline  "         },
    { "\\[Astromech\\]",          "{ASTROMECH}"       },
    { "\\[Barrel Roll\\]",        "{BARRELROLL}"      },
    { "\\[Boost\\]",              "{BOOST}"           },
    { "\\[Bullseye Arc\\]",       "{BULLSEYEARC}"     },
    { "\\[Calculate\\]",          "{CALCULATE}"       },
    { "\\[Cannon\\]",             "{CANNON}"          },
    { "\\[Cargo\\]",              "{CARGO}"           },
    { "\\[Cloak\\]",              "{CLOAK}"           },
    { "\\[Coordinate\\]",         "{COORDINATE}"      },
    { "\\[Charge\\]",             "{CHARGE}"          },
    { "\\[Configuration\\]",      "{CONFIG}"          },
    { "\\[Crew\\]",               "{CREW}"            },
    { "\\[Critical Hit\\]",       "{CRIT}"            },
    { "\\[Device\\]",             "{PAYLOAD}"         },
    { "\\[Energy\\]",             "{ENERGY}"          },
    { "\\[Evade\\]",              "{EVADE}"           },
    { "\\[Focus\\]",              "{FOCUS}"           },
    { "\\[Force\\]",              "{FORCE}"           },
    { "\\[Front Arc\\]",          "{FRONTARC}"        },
    { "\\[Full Front Arc\\]",     "{FULLFRONTARC}"    },
    { "\\[Full Rear Arc\\]",      "{FULLREARARC}"     },
    { "\\[Gunner\\]",             "{GUNNER}"          },
    { "\\[Hardpoint\\]",          "{HARDPOINT}"       },
    { "\\[Hit\\]",                "{HIT}"             },
    { "\\[Illicit\\]",            "{ILLICIT}"         },
    { "\\[Jam\\]",                "{JAM}"             },
    { "\\[Koiogran Turn\\]",      "{KTURN}"           },
    { "\\[Bank Left\\]",          "{LBANK}"           },
    { "\\[Left Arc\\]",           "{LEFTARC}"         },
    { "\\[Segnor's Loop Left\\]", "{LSLOOP}"          },
    { "\\[Tallon Roll Left\\]",   "{LTROLL}"          },
    { "\\[Turn Left\\]",          "{LTURN}"           },
    { "\\[Lock\\]",               "{LOCK}"            },
    { "\\[Missile\\]",            "{MISSILE}"         },
    { "\\[Modification\\]",       "{MODIFICATION}"    },
    { "\\[Bank Right\\]",         "{RBANK}"           },
    { "\\[Reinforce\\]",          "{REINFORCE}"       },
    { "\\[Right Arc\\]",          "{RIGHTARC}"        },
    { "\\[Rotate Arc\\]",         "{ROTATEARC}"       },
    { "\\[Segnor's Loop Right\\]","{RSLOOP}"          },
    { "\\[Shield\\]",             "{SHIELD}"          },
    { "\\[Tallon Roll Right\\]",  "{RTROLL}"          },
    { "\\[Team\\]",               "{TEAM}"            },
    { "\\[Turn Right\\]",         "{RTURN}"           },
    { "\\[Rear Arc\\]",           "{REARARC}"         },
    { "\\[Reload\\]",             "{RELOAD}"          },
    { "\\[Sensor\\]",             "{SENSOR}"          },
    { "\\[Single Turret Arc\\]",  "{SINGLETURRETARC}" },
    { "\\[SLAM\\]",               "{SLAM}"            },
    { "\\[Stationary\\]",         "{STATIONARY}"      },
    { "\\[Straight\\]",           "{STRAIGHT}"        },
    { "\\[Talent\\]",             "{TALENT}"          },
    { "\\[Torpedo\\]",            "{TORPEDO}"         },
    { "\\[Turret\\]",             "{TURRET}"          },
  };
  for(const std::pair<std::string,std::string>& c : conv) {
    t = strhlp::ReplaceStr(t, c.first, c.second);
  }
  return t;
}

static libxwing2::BSz GetBSz(const Json::Value& jShip) {
  std::string sizeStr = jShip.get("size","").asString();
  if     (sizeStr == "Small")  { return libxwing2::BSz::Small;  }
  else if(sizeStr == "Medium") { return libxwing2::BSz::Medium; }
  else if(sizeStr == "Large")  { return libxwing2::BSz::Large;  }
  else if(sizeStr == "Huge")   { return libxwing2::BSz::Huge;  }
  return libxwing2::BSz::None;
}

static libxwing2::Maneuvers GetManeuvers(const Json::Value& jMan) {
  libxwing2::Maneuvers ret;
  for(Json::ValueConstIterator iMan = jMan.begin(); iMan != jMan.end() ; iMan++ ) {
    std::string cur = iMan->asString();
    std::string speedStr = cur.substr(0,1);
    std::string bearStr = cur.substr(1,1);
    std::string diffStr = cur.substr(2,1);

    int8_t speed = std::stoi(speedStr);

    libxwing2::Brn bearing;
    if     (bearStr == "T") { bearing = libxwing2::Brn::LTurn; }
    else if(bearStr == "B") { bearing = libxwing2::Brn::LBank; }
    else if(bearStr == "F") { bearing = libxwing2::Brn::Straight; }
    else if(bearStr == "N") { bearing = libxwing2::Brn::RBank; }
    else if(bearStr == "Y") { bearing = libxwing2::Brn::RTurn; }
    else if(bearStr == "K") { bearing = libxwing2::Brn::KTurn; }
    else if(bearStr == "O") { bearing = libxwing2::Brn::Stationary; }
    else if(bearStr == "L") { bearing = libxwing2::Brn::LSloop; }
    else if(bearStr == "P") { bearing = libxwing2::Brn::RSloop; }
    else if(bearStr == "E") { bearing = libxwing2::Brn::LTroll; }
    else if(bearStr == "R") { bearing = libxwing2::Brn::RTroll; }
    else if(bearStr == "A") { bearing = libxwing2::Brn::RevLBank; }
    else if(bearStr == "S") { bearing = libxwing2::Brn::RevStraight; }
    else if(bearStr == "D") { bearing = libxwing2::Brn::RevRBank; }
    else {
      printf("Unknown bearing '%s'\n", bearStr.c_str());
      continue;
    }

    libxwing2::Dif difficulty;
    if     (diffStr == "B") { difficulty = libxwing2::Dif::Blue; }
    else if(diffStr == "W") { difficulty = libxwing2::Dif::White; }
    else if(diffStr == "R") { difficulty = libxwing2::Dif::Red; }
    else if(diffStr == "P") { difficulty = libxwing2::Dif::Purple; }
    else {
      printf("Unknown Difficulty '%s'\n", diffStr.c_str());
      continue;
    }

    ret.push_back({speed, bearing, difficulty});
  }
  return ret;
}

static std::optional<libxwing2::Faction> GetFaction(const Json::Value& jShip, std::vector<std::string>& err) {
  std::string factionStr = jShip.get("faction","").asString();
  for(const libxwing2::Faction& faction : libxwing2::Faction::GetAllFactions()) {
    if(factionStr == faction.GetName()) {
      return faction;
    }
  }
  err.push_back("Unknown Faction " + factionStr);
  return std::nullopt;
}

static std::optional<libxwing2::Ship> GetShip(const Json::Value& jShip, std::vector<std::string>& err) {
  std::string shipStr = jShip.get("name","").asString();
  for(const libxwing2::Ship& ship : libxwing2::Ship::GetAllShips()) {
    if(shipStr == ship.GetName()) {
      return ship;
    }
  }
  try {
    std::string shipXwsStr = jShip.get("xws","").asString();
    return libxwing2::Ship::GetShip(libxwing2::converter::xws::GetShip(shipXwsStr));
  }
  catch(libxwing2::converter::xws::ShipNotFound &snf) { }
  err.push_back("Unknown Ship " + shipStr);
  return std::nullopt;
}

static std::optional<libxwing2::SAct> GetSAct(const Json::Value& jAction, std::vector<std::string>& err) {
  std::string actStr = jAction.get("type","").asString();
  std::string difStr = jAction.get("difficulty","").asString();

  std::optional<libxwing2::Act> act;
  std::optional<libxwing2::Dif> dif;

  for(const libxwing2::Action& action : libxwing2::Action::GetAllActions()) {
    if(action.GetName() == actStr) {
      act = action.GetAct();
    }
  }
  if(!act) {
    err.push_back("Unknown Action " + actStr);
    return std::nullopt;
  }

  for(const libxwing2::Difficulty& difficulty : libxwing2::Difficulty::GetAllDifficulties()) {
    if(difficulty.GetName() == difStr) {
      dif = difficulty.GetDif();
    }
  }
  if(!dif) {
    err.push_back("Unknown Difficulty " + difStr);
    return std::nullopt;
  }
  return {{*dif,*act}};
}

static std::optional<libxwing2::ActionBar> GetActionBar(const Json::Value& jActions, std::vector<std::string>& err) {
  libxwing2::ActionBar ab;

  for(Json::ValueConstIterator iAct = jActions.begin(); iAct != jActions.end() ; iAct++ ) {

    std::optional<libxwing2::SAct> sact = GetSAct(*iAct, err);
    if(!sact) { return std::nullopt; }

    if(iAct->isMember("linked")) {
      std::optional<libxwing2::SAct> lSact = GetSAct((*iAct)["linked"], err);
      if(!lSact) { return std::nullopt; }
      ab.push_back({*sact, *lSact});
    } else {
      ab.push_back({*sact});
    }
  }
  return ab;
}

static std::optional<libxwing2::UpgradeBar> GetUpgradeBar(const Json::Value& jSlots, std::vector<std::string>& err) {
  libxwing2::UpgradeBar ub;
  for(Json::ValueConstIterator iSlt = jSlots.begin(); iSlt != jSlots.end() ; iSlt++ ) {
    std::string uptStr = iSlt->asString();
    if(uptStr == "Device") { uptStr = "Payload"; }

    std::optional<libxwing2::UpgradeType> upt;

    for(const libxwing2::UpgradeType& upgradeType : libxwing2::UpgradeType::GetAllUpgradeTypes()) {
      if(upgradeType.GetName() == uptStr) {
	upt = upgradeType;
      }
    }
    if(!upt) {
      err.push_back("Unknown UpgradeType " + uptStr);
    } else {
      ub.push_back({upt->GetUpT()});
    }
  }
  return ub;
}

static std::optional<std::vector<libxwing2::UpT>> GetUpgradeSlots(const Json::Value& jSlots
								  , std::vector<std::string>& err) {
  std::vector<libxwing2::UpT> us;
  for(Json::ValueConstIterator iSlt = jSlots.begin(); iSlt != jSlots.end() ; iSlt++ ) {
    std::string uptStr = iSlt->asString();
    if(uptStr == "Device") { uptStr = "Payload"; }

    std::optional<libxwing2::UpgradeType> upt;

    for(const libxwing2::UpgradeType& upgradeType : libxwing2::UpgradeType::GetAllUpgradeTypes()) {
      if(upgradeType.GetName() == uptStr) {
	upt = upgradeType;
      }
    }
    if(!upt) {
      err.push_back("Unknown UpgradeType " + uptStr);
    }
    us.push_back({upt->GetUpT()});
  }
  return us;
}

static std::optional<libxwing2::Pilot> GetPilot(const Json::Value& jPilot, libxwing2::Fac f, libxwing2::Shp s, std::vector<std::string>& err) {
  std::string pilotStr = jPilot.get("name","").asString();
  std::string subtitle = jPilot.get("caption","").asString();
  for(const libxwing2::Pilot& pilot : libxwing2::Pilot::GetAllPilots()) {
    if((pilotStr == pilot.GetName()) && (subtitle == pilot.GetSubtitle()) && (f == pilot.GetFac()) && (s == pilot.GetShp())) {
      return pilot;
    }
  }
  std::string pilotXwsStr = jPilot.get("xws","").asString();
  if(pilotXwsStr != "") {
    try {
      return libxwing2::Pilot::GetPilot(libxwing2::converter::xws::GetPilot(pilotXwsStr));
    }
    catch(libxwing2::converter::xws::PilotNotFound &pnf) { }
  }
  err.push_back("Unknown Pilot " + pilotStr);
  return std::nullopt;
}



static libxwing2::PriAttacks GetAttacks(const Json::Value& jStats, std::vector<std::string>& err) {
  std::vector<libxwing2::PriAttack> ret;

  for(Json::ValueConstIterator iStat = jStats.begin(); iStat != jStats.end() ; iStat++ ) {
    if(iStat->get("type","").asString() == "attack") {
      std::string arcStr = iStat->get("arc","").asString();
      uint8_t val = iStat->get("value",0).asInt();
      libxwing2::Arc arc;
      if     (arcStr == "Front Arc")          { arc = libxwing2::Arc::Front;        }
      else if(arcStr == "Rear Arc")           { arc = libxwing2::Arc::Rear;         }
      else if(arcStr == "Left Arc")           { arc = libxwing2::Arc::Left;         }
      else if(arcStr == "Right Arc")          { arc = libxwing2::Arc::Right;        }
      else if(arcStr == "Full Front Arc")     { arc = libxwing2::Arc::FullFront;    }
      else if(arcStr == "Single Turret Arc")  { arc = libxwing2::Arc::SingleTurret; }
      else if(arcStr == "Double Turret Arc")  { arc = libxwing2::Arc::DoubleTurret; }
      else if(arcStr == "Bullseye Arc")       { arc = libxwing2::Arc::Bullseye;     }
      else {
	err.push_back("Unknown Arc " + arcStr);
	continue;
      }
      ret.push_back({arc, val});
    }
  }
  return ret;
}

static int8_t GetAgility(const Json::Value& jStats, std::vector<std::string>& err) {
  for(Json::ValueConstIterator iStat = jStats.begin(); iStat != jStats.end() ; iStat++ ) {
    if(iStat->get("type","").asString() == "agility") {
      return iStat->get("value",0).asInt();
    }
  }
  return 0;
}

static int8_t GetHull(const Json::Value& jStats, std::vector<std::string>& err) {
  for(Json::ValueConstIterator iStat = jStats.begin(); iStat != jStats.end() ; iStat++ ) {
    if(iStat->get("type","").asString() == "hull") {
      return iStat->get("value",0).asInt();
    }
  }
  return 0;
}

static libxwing2::Chargeable GetShield(const Json::Value& jStats, std::vector<std::string>& err) {
  for(Json::ValueConstIterator iStat = jStats.begin(); iStat != jStats.end() ; iStat++ ) {
    if(iStat->get("type","").asString() == "shields") {
      const int8_t val = iStat->get("value",0).asInt();
      const int8_t rec = iStat->get("recovers",0).asInt();
      return { val, rec };
    }
  }
  return {0,0};
}

static libxwing2::FAf GetForceAffinity(const Json::Value& jAffs) {

  for(Json::ValueConstIterator iAff = jAffs.begin(); iAff != jAffs.end() ; iAff++ ) {
    std::string af = iAff->asString();
    if(false) { }
    else if(af == "dark")  { return libxwing2::FAf::Dark;  }
    else if(af == "light") { return libxwing2::FAf::Light; }
    else if(af == "none")  { return libxwing2::FAf::None;  }
  }
  return libxwing2::FAf::None;
}

static std::string GetShipAbility(const Json::Value& jPilot) {
  if(jPilot.isMember("shipAbility")) {
    return jPilot["shipAbility"].get("name","").asString() + " - " + ConvertCardText(jPilot["shipAbility"].get("text","").asString());
  }
  else {
    return "";
  }
}

static libxwing2::KeywordList GetKeywords(const Json::Value& jKey) {
  libxwing2::KeywordList ret;
  for(Json::ValueConstIterator iKey = jKey.begin(); iKey != jKey.end(); iKey++) {
    std::string keyword = iKey->asString();
    if     (keyword == "A-wing")        { ret.push_back(libxwing2::KWd::Awing);        }
    else if(keyword == "B-wing")        { ret.push_back(libxwing2::KWd::Bwing);        }
    else if(keyword == "Bounty Hunter") { ret.push_back(libxwing2::KWd::BountyHunter); }
    else if(keyword == "Clone")         { ret.push_back(libxwing2::KWd::Clone);        }
    else if(keyword == "Dark Side")     { ret.push_back(libxwing2::KWd::DarkSide);     }
    else if(keyword == "Droid")         { ret.push_back(libxwing2::KWd::Droid);        }
    else if(keyword == "Freighter")     { ret.push_back(libxwing2::KWd::Freighter);    }
    else if(keyword == "Jedi")          { ret.push_back(libxwing2::KWd::Jedi);         }
    else if(keyword == "Light Side")    { ret.push_back(libxwing2::KWd::LightSide);    }
    else if(keyword == "Mandalorian")   { ret.push_back(libxwing2::KWd::Mandalorian);  }
    else if(keyword == "Partisan")      { ret.push_back(libxwing2::KWd::Partisan);     }
    else if(keyword == "Sith")          { ret.push_back(libxwing2::KWd::Sith);         }
    else if(keyword == "Spectre")       { ret.push_back(libxwing2::KWd::Spectre);      }
    else if(keyword == "TIE")           { ret.push_back(libxwing2::KWd::TIE);          }
    else if(keyword == "X-wing")        { ret.push_back(libxwing2::KWd::Xwing);        }
    else if(keyword == "YT-1300")       { ret.push_back(libxwing2::KWd::YT1300);       }
    else if(keyword == "Y-wing")        { ret.push_back(libxwing2::KWd::Ywing);        }
    else                                { printf("Unknown keyword '%s'\n", keyword.c_str()); }
  }
  return ret;
}

static std::optional<libxwing2::UpT> GetUpt(const Json::Value& jUpgrade, std::vector<std::string>& err) {
  std::string uptStr = jUpgrade["sides"][0].get("type","").asString();
  if(uptStr == "Device") { uptStr = "Payload"; }
  for(const libxwing2::UpgradeType& upgradeType : libxwing2::UpgradeType::GetAllUpgradeTypes()) {
    if(uptStr == upgradeType.GetName()) {
      return upgradeType.GetUpT();
    }
  }
  err.push_back("Unknown UpgradeType " + uptStr);
  return std::nullopt;
}

static std::optional<libxwing2::Fac> GetUFac(const Json::Value& jUpgrade, std::vector<std::string>& err) {
  if(jUpgrade.isMember("restrictions")) {
    Json::Value restrictions = jUpgrade["restrictions"];
    for(int i=0; i<restrictions.size(); i++) {
      if(restrictions[i].isMember("factions")) {
	libxwing2::Fac ret = libxwing2::Fac::None;
	Json::Value factions = restrictions[i]["factions"];
	for(int j=0; j<factions.size(); j++) {
	  std::string factionStr = factions[j].asString();
	  for(const libxwing2::Faction &faction : libxwing2::Faction::GetAllFactions()) {
	    if(factionStr == faction.GetName()) {
	      ret = ret | faction.GetFac();
	    }
	  }
	}
	return ret;
      }
    }
  }
  return libxwing2::Fac::All;
}

static std::optional<libxwing2::Upgrade> GetUpgrade(const Json::Value& jUpgrade, libxwing2::UpT t, libxwing2::Fac f, std::vector<std::string>& err) {
  std::string upgradeStr = jUpgrade.get("name","").asString();
  for(const libxwing2::Upgrade& upgrade : libxwing2::Upgrade::GetAllUpgrades()) {
    if((upgrade.GetName() == upgradeStr) && (upgrade.GetUpT() == t) && (upgrade.GetRestriction().GetFactions() == f)) {
      return upgrade;
    }
  }
  std::string upgradeXwsStr = jUpgrade.get("xws","").asString();
  if(upgradeXwsStr != "") {
    try{
      return libxwing2::Upgrade::GetUpgrade(libxwing2::converter::xws::GetUpgrade(upgradeXwsStr));
    }
    catch(libxwing2::converter::xws::UpgradeNotFound &unf) { }
  }
  err.push_back("Unknown Upgrade " + upgradeStr);
  return std::nullopt;
}

static std::optional<libxwing2::Cost> GetCost(const Json::Value& jCost) {
  if(jCost.isMember("value")) {
    return libxwing2::Cost::ByCost(jCost.get("value",0).asInt());
  }
  else if(jCost.isMember("values")) {
    Json::Value jValues = jCost["values"];
    if(jCost.get("variable","").asString() == "agility") {
      return libxwing2::Cost::ByAgi(jValues.get("0",0).asInt(),
				    jValues.get("1",0).asInt(),
				    jValues.get("2",0).asInt(),
				    jValues.get("3",0).asInt());
    }
    else if(jCost.get("variable","").asString() == "size") {
      if(jValues.size() == 3) {
	return libxwing2::Cost::ByBase(jValues.get("Small",0).asInt(),
				       jValues.get("Medium",0).asInt(),
				       jValues.get("Large",0).asInt());
      } else {
	return libxwing2::Cost::ByBase(jValues.get("Small",0).asInt(),
				       jValues.get("Medium",0).asInt(),
				       jValues.get("Large",0).asInt(),
				       jValues.get("Huge",0).asInt());
      }
      
    }
    else if(jCost.get("variable","").asString() == "initiative") {
      if(jValues.size() == 9) {
	return libxwing2::Cost::ByInit(jValues.get("0",0).asInt(),
				       jValues.get("1",0).asInt(),
				       jValues.get("2",0).asInt(),
				       jValues.get("3",0).asInt(),
				       jValues.get("4",0).asInt(),
				       jValues.get("5",0).asInt(),
				       jValues.get("6",0).asInt(),
				       jValues.get("7",0).asInt(),
				       jValues.get("8",0).asInt());
      } else {
	return libxwing2::Cost::ByInit(jValues.get("0",0).asInt(),
				       jValues.get("1",0).asInt(),
				       jValues.get("2",0).asInt(),
				       jValues.get("3",0).asInt(),
				       jValues.get("4",0).asInt(),
				       jValues.get("5",0).asInt(),
				       jValues.get("6",0).asInt());
      }
    }
  }
  return std::nullopt;
}


#ifdef INC_QUICKBUILDS  
std::pair<int, std::vector<libxwing2::QBPlt>> GetQuickBuild(const Json::Value& jQb) {
  int threat = jQb.get("threat", 0).asInt();
  std::vector<libxwing2::QBPlt> plts;
  for(Json::ValueConstIterator iPlt = jQb["pilots"].begin(); iPlt != jQb["pilots"].end() ; iPlt++ ) {
    libxwing2::QBPlt p;
    std::string xwsPilot = iPlt->get("id","").asString();
    try {
      p.plt = libxwing2::converter::xws::GetPilot(xwsPilot);
    }
    catch(libxwing2::converter::xws::PilotNotFound& pnf) {
      break;
    }
    // for each upgrade type
    if(iPlt->isMember("upgrades")) {
      for(const std::string& t : iPlt->get("upgrades","").getMemberNames()) {
	// for each upgrade
	for(Json::ValueConstIterator iUpg = (*iPlt)["upgrades"][t].begin(); iUpg != (*iPlt)["upgrades"][t].end() ; iUpg++ ) {
	  try {
	    p.upgs.push_back(libxwing2::converter::xws::GetUpgrade(iUpg->asString()));
	  }
	  catch(libxwing2::converter::xws::UpgradeNotFound& unf) {
	  }
	}
      }
    }
    plts.push_back(p);
  }
  return {threat, plts};
}
#endif


XD2DS::XD2DS()
  : DataSrc("XD2") {
}

std::vector<std::string> XD2DS::Load() {
  std::vector<std::string> ret;
  const char* SUFX = ".json";
  { // version
    const char* PATH = "xwing-data2/data/manifest.json";
    std::ifstream file(PATH);
    Json::Value root;
    file >> root;
    this->version = root.get("version","").asString();
  }

  { // ships
    const char* PATH = "xwing-data2/data/pilots/";
    std::vector<std::string> files = strhlp::GetFilePaths(PATH, SUFX);
    for(const std::string& f : files) {
      std::ifstream file(f);
      Json::Value jRoot;
      file >> jRoot;

      std::optional<libxwing2::Ship> gShip = GetShip(jRoot, ret);
      if(!gShip) { continue; }

      libxwing2::Shp shp = gShip->GetShp();
      this->shpDb[shp][DP::Name] = jRoot.get("name","").asString();
      this->shpDb[shp][DP::Xws] = jRoot.get("xws","").asString();
      this->shpDb[shp][DP::Size] = libxwing2::BaseSize::GetBaseSize(GetBSz(jRoot)).GetName();
      this->shpDb[shp][DP::Manv] = GetStr(GetManeuvers(jRoot["dial"]));
    }
  }

  { // pilots
    const char* PATH = "xwing-data2/data/pilots/";
    std::vector<std::string> files = strhlp::GetFilePaths(PATH, SUFX);
    for(const std::string& f : files) {
      std::ifstream file(f);
      Json::Value jRoot;
      file >> jRoot;

      // ship info...
      // get the faction
      std::optional<libxwing2::Faction> faction = GetFaction(jRoot, ret);
      if(!faction) { continue; }
      // get the ship
      std::optional<libxwing2::Ship> ship = GetShip(jRoot, ret);
      if(!ship) { continue; }
      // get the default action bar
      std::optional<libxwing2::ActionBar> ab = GetActionBar(jRoot["actions"], ret);
      if(!ab) { continue; }
      // stats
      libxwing2::PriAttacks att = GetAttacks(jRoot["stats"], ret);
      int8_t agi = GetAgility(jRoot["stats"], ret);
      int8_t hul = GetHull(jRoot["stats"], ret);
      libxwing2::Chargeable shl = GetShield(jRoot["stats"], ret);

      // pilot info...
      for(int i=0; i<jRoot["pilots"].size(); i++) {
	Json::Value jPilot = jRoot["pilots"][i];

	std::optional<libxwing2::ActionBar> sab;
	if(jPilot.isMember("shipActions")) {
	  sab = GetActionBar(jPilot["shipActions"], ret);
	  if(!sab) { continue; }
	}
	std::optional<libxwing2::UpgradeBar> ub = GetUpgradeBar(jPilot["slots"], ret);

	std::optional<libxwing2::Pilot> gPilot = GetPilot(jPilot, faction->GetFac(), ship->GetShp(), ret);
	
	if(gPilot && ub) {
	  libxwing2::Plt plt = gPilot->GetPlt();
	  this->pltDb[plt][DP::Abil] = std::to_string(jPilot.isMember("ability"));
	  this->pltDb[plt][DP::Acti] = sab ? GetStr(*sab) : GetStr(*ab);
	  this->pltDb[plt][DP::Agil] = std::to_string(agi);
	  this->pltDb[plt][DP::ArtC] = jPilot.get("image", "").asString();
	  this->pltDb[plt][DP::ArtI] = jPilot.get("artwork", "").asString();
  	  this->pltDb[plt][DP::Atck] = GetStr(att);
  	  this->pltDb[plt][DP::Chrg] = GetStr({(int8_t)jPilot["charges"].get("value",0).asInt(), jPilot["charges"].get("recovers",0).asBool()});
	  this->pltDb[plt][DP::Cost] = std::to_string(jPilot.get("cost",200).asInt());
	  this->pltDb[plt][DP::Enga] = std::to_string(jPilot.get("engagement",jPilot.get("initiative","").asInt()).asInt());
	  this->pltDb[plt][DP::Fact] = faction->GetName();
	  this->pltDb[plt][DP::Ffg]  = std::to_string(jPilot.get("ffg",0).asInt());
	  this->pltDb[plt][DP::Forc] = GetStr({(int8_t)jPilot["force"].get("value",0).asInt(), jPilot["force"].get("recovers",0).asBool(), GetForceAffinity(jPilot["force"]["side"])});
	  this->pltDb[plt][DP::Hull] = std::to_string(hul);
	  this->pltDb[plt][DP::Hype] = std::to_string(jPilot.get("hyperspace",false).asBool());
	  this->pltDb[plt][DP::Init] = std::to_string(jPilot.get("initiative","").asInt());
	  this->pltDb[plt][DP::Kwds] = GetStr(GetKeywords(jPilot["keywords"]));
	  this->pltDb[plt][DP::Limi] = std::to_string(jPilot.get("limited",0).asInt());
	  this->pltDb[plt][DP::Name] = jPilot.get("name","").asString();
	  this->pltDb[plt][DP::ShAb] = GetShipAbility(jPilot);
	  this->pltDb[plt][DP::Ship] = ship->GetName();
	  this->pltDb[plt][DP::Shld] = "[" + std::to_string(shl.GetCapacity()) + "|"+ std::to_string(shl.GetRecurring())+"]";
	  this->pltDb[plt][DP::Subt] = jPilot.get("caption","").asString();
	  this->pltDb[plt][DP::Text] = jPilot.isMember("ability")
	                               ? ConvertCardText(jPilot.get("ability","").asString())
	                               : ConvertCardText(jPilot.get("text","").asString());
	  this->pltDb[plt][DP::UBar] = GetStr(*ub);
	  this->pltDb[plt][DP::Xws]  = jPilot.get("xws","").asString();
	}
      }
    }
  }

  { // upgrades
    const char* PATH = "xwing-data2/data/upgrades/";
    const char* SUFX = ".json";
    std::vector<std::string> files = strhlp::GetFilePaths(PATH, SUFX);

    for(const std::string& f : files) {
      std::ifstream file(f);
      Json::Value jRoot;
      file >> jRoot;

      for(int i=0; i<jRoot.size(); i++) {
	Json::Value jUpgrade = jRoot[i];
	// get type and factions
	std::optional<libxwing2::UpT> upt = GetUpt(jUpgrade, ret);
	if(!upt) { continue; }

	std::optional<libxwing2::Fac> fac = GetUFac(jUpgrade, ret);
	if(!fac) { continue; }

	std::optional<libxwing2::Upgrade> gUpgrade = GetUpgrade(jUpgrade, *upt, *fac, ret);

	if(gUpgrade) {
	  std::optional<libxwing2::Cost> c = GetCost(jUpgrade["cost"]);
	  
	  libxwing2::Upg upg = gUpgrade->GetUpg();
	  Json::Value jSides = jUpgrade["sides"];
	  Json::Value jSide = jSides[0];
	  this->upgDb[upg][DP::Abil] = std::to_string(jSide.isMember("ability"));
	  this->upgDb[upg][DP::ArtC] = jSide.get("image", "").asString();
	  this->upgDb[upg][DP::ArtI] = jSide.get("artwork", "").asString();
	  this->upgDb[upg][DP::Cost] = c ? GetStr(*c) : "200";
	  this->upgDb[upg][DP::Ffg]  = std::to_string(jSide.get("ffg",0).asInt());
	  this->upgDb[upg][DP::Hype] = std::to_string(jUpgrade.get("hyperspace", "0").asBool());
	  this->upgDb[upg][DP::Limi] = std::to_string(jUpgrade.get("limited", "").asInt());
	  this->upgDb[upg][DP::Name] = jUpgrade.get("name", "").asString();
	  this->upgDb[upg][DP::Slot] = GetStr(*GetUpgradeSlots(jSide["slots"], ret));
	  this->upgDb[upg][DP::Text] = (jSide.isMember("ability"))
	                               ? ConvertCardText(jSide.get("ability","").asString())
	                               : ConvertCardText(jSide.get("text","").asString());
	  this->upgDb[upg][DP::Titl] = jSide.get("title", "").asString();
	  this->upgDb[upg][DP::UpTy] = (jSide.get("type", "").asString() == "Device") ? "Payload" : jSide.get("type", "").asString();
	  this->upgDb[upg][DP::Xws]  = jUpgrade.get("xws", "").asString();
	  
	  if(jSides.size() > 1) {
	    jSide = jSides[1];
	    this->upgDb[upg][DP::Abil2] = std::to_string(jSide.isMember("ability"));
	    this->upgDb[upg][DP::ArtC2] = jSide.get("image", "").asString();
	    this->upgDb[upg][DP::ArtI2] = jSide.get("artwork", "").asString();
	    this->upgDb[upg][DP::Ffg2]  = std::to_string(jSide.get("ffg",0).asInt());
	    this->upgDb[upg][DP::Slot2] = GetStr(*GetUpgradeSlots(jSide["slots"], ret));
	    this->upgDb[upg][DP::Text2] = (jSide.isMember("ability")) ? ConvertCardText(jSide.get("ability","").asString()) : ConvertCardText(jSide.get("text","").asString());
	    this->upgDb[upg][DP::Titl2] = jSide.get("title", "").asString();
	    this->upgDb[upg][DP::UpTy2] = (jSide.get("type", "").asString() == "Device") ? "Payload" : jSide.get("type", "").asString();
	  }
	}
      }
    }
  }

#ifdef INC_QUICKBUILDS
  { // quickbuilds
    const char* PATH = "xwing-data2/data/quick-builds/";
    std::vector<std::string> files = strhlp::GetFilePaths(PATH, SUFX);
    for(const std::string& f : files) {
      std::ifstream file(f);
      Json::Value jRoot;
      file >> jRoot;
      Json::Value jQBs = jRoot["quick-builds"];
      for(Json::ValueConstIterator iQb = jQBs.begin(); iQb != jQBs.end() ; iQb++ ) {
	std::pair<int, std::vector<libxwing2::QBPlt>> quickBuild = GetQuickBuild(*iQb);
	std::optional<libxwing2::QB> qb = GetQB(quickBuild, ret);
	if(qb) {
	  this->qbDb[*qb][DP::QuBu] = GetStr(quickBuild.first, quickBuild.second);
	}
      }
    }
  }
#endif
  this->PopulateDps();
  return ret;
}

}

#pragma once
#include "datasrc.h"

namespace xdv::yasb {

class YASBDS : public DataSrc {
 public:
  YASBDS();
  virtual std::vector<std::string> Load() override;
};

}

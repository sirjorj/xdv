#include "ffg.h"
#include "strhlp.h"
#include <iostream>
#include <regex>
#include <sstream>

#if 1
#define LINECOLOR "\e[0;97m\u001b[44m"
#define NORMCOLOR "\e[0m"
#define DBG(fmt)       printf(fmt)
#define DBGV(fmt, ...) printf(fmt, __VA_ARGS__)
#else
#define DBG(fmt)
#define DBGV(fmt, ...)
#endif

namespace xdv::ffg {

// helpers

static std::vector<std::string> GetPdfText(std::string file) {
  std::string command = "pdftotext -layout '" + file + "' -";
  char* line = 0;
  size_t len = 0;
  FILE *f = popen(command.c_str(), "r");
  std::vector<std::string> ret;
  while(getline(&line, &len, f) != -1) {
    ret.push_back(line);
  }
  pclose(f);
  if(line) { free(line); }
  return ret;
}

struct Substr {
  size_t start;
  size_t length;
};

const std::map<uint32_t,libxwing2::UpT> UpTMap = {
  { 0xF3B28194, libxwing2::UpT::Astro     },
  { 0xF3B2818D, libxwing2::UpT::Cannon    },
  { 0xf3b28195, libxwing2::UpT::Cargo     },
  { 0xF3B29085, libxwing2::UpT::Command   },
  { 0xF3B288A1, libxwing2::UpT::Config    },
  { 0xF3B28192, libxwing2::UpT::Crew      },
  { 0xF3B288A2, libxwing2::UpT::Force     },
  { 0xF3B288A6, libxwing2::UpT::Gunner    },
  { 0xf3b28191, libxwing2::UpT::Hardpoint },
  { 0xF3B28198, libxwing2::UpT::Illicit   },
  { 0xF3B28190, libxwing2::UpT::Missile   },
  { 0xF3B28199, libxwing2::UpT::Mod       },
  { 0xF3B28196, libxwing2::UpT::Payload   },
  { 0xF3B2818C, libxwing2::UpT::Sensor    },
  { 0xF3B29082, libxwing2::UpT::TactRel   },
  { 0xF3B2818B, libxwing2::UpT::Talent    },
  { 0xf3b28193, libxwing2::UpT::Team      },
  { 0xF3B2819B, libxwing2::UpT::Tech      },
  { 0xF3B2819A, libxwing2::UpT::Title     },
  { 0xF3B2818F, libxwing2::UpT::Torpedo   },
  { 0xF3B2818E, libxwing2::UpT::Turret    },
};

enum state {
  Searching,
  Space,
  Reading,
  //Symbol,
};

static std::vector<Substr> Tokenize(std::string str) {
  std::vector<Substr> ret;
  size_t start = 0;
  size_t size  = 0;
  state s = Searching;
  for(int i=0; i<str.size(); i++) {
    char c = str[i];
    switch(s) {

    case Searching:
      if(c != ' ') {
	s = Reading;
	start = i;
	size = 0;
      }
      break;

    case Space:
      if(c == ' ') {
	s = Searching;
	ret.push_back({start,size});
      }
      else {
	size++;
	s = Reading;
      }
      break;

    case Reading:
      size++;
      if(c == ' ') {
	s = Space;
      }
      break;
    }
  }
  if(s == Reading) {
    size++;
    ret.push_back({start,size});
  }
  return ret;
}



static std::string CleanupName(std::string name) {
  std::string n = name;

  // remove unique pilot dot
  n = strhlp::ReplaceStr(n, { (char)0xE2, (char)0x80, (char)0xA2 }, "");

  // this was right after the unique dot for palp/sidious for some reason
  n = strhlp::ReplaceStr(n, { (char)0x07 }, "");

  // the doc uses different double quotes characters (“Wampa”)
  n = strhlp::ReplaceStr(n, "“", "\"");
  n = strhlp::ReplaceStr(n, "”", "\"");

  // ...and single quotes
  n = strhlp::ReplaceStr(n, "’", "'");

  // no asterisks needed
  n = strhlp::ReplaceStr(n, "\\*", "");

  // remove leading or trailing whitespace
  n = strhlp::Trim(n);

  // some names are formatted slightly differently between the docs and the cards
  // ships
  if(false) {}

#if 0
  // wave2
  else if(n == "Customized YT-1300")         { n = "Customized YT-1300 Light Freighter"; }
  else if(n == "M12-L Kimogila")             { n = "M12-L Kimogila Fighter"; }
  else if(n == "Modified YT-1300")           { n = "Modified YT-1300 Light Freighter"; }
  else if(n == "Scavanged YT-1300")          { n = "Scavenged YT-1300"; }
  else if(n == "Scurrg H-6 bomber")          { n = "Scurrg H-6 Bomber"; }
  else if(n == "StarViper-class")            { n = "StarViper-class Attack Platform"; }
  else if(n == "TIE Silencer")               { n = "TIE/vn Silencer"; }
  else if(n == "Upsilon-class Shuttle")      { n = "Upsilon-class command shuttle"; }
  else if(n == "Epsilon Squadon Cadet")      { n = "Epsilon Squadron Cadet"; }
  else if(n == "Jaycriss Tubbs")             { n = "Jaycris Tubbs"; }
  else if(n == "Jessica Pava")               { n = "Jessika Pava"; }
  else if(n == "Major Striden")              { n = "Major Stridan"; }  
  else if(n == "Seinar-Jamus Engineer")      { n = "Sienar-Jaemus Engineer"; }
  else if(n == "C-3P0")                      { n = "C-3PO"; }
  else if(n == "OS-1 Ordnance Loadout")      { n = "Os-1 Arsenal Loadout"; }
  else if(n == "XG-1 Assault Configuration") { n = "Xg-1 Assault Configuration"; }  

#elif 0

  // january 2019
  else if(n == "Alpha-class StarWing")  { n = "Alpha-class Star Wing"; }
  else if(n == "Customized YT-1300")    { n = "Customized YT-1300 Light Freighter"; }
  else if(n == "Modified YT-1300")      { n = "Modified YT-1300 Light Freighter"; }
  else if(n == "Scurrg H-6 bomber")     { n = "Scurrg H-6 Bomber"; }
  else if(n == "StarViper-class")       { n = "StarViper-class Attack Platform"; }
  else if(n == "Upsilon-class")         { n = "Upsilon-class command shuttle"; }
  else if(n == "Sienar-Jamus Engineer") { n = "Sienar-Jaemus Engineer"; }

#elif 0

  // wave 3
  else if(n == "Alpha-class StarWing") { n = "Alpha-class Star Wing"; }
  else if(n == "Scurrg H-6 bomber")    { n = "Scurrg H-6 Bomber"; }
  else if(n == "Command Shuttle")      { n = "Upsilon-class command shuttle"; }
  else if(n == "Grappling Struts (Closed) / Grappling Struts (Open)") { n = "Grappling Struts"; }
  else if(n == "Os-1 Ordnance Loadout")      { n = "Os-1 Arsenal Loadout"; }
  else if(n == "Chancellor Palpatine / Darth Sidious") { n = "Palpatine/Sidious"; }

#elif 0

  // july 2019
  else if(n == "Alpha-class StarWing") { n = "Alpha-class Star Wing"; }
  else if(n == "Scurrg H-6 bomber")    { n = "Scurrg H-6 Bomber"; }
  else if(n == "TIE Interceptor")      { n = "TIE/in Interceptor"; }
  else if(n == "Command Shuttle")      { n = "Upsilon-class command shuttle"; }
  else if(n == "Grappling Struts (Closed) / Grappling Struts (Open)") { n = "Grappling Struts"; }
  else if(n == "Landing Struts (Closed) / Landing Struts (Open)") { n = "Landing Struts"; }
  else if(n == "Os-1 Ordnance Loadout")      { n = "Os-1 Arsenal Loadout"; }
  else if(n == "Chancellor Palpatine / Darth Sidious") { n = "Palpatine/Sidious"; }

#elif 0

  // wave 5
  else if(n == "Alpha-class StarWing") { n = "Alpha-class Star Wing"; }
  else if(n == "\"Oddball\"") { n = "\"Odd Ball\""; }
  else if(n == "Scurrg H-6 bomber")    { n = "Scurrg H-6 Bomber"; }
  else if(n == "TIE Interceptor")      { n = "TIE/in Interceptor"; }
  else if(n == "Command Shuttle")      { n = "Upsilon-class command shuttle"; }
  else if(n == "Grappling Struts (Closed) / Grappling Struts (Open)") { n = "Grappling Struts"; }
  else if(n == "Landing Struts (Closed) / Landing Struts (Open)") { n = "Landing Struts"; }
  else if(n == "Os-1 Ordnance Loadout")      { n = "Os-1 Arsenal Loadout"; }
  else if(n == "Chancellor Palpatine / Darth Sidious") { n = "Palpatine/Sidious"; }
  else if(n == "Calibrated")           { n = "Calibrated Laser Targeting"; }

#elif 0

  // huge/epic
  else if(n == "Alpha-class StarWing")                                { n = "Alpha-class Star Wing"; }
  else if(n == "Boosted Sensors")                                     { n = "Boosted Scanners"; }
  else if(n == "Command Shuttle")                                     { n = "Upsilon-class command shuttle"; }
  else if(n == "CR-90 Corvette")                                      { n = "CR90 Corellian Corvette"; }
  else if(n == "GR-75 Med. Transport")                                { n = "GR-75 Medium Transport"; }
  else if(n == "Grappling Struts (Closed) / Grappling Struts (Open)") { n = "Grappling Struts"; }
  else if(n == "Landing Struts (Closed) / Landing Struts (Open)")     { n = "Landing Struts"; }
  else if(n == "\"Oddball\"")                                         { n = "\"Odd Ball\""; }
  else if(n == "Os-1 Ordnance Loadout")                               { n = "Os-1 Arsenal Loadout"; }
  else if(n == "Chancellor Palpatine / Darth Sidious")                { n = "Palpatine/Sidious"; }
  else if(n == "Sabotage Mapper")                                     { n = "Saboteur's Map"; }
  else if(n == "Sensor Baffler")                                      { n = "Scanner Baffler"; }
  else if(n == "Targeting Battery Turret")                            { n = "Targeting Battery"; }
  else if(n == "TIE Interceptor")                                     { n = "TIE/in Interceptor"; }

#elif 0

  // jan 2020
  else if(n == "Alpha-class StarWing")                                { n = "Alpha-class Star Wing"; }
  else if(n == "\"Oddball\"")                                         { n = "\"Odd Ball\""; }
  else if(n == "CR-90 Corvette")                                      { n = "CR90 Corellian Corvette"; }
  else if(n == "GR-75 Med. Transport")                                { n = "GR-75 Medium Transport"; }
  else if(n == "Zizi Tio")                                            { n = "Zizi Tlo"; }
  else if(n == "Ronnith Blario")                                      { n = "Ronith Blario"; }
  else if(n == "Scurrg H-6 Bomber")                                   { n = "Scurrg H-6 bomber"; }
  else if(n == "TIE Interceptor")                                     { n = "TIE/in Interceptor"; }
  else if(n == "Command Shuttle")                                     { n = "Upsilon-class command shuttle"; }
  else if(n == "Upsilon-class Command Shuttle")                       { n = "Upsilon-class command shuttle"; }
  else if(n == "Boosted Sensors")                                     { n = "Boosted Scanners"; }
  else if(n == "Calibrated")                                          { n = "Calibrated Laser Targeting"; }
  else if(n == "Grappling Struts (Closed) / Grappling Struts (Open)") { n = "Grappling Struts"; }
  else if(n == "Landing Struts (Closed) / Landing Struts (Open)")     { n = "Landing Struts"; }
  else if(n == "Os-1 Ordnance Loadout")                               { n = "Os-1 Arsenal Loadout"; }
  else if(n == "Chancellor Palpatine / Darth Sidious")                { n = "Palpatine/Sidious"; }
  else if(n == "Targeting Battery Turret")                            { n = "Targeting Battery"; }
  else if(n == "Sabotage Mapper")                                     { n = "Saboteur's Map"; }
  else if(n == "Sensor Baffler")                                      { n = "Scanner Baffler"; }
  else if(n == "Proud / False Tradition")                             { n = "Proud Tradition"; }

#elif 1

  // jan 2020 (fixes)

  else if(n == "Alpha-class StarWing")                                { n = "Alpha-class Star Wing"; }
  else if(n == "\"Oddball\"")                                         { n = "\"Odd Ball\""; }
  else if(n == "CR-90 Corvette")                                      { n = "CR90 Corellian Corvette"; }
  else if(n == "GR-75 Med. Transport")                                { n = "GR-75 Medium Transport"; }
  else if(n == "Scurrg H-6 Bomber")                                   { n = "Scurrg H-6 bomber"; }
  else if(n == "TIE Interceptor")                                     { n = "TIE/in Interceptor"; }
  else if(n == "Upsilon-class Command Shuttle")                       { n = "Upsilon-class command shuttle"; }
  else if(n == "Boosted Sensors")                                     { n = "Boosted Scanners"; }
  else if(n == "Calibrated")                                          { n = "Calibrated Laser Targeting"; }
  else if(n == "Grappling Struts (Closed) / Grappling Struts (Open)") { n = "Grappling Struts"; }
  else if(n == "Landing Struts (Closed) / Landing Struts (Open)")     { n = "Landing Struts"; }
  else if(n == "Os-1 Ordnance Loadout")                               { n = "Os-1 Arsenal Loadout"; }
  else if(n == "Chancellor Palpatine / Darth Sidious")                { n = "Palpatine/Sidious"; }
  else if(n == "Targeting Battery Turret")                            { n = "Targeting Battery"; }
  else if(n == "Sabotage Mapper")                                     { n = "Saboteur's Map"; }
  else if(n == "Sensor Baffler")                                      { n = "Scanner Baffler"; }
  else if(n == "Proud / False Tradition")                             { n = "Proud Tradition"; }
#endif

  return n;
}

static std::optional<libxwing2::UpT> GetUpT(unsigned int i) {
  if(UpTMap.find(i) == UpTMap.end()) {
    return std::nullopt;
  } else {
    return UpTMap.at(i);
  }
}

static std::optional<libxwing2::Shp> FindShpByName(std::string name) {
  for(const libxwing2::Ship& s : libxwing2::Ship::GetAllShips()) {
    if(s.GetName() == name) {
      return s.GetShp();
    }
  }
  return std::nullopt;
}

static std::optional<libxwing2::Plt> FindPltByName(std::string name, libxwing2::Shp shp) {
  for(const libxwing2::Pilot& p : libxwing2::Pilot::GetAllPilots()) {
    if((p.GetName() == name) && (p.GetShp() == shp)) {
      return p.GetPlt();
    }
  }
  return std::nullopt;
}

static int GetLimited(std::string s) {
  int ret = 0;
  std::string limStr = { (char)0xE2, (char)0x80, (char)0xA2 };
  std::string::size_type pos = 0;
  while ((pos = s.find(limStr, pos )) != std::string::npos) {
    ret++;
    pos += limStr.length();
  }
  return ret;
}

static std::optional<libxwing2::Upg> FindUpgByName(std::string name, libxwing2::Fac fac, std::vector<libxwing2::UpT> slots) {
  for(const libxwing2::Upgrade& u : libxwing2::Upgrade::GetAllUpgrades()) {
    if((u.GetName() == name)) {
      libxwing2::Fac allowedFac = u.GetRestriction().GetAllowedFactions();
      if((allowedFac & fac) != libxwing2::Fac::None) {
	if((u.GetSlots() == slots)) {
	  return u.GetUpg();
	}
      }
    }
  }
  return std::nullopt;
}



static std::optional<bool> TryHyperspaceLegal(std::string s) {
  if(s == "Yes")     { return true; }
  else if(s == "No") { return false; }
  else               { return std::nullopt; }
}

static std::optional<std::vector<libxwing2::UpT>> TryUpgrades(std::string s) {
  std::vector<libxwing2::UpT> upgs;

  s.erase(std::remove_if(s.begin(), s.end(), isalpha), s.end());
  s.erase(std::remove_if(s.begin(), s.end(), ispunct), s.end());
  s = strhlp::Trim(s);

  if(s.size() == 0) {
    return std::nullopt;
  }

  if((s.size() % 4) == 0) {
    for(int i=0; i<s.size()/4; i++) {
      unsigned int c = 0;
      c |= (s[(i*4)+0] & 0xFF); c <<= 8;
      c |= (s[(i*4)+1] & 0xFF); c <<= 8;
      c |= (s[(i*4)+2] & 0xFF); c <<= 8;
      c |= (s[(i*4)+3] & 0xFF);
      std::optional<libxwing2::UpT> upt = GetUpT(c);
      if(upt) {
	upgs.push_back(*upt);
      }
      else {
	DBGV("UNKNOWN UPG '%02x'\n", c);
	return std::nullopt;
      }
    }
    return upgs;
  }
  return std::nullopt;
}
  /*
static std::optional<int> TryCost(std::string s) {
  if(!strhlp::isDigits(s)) { return std::nullopt; }
  try {
    int c = std::stoi(s);
    if((c >= 0) && (c <= 200)) {
      return c;
    }
  }
  catch(...) {
  }
  return std::nullopt;
}
  */
static std::optional<libxwing2::Shp> TryShipName(std::string s) {
  std::optional<libxwing2::Shp> shp = FindShpByName(s);
  if(shp) { return shp; }
  else    { return std::nullopt; }
}

static std::optional<libxwing2::Plt> TryPilotName(std::string s, libxwing2::Shp shp) {
    std::optional<libxwing2::Plt> plt = FindPltByName(s, shp);
  if(plt) { return plt; }
  else    { return std::nullopt; }
}

static std::optional<libxwing2::Cost> TryCost(std::string s) {
  s = CleanupName(s);
  std::vector<std::string> toks = strhlp::Tokenize(s, '/');
  switch(toks.size()) {
  case 1:
    if(strhlp::isDigits(toks[0])) {
      return libxwing2::Cost(std::stoi(toks[0]));
    }
    break;
  case 3:
    if(strhlp::isDigits(toks[0]) && strhlp::isDigits(toks[1]) && strhlp::isDigits(toks[2])) {
      return libxwing2::Cost(std::stoi(toks[0]), std::stoi(toks[1]), std::stoi(toks[2]));
    }
    break;
  case 4:
    if(strhlp::isDigits(toks[0]) && strhlp::isDigits(toks[1]) && strhlp::isDigits(toks[2]) && strhlp::isDigits(toks[3])) {
      return libxwing2::Cost(std::stoi(toks[0]), std::stoi(toks[1]), std::stoi(toks[2]), std::stoi(toks[3]));
    }
    break;
  case 7:
    if(strhlp::isDigits(toks[0]) && strhlp::isDigits(toks[1]) && strhlp::isDigits(toks[2]) && strhlp::isDigits(toks[3]) && strhlp::isDigits(toks[4]) && strhlp::isDigits(toks[5]) && strhlp::isDigits(toks[6])) {
      return libxwing2::Cost(std::stoi(toks[0]), std::stoi(toks[1]), std::stoi(toks[2]), std::stoi(toks[3]), std::stoi(toks[4]), std::stoi(toks[5]), std::stoi(toks[6]));
    }
    break;
  }
  return std::nullopt;
}

static std::optional<libxwing2::Upg> TryUpgradeName(std::string s, libxwing2::Fac fac, std::vector<libxwing2::UpT> type) {
  std::optional<libxwing2::Upg> upg = FindUpgByName(s, fac, type);
  if(upg) { return upg; }
  else    { return std::nullopt; }
}



// PILOT
struct PilotInfo {
  libxwing2::Shp                 shp;
  std::optional<libxwing2::Cost> cost;
  int                            limit;
  libxwing2::Plt                 plt;
  libxwing2::UpgradeBar          upgs;
  bool                           isHyperspace;
};

static std::optional<PilotInfo> GetPilotInfo(std::string line) {
  std::vector<Substr> tokens = Tokenize(line);

  std::optional<bool>                  hyp = std::nullopt;
  std::optional<libxwing2::UpgradeBar> upg = std::nullopt;
  std::optional<libxwing2::Cost>       cst = std::nullopt;
  std::optional<libxwing2::Shp>        shp = std::nullopt;
  std::optional<int>                   lim = std::nullopt;
  std::optional<libxwing2::Plt>        plt = std::nullopt;

  for(const Substr& ss : tokens) {
    std::string st = CleanupName(line.substr(ss.start, ss.length));

    std::optional<bool>                        tHyp = TryHyperspaceLegal(st);
    std::optional<std::vector<libxwing2::UpT>> tUpg = TryUpgrades(st);
    std::optional<libxwing2::Cost>             tCst = TryCost(st);
    std::optional<libxwing2::Shp>              tShp = TryShipName(st);
    std::optional<libxwing2::Plt>              tPlt = TryPilotName(st, *shp);

    int hits = (tShp?1:0) + (tCst?1:0) + (tPlt?1:0) + (tUpg?1:0) + (tHyp?1:0);

    DBGV("%s...", st.c_str());
    if(hits == 1) {
      if(tHyp) {
	DBG("HYPERSPACE\n");
	hyp = *tHyp;
      }
      else if(tUpg) {
	DBG("UPGRADEBAR\n");
	libxwing2::UpgradeBar ub;
	for(libxwing2::UpT t : *tUpg) {
	  ub.push_back(t);
	}
	upg = { ub };
      }
      else if(tCst) {
	DBG("COST\n");
	cst = tCst;
      }
      else if(tShp) {
	DBG("SHIP\n");
	shp = *tShp;
      }
      else if(tPlt) {
	DBG("PILOT\n");
	std::string fullPilot = line.substr(ss.start, ss.length);
	plt = *tPlt;
	lim = GetLimited(fullPilot);
      }
      else {
	DBG("YOU SHOULD NEVER SEE THIS!\n");
      }
    }
    else if(hits == 0) {
      /*
      DBGV("NO HITS (%s) - checking with next line", st.c_str());
      // see if rest of ship name is on the next line
      std::string nextLine = strhlp::Trim(next);
      std::string newName = CleanupName(st + " " + nextLine);
      DBGV("(%s)...", newName.c_str());      
      tShp = TryShipName(newName);
      if(tShp) {
	DBG("SHIP\n");
	shp = *tShp;

      }
      else {
      */
	DBG("NO HITS - checking if cost is attached...");
	size_t split = st.rfind(" ");
	if(split < st.size()) {
	  std::string maybeShipStr = strhlp::Trim(st.substr(0, split));
	  std::string maybeCostStr = strhlp::Trim(st.substr(split));
	  tShp = TryShipName(maybeShipStr);
	  tCst = TryCost(maybeCostStr);
	  if(tShp && tCst) {
	    DBG("SHIP & COST\n");
	    shp = tShp;
	    cst = tCst;
	  }
	  else {
	    DBG("NOPE!\n");
	  }
	}
	//}
    }

    else if(hits > 1) {
      DBGV("MULTIPLE HITS on '%s'", st.c_str());
      if(tHyp) { DBG(" [Hyperspace]"); }
      if(tUpg) { DBG(" [UpgradeBar]"); }
      if(tCst) { DBG(" [Cost]"); }
      if(tShp) { DBG(" [Ship]"); }
      if(tPlt) { DBG(" [Pilot]"); }
      DBG("\n");
    }
    else {
      DBG("NEGATIVE HITS???????????????????????????????????????????????\n");
    }
  }

  // now build the PilotInfo.
  PilotInfo pi;
  if(shp) { pi.shp = *shp; }
  else    { return {};     }

  if(cst) { pi.cost = *cst; }
  else    { return {};      }

  if(lim) { pi.limit = *lim; }
  else    { pi.limit = 0;    }

  if(plt) { pi.plt = *plt; }
  else    { return {};     }

  if(upg) { pi.upgs = *upg; }
  else    { pi.upgs = {};   }

  if(hyp) { pi.isHyperspace = *hyp;  }
  else    { pi.isHyperspace = false; }

  DBG("OK!\n\n");
  
  return pi;
}



// Upgrade
struct UpgradeInfo {
  int                            limit;
  libxwing2::Upg                 upg;
  std::optional<libxwing2::Cost> cost; // optional is just to get around the default ctor thing
  std::vector<libxwing2::UpT>    slots;
  bool                           isHyperspace;
};

static std::optional<UpgradeInfo> GetUpgradeInfo(std::string line, libxwing2::Fac f) {
  std::vector<Substr> tokens = Tokenize(line);
  std::reverse(tokens.begin(), tokens.end());

  std::optional<bool>                        hyp = std::nullopt;
  std::optional<std::vector<libxwing2::UpT>> slt = std::nullopt;
  std::optional<libxwing2::Cost>             cst = std::nullopt;
  std::optional<int>                         lim = std::nullopt;
  std::optional<libxwing2::Upg>              upg = std::nullopt;

  for(const Substr& ss : tokens) {
    std::string st = CleanupName(line.substr(ss.start, ss.length));

    std::optional<bool>                        tHyp = TryHyperspaceLegal(st);
    std::optional<std::vector<libxwing2::UpT>> tSlt = TryUpgrades(st);
    std::optional<libxwing2::Cost>             tCst = TryCost(st);
    std::optional<libxwing2::Upg>              tUpg = slt ? TryUpgradeName(st, f, *slt) : std::nullopt;

    int hits = (tUpg?1:0) + (tCst?1:0) + (tSlt?1:0) + (tHyp?1:0);

    DBGV("NEW TOKEN (UPG)!!!!! '%s'...", st.c_str());
    if(hits == 1) {
      if(tHyp) {
	DBG("HYPERSPACE\n");
	hyp = *tHyp;
      }
      else if(tSlt) {
	DBG("UPGRADETYPE(s)\n");
	slt = tSlt;
      }
      else if(tCst) {
	DBG("COST\n");
	cst = *tCst;
      }
      else if(tUpg) {
	DBG("UPGRADE\n");
	std::string fullUpgrade = line.substr(ss.start, ss.length);
	lim = GetLimited(fullUpgrade);
	upg = *tUpg;
      }
      else {
	DBG("YOU SHOULD NEVER SEE THIS!\n");
      }
    }

    else if(hits == 0) {
      DBGV("NO HITS (%s)\n", st.c_str());
    }

    else if(hits > 1) {
      DBGV("MULTIPLE HITS on '%s'", st.c_str());
      if(tHyp) { DBG(" [Hyperspace]"); }
      if(tSlt) { DBG(" [UpgradeType]"); }
      if(tCst) { DBG(" [Cost]"); }
      if(tUpg) { DBG(" [Upgrade]"); }
      DBG("\n");
    }
    else {
      DBG("NEGATIVE HITS???????????????????????????????????????????????\n");
    }
  }

  // now build the UpgradeInfo.
  UpgradeInfo ui;
  if(lim) { ui.limit = *lim; }
  else    { ui.limit = 0;    }

  if(upg) { ui.upg = *upg; }
  else    { return std::nullopt; }

  if(cst) { ui.cost = *cst; }
  else    { return std::nullopt; }

  if(slt) { ui.slots = *slt; }
  else    { return std::nullopt; }

  if(hyp) { ui.isHyperspace = *hyp;  }
  else    { return std::nullopt; } //ui.isHyperspace = false; }

  DBG("OK!\n\n");
  
  return ui;
}


int FirstNonSpace(std::string s) {
  int i = 0;
  for(char c : s) {
    if(c != ' ') {
      break;
    }
    i++;
  }
  return i;
}

// class

FFGDS::FFGDS()
  : DataSrc("FFG") {
}

std::vector<std::string> FFGDS::Load() {
  std::vector<std::string> ret;

  std::vector<std::string> files = strhlp::GetFilePaths("./ffg/", ".pdf");

  enum class CardType {
    Unknown,
    Pilot,
    Upgrade,
    HugePilot,
    HugeUpgrade,
  };

  for(const std::string& filename : files) {
    libxwing2::Fac faction = libxwing2::Fac::All;
    CardType cardType = CardType::Unknown;

    std::vector<std::string> lines = GetPdfText(filename);
    int lastCombined = 0;

    // for each line...
    for(int i=0; i<lines.size(); i++) {

      if(lastCombined > 2) {
	DBG("RESETTING lastCombined\n");
	i = i - lastCombined + 1;
	lastCombined = 0;
      }
      
      bool first = true;
      std::string line = "";
      for(int l = lastCombined; l >= 0; l--) {
	if(l >= 0) {
	  if(first) {
	    first=false;
	    line = strhlp::Trim(lines[i-l]);
	  }
	  else {
	    line += " ";
	    // if there aren't a lot of spaces at the beginning, assume it is the card name being split onto 2 lines
	    if(FirstNonSpace(lines[i-l]) < 4) {
	      line += strhlp::Trim(lines[i-l]);;
	    } else {
	      line += lines[i-l];
	    }
	  }
  	}
      }

      DBGV("%d%s%s%s\n", lastCombined, LINECOLOR, line.c_str(), NORMCOLOR);

      //std::string next = ((i+1)<lines.size()) ? lines[i+1] : "";

      // skip empty linest
      if(line == "") {
	DBG("[]\n");
	lastCombined = 0;
	continue;
      }

      // some lines are just 'TM' - ignore them
      if(line == "TM") {
	DBG("[TM]\n");
	lastCombined = 0;
	continue;
      }

      // some lines have a weird codepoint and a TM
      if(strhlp::EndsWith(line, "TM")) {
	DBG("[ TM]\n");
	lastCombined = 0;
	continue;
      }

      // some lines have a weird codepoint and a TM
      if(strhlp::Contains(line, "©")) {
	DBG("[ CR]\n");
	lastCombined = 0;
	continue;
      }
      
      // 'Point Costs and Upgrade Slots'
      if(line == "Point Costs and Upgrade Slots") {
	DBG("[PCUS]\n");
	lastCombined = 0;
	continue;
      }

      // 'Faction-Specific Upgrade Cards'
      if(line == "Faction-Specific Upgrade Cards") {
	DBG("[FSUC]\n");
	lastCombined = 0;
	continue;
      }
      
      // at the top there is a line with the faction
      {
	bool foundIt = false;
	for(const libxwing2::Faction& f : libxwing2::Faction::GetAllFactions()) {
	  if(line == f.GetName()) {
	    DBGV("[%s]\n", f.Get3Letter().c_str());
	    foundIt = true;
	    faction = f.GetFac();
	    break;
	  }
	}
	if(foundIt) {
	  lastCombined = 0;
	  continue;
	}
      }

      // there is a version string too
      if(strhlp::StartsWith(line, "version")) {
	version = strhlp::Tokenize(line, ' ')[1];
	DBGV("[%s]\n", version.c_str());
	lastCombined = 0;
	continue;
      }

      // copyright line
      if(strhlp::StartsWith(line, "©")) {
	DBG("[©]\n");
	lastCombined = 0;
	continue;
      }

      // VPC tables - these are at the end so when we get here, we are done with this file
      if(line == "Variable Point Cost Tables") {
	lastCombined = 0;
	break;
      }
      
      // pilot header
      if(std::regex_match(line, std::regex(R"(^Ship Type[ ]+Cost[ ]+Pilot Name[ ]+Upgrade Bar[ ]+Hyperspace Legal$)"))) {
	DBG("[PH]\n");
	cardType = CardType::Pilot;
	continue;
      }

      // upgrade header
      if(std::regex_match(line, std::regex(R"(^Upgrade Name[ ]+Cost[ ]+Upgrade Type[ ]+Hyperspace Legal$)"))) {
	DBG("[UH]\n");
	cardType = CardType::Upgrade;
	continue;
      }

      // huge pilot header
      if(std::regex_match(line, std::regex(R"(^Ship Type[ ]+Faction[ ]+Cost[ ]+Pilot Name[ ]+Upgrade Bar[ ]+Extended Legal$)"))) {
	DBG("[HPH]\n");
	cardType = CardType::HugePilot;
	continue;
      }

      // huge upgrade header
      if(std::regex_match(line, std::regex(R"(^Upgrade Name[ ]+Cost[ ]+Upgrade Type[ ]+Extended Legal$)"))) {
	DBG("[HUH]\n");
	cardType = CardType::HugeUpgrade;
	continue;
      }

      // now we get to the pilot and upgrade lines...
      if(cardType == CardType::Pilot) {
	std::optional<PilotInfo> pi = GetPilotInfo(line);
	if(pi) {
	  lastCombined = 0;
	  this->pltDb[pi->plt][DP::Cost] = GetStr(pi->cost);
	  this->pltDb[pi->plt][DP::Hype] = std::to_string(pi->isHyperspace);
	  this->pltDb[pi->plt][DP::Limi] = std::to_string(pi->limit);
	  this->pltDb[pi->plt][DP::UBar] = GetStr(pi->upgs);
	  continue;
	} else {
	  lastCombined++;
	}
      }

      if(cardType == CardType::Upgrade) {
	std::optional<UpgradeInfo> ui = GetUpgradeInfo(line, faction);
	if(ui) {
	  lastCombined = 0;
  	  this->upgDb[ui->upg][DP::Cost] = GetStr(*ui->cost);
	  this->upgDb[ui->upg][DP::Hype] = std::to_string(ui->isHyperspace);
	  this->upgDb[ui->upg][DP::Limi] = std::to_string(ui->limit);
	  this->upgDb[ui->upg][DP::Slot] = GetStr(ui->slots);
	  continue;
	} else {
	  lastCombined++;
	}
      }

      if(cardType == CardType::HugePilot) {
	std::optional<PilotInfo> pi = GetPilotInfo(line);
	if(pi) {
	  lastCombined = 0;
	  this->pltDb[pi->plt][DP::Cost] = GetStr(pi->cost);
	  this->pltDb[pi->plt][DP::Hype] = std::to_string(pi->isHyperspace);
	  this->pltDb[pi->plt][DP::Limi] = std::to_string(pi->limit);
	  this->pltDb[pi->plt][DP::UBar] = GetStr(pi->upgs);
	  continue;
	} else {
	  lastCombined++;
	}
      }

      if(cardType == CardType::HugeUpgrade) {
	std::optional<UpgradeInfo> ui = GetUpgradeInfo(line, libxwing2::Fac::All); // huge ship pdf doesnt specify faction for upgrades so say all
	if(ui) {
	  lastCombined = 0;
  	  this->upgDb[ui->upg][DP::Cost] = GetStr(*ui->cost);
	  this->upgDb[ui->upg][DP::Hype] = std::to_string(ui->isHyperspace);
	  this->upgDb[ui->upg][DP::Limi] = std::to_string(ui->limit);
	  this->upgDb[ui->upg][DP::Slot] = GetStr(ui->slots);
	  continue;
	} else {
	  lastCombined++;
	}
      }

    }
  }
  this->PopulateDps();
  return ret;
}

}

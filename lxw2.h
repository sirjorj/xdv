#pragma once
#include "datasrc.h"

namespace xdv::lxw2 {

class LXW2DS : public DataSrc {
 public:
  LXW2DS();
  virtual std::vector<std::string> Load() override;

  const ShpDB& GetShps();
  const PltDB& GetPlts();
  const UpgDB& GetUpgs();
  const QBDB&  GetQBs();

 private:
  std::string version;
};

}

#pragma once
#include <string>
#include <vector>

namespace strhlp {

bool StartsWith(const std::string& str, const std::string& prefix);
bool EndsWith(const std::string& str, const std::string& suffix);
bool Contains(const std::string& str, const std::string& target);
bool isDigits(const std::string &s);

std::string Trim(std::string s);
std::string ReplaceStr(std::string str, std::string remove, std::string add);

std::vector<std::string> Tokenize(std::string s, const char DELIMITER);

std::vector<std::string> GetFilePaths(std::string path, std::string suffix);

}

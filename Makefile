DEBUG=-g
CPP=clang++
CPPFLAGS=$(DEBUG) -Wall -fPIC -std=c++17 

all: xdv

xdv: main.cpp datasrc.h jsoncpp.o strhlp.o datasrc.o lxw2.o ffg.o xd2.o yasb.o
	$(CPP) $(CPPFLAGS) -v main.cpp -o xdv jsoncpp.o strhlp.o datasrc.o lxw2.o ffg.o xd2.o yasb.o -lxwing2

jsoncpp.o: jsoncpp.cpp json/json.h
	$(CPP) $(CPPFLAGS) -c jsoncpp.cpp

strhlp.o: strhlp.cpp strhlp.h
	$(CPP) $(CPPFLAGS) -c strhlp.cpp

datasrc.o: datasrc.cpp datasrc.h
	$(CPP) $(CPPFLAGS) -c datasrc.cpp

lxw2.o: lxw2.cpp lxw2.h
	$(CPP) $(CPPFLAGS) -c lxw2.cpp

ffg.o: ffg.cpp ffg.h
	$(CPP) $(CPPFLAGS) -c ffg.cpp

xd2.o: xd2.cpp xd2.h
	$(CPP) $(CPPFLAGS) -c xd2.cpp

yasb.o: yasb.cpp yasb.h
	$(CPP) $(CPPFLAGS) -c yasb.cpp

clean:
	rm -rf *.o *~ xdv

#include "lxw2.h"
#include "strhlp.h"
#include <libxwing2/converter.h>

namespace xdv::lxw2 {

const libxwing2::PtL PTL = libxwing2::PtL::Current;

static int SafeGetFfgId(libxwing2::Plt plt) {
  try { return libxwing2::converter::ffg::GetPilot(plt); }
  catch(libxwing2::converter::ffg::PilotNotFound &pnf) { return 0; }
}
static std::string SafeGetFfgArtC(libxwing2::Plt plt) {
  try { return libxwing2::converter::ffg::GetCard(plt); }
  catch(libxwing2::converter::ffg::PilotNotFound &unf) { return ""; }
}
static std::string SafeGetFfgArtI(libxwing2::Plt plt) {
  try { return libxwing2::converter::ffg::GetArt(plt); }
  catch(libxwing2::converter::ffg::PilotNotFound &unf) { return ""; }
}
static int SafeGetFfgId(libxwing2::Upg upg, int side) {
  try { return libxwing2::converter::ffg::GetUpgrade(upg, side); }
  catch(libxwing2::converter::ffg::UpgradeNotFound &unf) { return 0; }
}
static std::string SafeGetFfgArtC(libxwing2::Upg upg, int side) {
  try { return libxwing2::converter::ffg::GetCard(upg, side); }
  catch(libxwing2::converter::ffg::UpgradeNotFound &unf) { return ""; }
}
static std::string SafeGetFfgArtI(libxwing2::Upg upg, int side) {
  try { return libxwing2::converter::ffg::GetArt(upg, side); }
  catch(libxwing2::converter::ffg::UpgradeNotFound &unf) { return ""; }
}
static std::string SafeGetXws(libxwing2::Plt plt) {
  try { return libxwing2::converter::xws::GetPilot(plt); }
  catch(libxwing2::converter::xws::PilotNotFound &pnf) { return ""; }
}
static std::string SafeGetXws(libxwing2::Upg upg) {
  try { return libxwing2::converter::xws::GetUpgrade(upg); }
  catch(libxwing2::converter::xws::UpgradeNotFound &unf) { return ""; }
}
static int SafeGetYasbId(libxwing2::Plt plt) {
  try { return libxwing2::converter::yasb::GetPilot(plt); }
  catch(libxwing2::converter::yasb::PilotNotFound &pnf) { return 0; }
}
static int SafeGetYasbId(libxwing2::Upg upg) {
  try { return libxwing2::converter::yasb::GetUpgrade(upg); }
  catch(libxwing2::converter::yasb::UpgradeNotFound &unf) { return 0; }
}



LXW2DS::LXW2DS()
  : DataSrc("LXW2") {
}

std::vector<std::string> LXW2DS::Load() {

  { // version
  }

  { // ships
    for(const libxwing2::Ship& ship : libxwing2::Ship::GetAllShips()) {
      libxwing2::Shp shp = ship.GetShp();
      this->shpDb[shp][DP::Manv] = GetStr(ship.GetManeuvers());
      this->shpDb[shp][DP::Name] = ship.GetName();
      this->shpDb[shp][DP::Size] = ship.GetBaseSize().GetName();
      this->shpDb[shp][DP::Xws] = libxwing2::converter::xws::GetShip(ship.GetShp());
    }
  }

  { // pilots
    for(const libxwing2::Pilot& pilot : libxwing2::Pilot::GetAllPilots()) {
      libxwing2::Plt plt = pilot.GetPlt();
      this->pltDb[plt][DP::Abil] = std::to_string(pilot.HasAbility());
      this->pltDb[plt][DP::Acti] = GetStr(pilot.GetNatActions());
      this->pltDb[plt][DP::ArtC] = SafeGetFfgArtC(plt);
      this->pltDb[plt][DP::ArtI] = SafeGetFfgArtI(plt);
      this->pltDb[plt][DP::Agil] = std::to_string(pilot.GetNatAgility());
      this->pltDb[plt][DP::Atck] = GetStr(pilot.GetNatAttacks());
      this->pltDb[plt][DP::Chrg] = GetStr(pilot.GetNatCharge());
      this->pltDb[plt][DP::Cost] = pilot.GetNatCost(PTL) ? std::to_string(*pilot.GetNatCost(PTL)) : "200";
      this->pltDb[plt][DP::Enga] = std::to_string(pilot.GetNatEngagement());
      this->pltDb[plt][DP::Fact] = pilot.GetFaction().GetName();
      this->pltDb[plt][DP::Ffg]  = std::to_string(SafeGetFfgId(plt));
      this->pltDb[plt][DP::Forc] = GetStr(pilot.GetNatForce());
      this->pltDb[plt][DP::Hull] = std::to_string(pilot.GetNatHull());
      this->pltDb[plt][DP::Hype] = pilot.GetHyperspace(PTL) ? std::to_string(*pilot.GetHyperspace(PTL)) : "0";
      this->pltDb[plt][DP::Init] = std::to_string(pilot.GetNatInitiative());
      this->pltDb[plt][DP::Kwds] = pilot.GetKeywordList() ? GetStr(*pilot.GetKeywordList()) : "";
      this->pltDb[plt][DP::Limi] = std::to_string(pilot.GetLimited());
      this->pltDb[plt][DP::Name] = pilot.GetName();
      this->pltDb[plt][DP::ShAb] = pilot.GetShipAbility() ? GetStr(*pilot.GetShipAbility()) : "";
      this->pltDb[plt][DP::Ship] = pilot.GetShip().GetName();
      this->pltDb[plt][DP::Shld] = GetStr(pilot.GetNatShield());
      this->pltDb[plt][DP::Subt] = pilot.GetSubtitle();
      this->pltDb[plt][DP::Text] = pilot.GetText().GetCleanText();
      this->pltDb[plt][DP::UBar] = pilot.GetNatUpgradeBar(PTL) ? GetStr(*pilot.GetNatUpgradeBar(PTL)) : "";
      this->pltDb[plt][DP::Xws]  = SafeGetXws(plt);
      this->pltDb[plt][DP::Yasb] = std::to_string(SafeGetYasbId(plt));
    }
  }

  {// upgrades
    for(libxwing2::Upgrade upgrade : libxwing2::Upgrade::GetAllUpgrades()) {
      libxwing2::Upg upg = upgrade.GetUpg();
      this->upgDb[upg][DP::Abil] = std::to_string(upgrade.HasAbility());
      this->upgDb[upg][DP::ArtC] = SafeGetFfgArtC(upg,0);
      this->upgDb[upg][DP::ArtI] = SafeGetFfgArtI(upg,0);
      this->upgDb[upg][DP::Cost] = upgrade.GetCost(PTL) ? GetStr(*upgrade.GetCost(PTL)) : "200";
      this->upgDb[upg][DP::Ffg]  = std::to_string(SafeGetFfgId(upg,0));
      this->upgDb[upg][DP::Hype] = upgrade.GetHyperspace(PTL) ? std::to_string(*upgrade.GetHyperspace(PTL)) : "0";
      this->upgDb[upg][DP::Limi] = std::to_string(upgrade.GetLimited());
      this->upgDb[upg][DP::Name] = upgrade.GetName();
      this->upgDb[upg][DP::Slot] = GetStr(upgrade.GetSlots());
      this->upgDb[upg][DP::Text] = upgrade.GetText().GetCleanText();
      this->upgDb[upg][DP::Titl] = upgrade.GetTitle();
      this->upgDb[upg][DP::UpTy] = upgrade.GetUpgradeType().GetName();
      this->upgDb[upg][DP::Xws]  = SafeGetXws(upg);
      this->upgDb[upg][DP::Yasb] = std::to_string(SafeGetYasbId(upg));
      if(upgrade.IsDualSided()) {
	upgrade.Flip();
	this->upgDb[upg][DP::Abil2] = std::to_string(upgrade.HasAbility());
	this->upgDb[upg][DP::ArtC2] = SafeGetFfgArtC(upg,1);
	this->upgDb[upg][DP::ArtI2] = SafeGetFfgArtI(upg,1);
	this->upgDb[upg][DP::Ffg2]  = std::to_string(SafeGetFfgId(upg,1));
	this->upgDb[upg][DP::Slot2] = GetStr(upgrade.GetSlots());
	this->upgDb[upg][DP::Text2] = upgrade.GetText().GetCleanText();
	this->upgDb[upg][DP::Titl2] = upgrade.GetTitle();
	this->upgDb[upg][DP::UpTy2] = upgrade.GetUpgradeType().GetName();
      }
    }
  }

  { // quickbuild
    for(const libxwing2::QuickBuild& quickBuild : libxwing2::QuickBuild::GetAllQuickBuilds()) {
      this->qbDb[quickBuild.GetQB()][DP::QuBu] = GetStr(quickBuild.GetThreat(), quickBuild.GetQBPlts());
    }
  }

  this->PopulateDps();
  return {};
}

const ShpDB& LXW2DS::GetShps() { return this->shpDb; }
const PltDB& LXW2DS::GetPlts() { return this->pltDb; }
const UpgDB& LXW2DS::GetUpgs() { return this->upgDb; }
const QBDB&  LXW2DS::GetQBs()  { return this->qbDb;  }

}

#include "strhlp.h"
#include <regex>
#include <dirent.h>

namespace strhlp {

bool StartsWith(const std::string& str, const std::string& prefix) {
  return str.size() >= prefix.size() && 0 == str.compare(0, prefix.size(), prefix);
}

bool EndsWith(const std::string& str, const std::string& suffix) {
  return str.size() >= suffix.size() && 0 == str.compare(str.size()-suffix.size(), suffix.size(), suffix);
}

bool Contains(const std::string& str, const std::string& target) {
  return str.find(target) != std::string::npos;
}
  
bool isDigits(const std::string &s) {
  return s.find_first_not_of("0123456789") == std::string::npos;
}



std::string Trim(std::string s) {
  while ((s.size() > 0) && ((s[s.size()-1] == '\r') || (s[s.size()-1] == '\n'))) {
    s = s.substr(0, s.size()-1);
  }
  if(s.size() == 0) { return s; }
  size_t first = s.find_first_not_of(' ');
  size_t last = s.find_last_not_of(' ');
  if(first == -1 && last == -1) { return ""; } // all spaces
  return s.substr(first, (last-first+1));
}

std::string ReplaceStr(std::string str, std::string remove, std::string add) {
  // doing a std::string.replace() here did not work consistently, probaly due to the multibyte characters
  return regex_replace(str, std::regex(remove), add);
}



std::vector<std::string> Tokenize(std::string s, const char DELIMITER) {
  std::vector<std::string> ret;
  size_t start = s.find_first_not_of(DELIMITER), end=start;
  while (start != std::string::npos){
    end = s.find(DELIMITER, start);
    ret.push_back(Trim(s.substr(start, end-start)));
    start = s.find_first_not_of(DELIMITER, end);
  }
  return ret;
}



  std::vector<std::string> GetFilePaths(std::string path, std::string suffix) {
  std::vector<std::string> ret;
  struct dirent *ent;
  DIR *dir;
  if((dir = opendir(path.c_str())) != NULL) {
    while((ent = readdir(dir)) != NULL) {
      std::string file = ent->d_name;
      if((file == ".") || (file == "..")) { continue; }
      std::string fullPath = std::string(path.c_str()) + file;
      if(ent->d_type == DT_DIR) {
	fullPath += "/";
	for(const std::string& f : GetFilePaths(fullPath, suffix)) {
	  ret.push_back(f);
	}
      }
      else if(ent->d_type == DT_REG) {
	if(file.size() >= suffix.size() && 0 == file.compare(file.size()-suffix.size(), suffix.size(), suffix)) {
	  ret.push_back(fullPath);
	}
      }
    }
    closedir (dir);
  }
  return ret;
}
  
}

#pragma once
#include "datasrc.h"

namespace xdv::ffg {

class FFGDS : public DataSrc {
 public:
  FFGDS();
  virtual std::vector<std::string> Load() override;
private:
  void ParseFile(std::vector<std::string> lines);
};

}

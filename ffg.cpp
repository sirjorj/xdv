#include "ffg.h"
#include "strhlp.h"
#include <codecvt>
#include <iostream>
#include <locale>
#include <regex>
#include <sstream>

#if 0
#define FILECOLOR "\e[0;97m\u001b[45m"
#define LNUMCOLOR "\e[0;97m\u001b[46m"
#define LINECOLOR "\e[0;97m\u001b[44m"
#define LINEERROR "\e[0;97m\u001b[41m"

#define DATAOK    "\e[0;32m"
#define DATAERROR "\e[0;31m"


#define NORMCOLOR  "\e[0;37m"

#define DBG(fmt)       printf(fmt)
#define DBGV(fmt, ...) printf(fmt, __VA_ARGS__)
#else
#define DBG(fmt)
#define DBGV(fmt, ...)
#endif

namespace xdv::ffg {

// regex keys
const std::regex RX_BLANK  (R"(^\x0c? *[\n]?$)");
const std::regex RX_PLTS   (R"(^\x0c? *([a-zA-Z& ]+?) +Pilots[\n]?$)");
const std::regex RX_UPGS   (R"(^\x0c? *([a-zA-Z& ]+?) +Upgrades[\n]?$)");
const std::regex RX_HPLTS  (R"(^\x0c? +Huge +Ships[\n]?$)");
const std::regex RX_HUPGS  (R"(^\x0c? +Huge +Ship +& +Epic +Battles[\n]?$)");
const std::regex RX_VER    (R"(^ *Version +([0-9.]+) \/ ([a-zA-Z0-9 ]+) Update[\n]?$)");
const std::regex RX_PLTHDR (R"(^ *Pilot Name +Subtitle +Cost +Upgrade Bar +HS +Ext +Builder Keywords[\n]?$)");
const std::regex RX_UPGHDR (R"(^ *Upgrade Name +Upgrade Type\(s\) +Cost +Variable Cost\? +Restrictions +HS +Ext[\n]?$)");
const std::regex RX_HPLTHDR(R"(^ *Pilot Name +Faction +Cost +Upgrade Bar +HS +Ext +Builder Keywords[\n]?$)");

class Parser {
public:
  struct Entry {
    std::string name;
    int position;
    int vPosition;
  };

  Parser() { }

  void Init(std::string header) {
    this->entries = Chunkify(header);
  }

  std::vector<std::string> ParseLine(std::string line) {
    std::vector<std::string> ret(this->entries.size());
    std::vector<Entry> entries = Chunkify(line);
    //printf("CHUNKS:\n"); for(const Entry& entry : entries) { std::cout << "  " << entry.position << "("<< entry.vPosition << ")" << " - '" << entry.name << "'" << std::endl; }
    for(const Entry& entry : entries) {
      // find the center of this entry
      int centerPos = (entry.name.size() / 2) + entry.vPosition + 1; // the +1 here fixed a bug - not sure if it introduced other ones though...;
      //printf("entry - name:'%s' pos:%d vpos:%d centerPos:%d\n", entry.name.c_str(), entry.position, entry.vPosition, centerPos);
      for(int i=0; i<this->entries.size(); i++) {
	const Entry& subEntry = this->entries[i];
	//printf("  check if centerPos:%d >= subEntry.vPos:%d\n", centerPos, subEntry.vPosition);
	if(centerPos >= subEntry.vPosition) {
	  //printf("    check if (i:%d >= this->entries.size():%zu) or centerPos:%d < this->etries[i+1].vPosition:%d\n", i, this->entries.size(), centerPos, this->entries[i+1].vPosition);
	  if((i >= this->entries.size()) ||
	     (centerPos < this->entries[i+1].vPosition)) {
	    if(ret[i] != "") { ret[i] += " "; }
	    ret[i] += entry.name;
	    //printf("      DOING THE WIERD STUFF\n");
	    //printf("        i:%d\n", i);
	    //printf("        this->entries.size():%zu\n", this->entries.size());
	    //printf("        centerPos:%d\n", centerPos);
	    //printf("        this->entries[i+1].vPosition:%d\n", this->entries[i+1].vPosition);
	    //printf("        ret:\n");
	    //for(const std::string& s : ret) { printf("    '%s'\n", s.c_str()); }
	    break;
	  }
	}
      }
    }
    //printf("RET\n");
    //for(const std::string& s : ret) {
    //  printf("  %s\n", s.c_str());
    //}
    return ret;
  }

  std::vector<Entry> GetEntries() {
    return this->entries;
  }

private:
  std::vector<Entry> entries;

  // split a line into tokens, split when there is more than 1 consecutive space
  // each token has:
  //   name: the string value of the token itself
  //   position: number of characters into the line
  //   vposition: number of bytes into the line (the unicode symbols are multiple bytes)
  std::vector<Entry> Chunkify(std::string line) {
    std::vector<Entry> ret;
    std::ostringstream curName;
    int curPos=0;
    int curPosOff=0;
    enum class State { Word, Space, Gap };
    State state = State::Gap;
    std::string uLine = line;
    int uOffset=0;

    for(int i=0; i<uLine.size(); i++) {
      char c = uLine[i];

      if(c == ' ') {
	switch(state) {
	case State::Word:
	  state = State::Space;
	  break;
	case State::Space:
	  state = State::Gap;
	  ret.push_back({curName.str(), curPos, curPosOff});
	  //DBGV("  ...'%s' @ %d (%d)\n", curName.str().c_str(), curPosOff, curPos);
	  break;
	case State::Gap:
	  break;
	}
      }
      else if(c == '\n') { }
      else {
	if(state == State::Gap) {
	  curName = std::ostringstream();
	  curPos = i;
	  curPosOff = i-uOffset;
	  state = State::Word;
	  //DBGV("  %d...\n", curPosOff);
	}
	else if(state == State::Space) {
	  state = State::Word;
	  curName << ' ';
	}
	curName << c;
      }
      // handle unicode chars
      // 110xxxxx
      if     (((uint8_t)c>>5) ==  6) { uOffset += 1; }
      // 1110xxxx
      else if(((uint8_t)c>>4) == 14) { uOffset += 2; }
      // 11110xxx
      else if(((uint8_t)c>>3) == 30) { uOffset += 3; }
    }
    if(curName.str().size()) {
      ret.push_back({curName.str(), curPos, curPosOff});
      //DBGV("  ...'%s' @ %d (%d)\n", curName.str().c_str(), curPosOff, curPos);
    }
    return ret;
  }

};



// helpers
static std::vector<std::string> GetPdfText(std::string file) {
  std::string command = "pdftotext -fixed 3 -layout '" + file + "' -";
  char* line = 0;
  size_t len = 0;
  FILE *f = popen(command.c_str(), "r");
  std::vector<std::string> ret;
  while(getline(&line, &len, f) != -1) {
    ret.push_back(line);
  }
  pclose(f);
  if(line) { free(line); }
  return ret;
}

// note: the actual code points in the official font are:
//   f2040-f206f
//   f2220-f222f
//   f2400-f2409
//   f2500
const std::map<uint32_t,libxwing2::UpT> UpTMap = {
  { 0xF3B28194, libxwing2::UpT::Astro     },
  { 0xF3B2818D, libxwing2::UpT::Cannon    },
  { 0xf3b28195, libxwing2::UpT::Cargo     },
  { 0xF3B29085, libxwing2::UpT::Command   },
  { 0xF3B288A1, libxwing2::UpT::Config    },
  { 0xF3B28192, libxwing2::UpT::Crew      },
  { 0xF3B288A2, libxwing2::UpT::Force     },
  { 0xF3B288A6, libxwing2::UpT::Gunner    },
  { 0xf3b28191, libxwing2::UpT::Hardpoint },
  { 0xf3b29480, libxwing2::UpT::Hyperdr   },
  { 0xF3B28198, libxwing2::UpT::Illicit   },
  { 0xF3B28190, libxwing2::UpT::Missile   },
  { 0xF3B28199, libxwing2::UpT::Mod       },
  { 0xF3B28196, libxwing2::UpT::Payload   },
  { 0xF3B2818C, libxwing2::UpT::Sensor    },
  { 0xF3B29082, libxwing2::UpT::TactRel   },
  { 0xF3B2818B, libxwing2::UpT::Talent    },
  { 0xf3b28193, libxwing2::UpT::Team      },
  { 0xF3B2819B, libxwing2::UpT::Tech      },
  { 0xF3B2819A, libxwing2::UpT::Title     },
  { 0xF3B2818F, libxwing2::UpT::Torpedo   },
  { 0xF3B2818E, libxwing2::UpT::Turret    },
};

static std::optional<libxwing2::UpT> GetUpT(unsigned int i) {
  if(UpTMap.find(i) == UpTMap.end()) {
    //printf("------------------------------------------------------UpT NOT FOUND!\n");
    return std::nullopt;
  } else {
    return UpTMap.at(i);
  }
}

static libxwing2::UpgradeBar GetUpgradeBar(std::string s) {
  libxwing2::UpgradeBar upgs;
  if((s.size() % 4) == 0) {
    for(int i=0; i<s.size()/4; i++) {
      unsigned int c = 0;
      c |= (s[(i*4)+0] & 0xFF); c <<= 8;
      c |= (s[(i*4)+1] & 0xFF); c <<= 8;
      c |= (s[(i*4)+2] & 0xFF); c <<= 8;
      c |= (s[(i*4)+3] & 0xFF);
      std::optional<libxwing2::UpT> upt = GetUpT(c);
      if(upt) {
	upgs.push_back(*upt);
      }
      else {
	DBGV("UNKNOWN UPG '%02x'\n", c);
      }
    }
  }
  return upgs;
}

static libxwing2::KeywordList GetKeywords(std::string s) {
  if(s.size() == 0) {
    return {};
  } else {
    libxwing2::KeywordList ret;
    std::vector<std::string> tokens = strhlp::Tokenize(s, ',');
    for(const std::string& k : tokens) {
      bool match = false;
      for(const libxwing2::Keyword& keyword : libxwing2::Keyword::GetAllKeywords()) {
	if(keyword.GetName() == k) {
	  match = true;
	  ret.push_back(keyword.GetKWd());
	  break;
	}
      }
      if(!match) {
	printf("KEYWORD '%s' NOT RECOGNIZED\n", k.c_str());
      }
    }
    return ret;
  }
}

static std::vector<libxwing2::UpT> GetSlots(std::string s) {
  std::vector<libxwing2::UpT> upgs;
  if((s.size() % 4) == 0) {
    for(int i=0; i<s.size()/4; i++) {
      unsigned int c = 0;
      c |= (s[(i*4)+0] & 0xFF); c <<= 8;
      c |= (s[(i*4)+1] & 0xFF); c <<= 8;
      c |= (s[(i*4)+2] & 0xFF); c <<= 8;
      c |= (s[(i*4)+3] & 0xFF);
      std::optional<libxwing2::UpT> upt = GetUpT(c);
      if(upt) {
	upgs.push_back(*upt);
      }
      else {
	DBGV("UNKNOWN UPG '%02x'\n", c);
      }
    }
  }
  return upgs;
}

std::vector<std::pair<int, std::string>> GetPartials(std::string p) {
  enum State { Searching, Space, Reading };
  std::vector<std::pair<int, std::string>> ret;
  size_t start=0, size=0;
  State s = Searching;
  for(int i=0; i < p.size(); i++) {
    char c = p[i];
    switch(s) {
    case Searching:
      if(c != ' ') {
	s = Reading;
	start = i;
	size=0;
      }
      break;
    case Space:
      if(c == ' ') {
	s = Searching;
	std::string dbg = std::string(&p[start], size);
	ret.push_back({start,dbg});
      } else {
	size++;
	s = Reading;
      }
      break;
    case Reading:
      size++;
      if(c == ' ') {
	s = Space;
      }
      break;
    }
  }
  if(s == Reading) {
    size++;
    ret.push_back({start,std::string(&p[start], size)});
  }

  DBG("partials:\n");
  for(const std::pair<const int,std::string> p : ret) {
    DBGV("  %3d) %s\n", p.first, p.second.c_str());
  }
  return ret;
}

static std::string CleanupFaction(std::string ffg) {
  if(false) {}
  else if(ffg == "Rebel")                                { return "Rebel Alliance"; }
  else if(ffg == "Imperial")                             { return "Galactic Empire"; }
  else if(ffg == "Scum                 &      Villainy") { return "Scum and Villainy"; }
  else if(ffg == "Scum & Villainy")                      { return "Scum and Villainy"; }
  else if(ffg == "Resistance")                           { return "Resistance"; }
  else if(ffg == "First             Order")              { return "First Order"; }
  else if(ffg == "Republic")                             { return "Galactic Republic"; }
  else if(ffg == "Separatist")                           { return "Separatist Alliance"; }
  else                                                   { return ffg; }
}

static std::string CleanupName(std::string ffg) {
  std::string n = ffg;
  // remove limited dots
  n = strhlp::ReplaceStr(n, { (char)0xE2, (char)0x80, (char)0xA2 }, "");
  // the doc uses different double quotes characters (“Wampa”)
  n = strhlp::ReplaceStr(n, "“", "\"");
  n = strhlp::ReplaceStr(n, "”", "\"");
  // ...and single quotes
  n = strhlp::ReplaceStr(n, "’", "'");
  // that wierd symbol in "D'Qar"
  n = strhlp::ReplaceStr(n, "’", "'");
  // some ships may have ' (continued)' as a suffix if it spanned pages
  n = strhlp::ReplaceStr(n, " \\(continued\\)", "");
  n = strhlp::ReplaceStr(n, " \\(Continued\\)", "");
  // some have a space before the '-class'
  n = strhlp::ReplaceStr(n, " -class", "-class");
  // name differences
  if(false) {}
  // ships
  else if(n == "CR-90 Corvette")                     { n = "CR90 Corellian Corvette"; }
  else if(n == "Scavenged YT-1300 Light Freighter")  { n = "Scavenged YT-1300"; }
  else if(n == "Scurrg H-6 Bomber")                  { n = "Scurrg H-6 bomber"; }
  else if(n == "TIE/fo fighter")                     { n = "TIE/fo Fighter"; }
  else if(n == "TIE/ln fighter")                     { n = "TIE/ln Fighter"; }
  else if(n == "TIE/sf fighter")                     { n = "TIE/sf Fighter"; }
  else if(n == "Upsilon-class shuttle")              { n = "Upsilon-class command shuttle"; }
  else if(n == "Xi-class shuttle")                   { n = "Xi-class Light Shuttle"; }

  // pilots
  else if(n == "Karé Kun")                          { n = "Kare Kun"; }
  else if(n == "Nimi Chereen")                      { n = "Nimi Chireen"; }

  // upgrades
  else if(n == "Agent Terex / Agent Terex (Cyborg)")                                     { n = "Agent Terex"; }
  else if(n == "Bounty (Paid) / Bounty (Hired)")                                         { n = "Bounty"; }
  else if(n == "C1-10P / C1-10P (Erratic)")                                              { n = "C1-10P"; }
  else if(n == "Chancellor Palpatine / Darth Sidious")                                   { n = "Palpatine/Sidious"; }
  else if(n == "Commander Malarus / Commander Malarus (Perfected)")                      { n = "Commander Malarus"; }
  else if(n == "Grappling Struts (Open) / Grappling Struts (Closed)")                    { n = "Grappling Struts"; }
  else if(n == "In It For Your Rebellion / In It For The Money")                         { n = "In It For..."; }
  else if(n == "Integrated S-foils (Open) / Integrated S-foils (Closed)")                { n = "Integrated S-foils"; }
  else if(n == "Ion Cannon Battery / Ion Cannon Battery (Offline)")                      { n = "Ion Cannon Battery"; }
  else if(n == "L3–37 / L3-37's Programming")                                            { n = "L3-37"; }
  else if(n == "Landing Struts Open / Landing Struts (Closed)")                          { n = "Landing Struts"; }
  else if(n == "Pivot Wing (Open) / Pivot Wing (Closed)")                                { n = "Pivot Wing"; }
  else if(n == "Proud Tradition / False Tradition")                                      { n = "Proud Tradition"; }
  else if(n == "Repulsorlift Stabilizers (Active) / Repulsorlift Stabilizers (Offline)") { n = "Repulsorlift Stabilizers"; }  
  else if(n == "Servomotor S-foils (Open) / Servomotor S-foils (Closed)")                { n = "Servomotor S-foils"; }  
  else if(n == "Stabilized S-foils (Open) / Stabilized S-foils (Closed)")                { n = "Stabilized S-foils"; }  

  else if(n == "Ion Cannon Battery / Ion Cannon Battery (Offline)")                      { n = "Ion Cannon Battery"; }
  else if(n == "Ordnance Tubes / Ordnance Tubes (Offline)")                              { n = "Ordnance Tubes"; printf("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"); }
  else if(n == "Point-Defense Battery / Point-Defense Battery (Offline)")                { n = "Point-Defense Battery"; }
  else if(n == "Targeting Battery / Targeting Battery (Offline)")                        { n = "Targeting Battery"; }
  else if(n == "Turbolaser Battery / Turbolaser Batter (Offline)")                       { n = "Turbolaser Battery"; }
  
  return n;
}

static std::vector<libxwing2::UpT> CleanupType(std::string s) {
  s = strhlp::Trim(s);
  std::smatch sm;
  //printf("trying to match '%s'\n", s.c_str());
  //printf("hex is: "); for(char c : s) { printf(" %02x", (uint8_t)c); } printf("\n");
  if(std::regex_match(s, sm, std::regex(R"(^.+?\((.+)\)$)"))) {
    std::string cap =  sm[1];
    if(cap[cap.size()-1] == ' ') {
      cap = cap.substr(0, cap.size()-1);
    }
    //printf("cap size is %d\n", cap.size());
    //printf("CleanupType(): %s -> %s\n", s.c_str(), cap.c_str());
    return GetSlots(cap);
  } else {
    //printf("CleanupType() - no regex match\n");
    return {};
  }
}

static bool GetBool(std::string b) {
  if(false) {}
  else if(b == "No")  { return false; }
  else if(b == "Yes") { return true; }
  else                { return false; }
}

static int GetLimited(std::string s) {
  int ret = 0;
  std::string limStr = { (char)0xE2, (char)0x80, (char)0xA2 };
  std::string::size_type pos = 0;
  while ((pos = s.find(limStr, pos )) != std::string::npos) {
    ret++;
    pos += limStr.length();
  }
  return ret;
}

static std::optional<libxwing2::Plt> FindPlt(std::string f, std::string s, std::string n, std::string t) {
  for(const libxwing2::Pilot& pilot : libxwing2::Pilot::GetAllPilots()) {
    if((pilot.GetFaction().GetName() == f) &&
       (pilot.GetShip().GetName() == s) &&
       (pilot.GetName() == n) &&
       (pilot.GetSubtitle() == t)) {
      return pilot.GetPlt();
    }
  }
  return {};
}

static std::optional<libxwing2::Upg> FindUpg(std::vector<libxwing2::UpT> s, std::string n, std::string f) {
  libxwing2::Fac fac = libxwing2::Fac::All;

  for(const libxwing2::Faction& faction : libxwing2::Faction::GetAllFactions()) {
    if(faction.GetName() == f) {
      fac = faction.GetFac();
      break;
    }
  }

  for(const libxwing2::Upgrade& upgrade : libxwing2::Upgrade::GetAllUpgrades()) {
    if(upgrade.GetName() == n) {
      //printf("--- NAME '%s'\n", n.c_str());
      //printf("    slots:"); for(libxwing2::UpT t : s) { printf("[%s]", libxwing2::UpgradeType::GetUpgradeType(t).GetName().c_str()); } printf("\n");
      if(upgrade.GetSlots() == s) {
	//printf(" --- SLOTS\n");
	if((upgrade.GetRestriction().GetAllowedFactions() & fac) != libxwing2::Fac::None) {
	  //printf("  --- HOORAY!\n");
	  return upgrade.GetUpg();
	}
      }
    }
  }
  return {};
}

static libxwing2::Cost GetCost(std::string c, std::string v) {
  if(c != "*") { return libxwing2::Cost::ByCost(std::stoi(c)); }
  else {
    std::vector<std::string> toks = strhlp::Tokenize(v, '/');

    if(toks[0][0] == 'A' && toks[0][1] == 'g') {
      std::vector<uint16_t> n;
      for(const std::string &tok : toks) {
	std::smatch sm;
	if(std::regex_match(tok, sm, std::regex(R"(^Ag\d: (\d+)$)"))) {
	  n.push_back(std::stoi(sm[1]));
	}
      }
      return libxwing2::Cost::ByAgi(n[0],n[1],n[2],n[3]);
    }

    else if(toks[0][0] == 'S' && toks[0][1] == 'm') {
      std::vector<uint16_t> n;
      for(const std::string &tok : toks) {
      	std::smatch sm;
      	if(std::regex_match(tok, sm, std::regex(R"(^.+?: (\d+)$)"))) {
      	  n.push_back(std::stoi(sm[1]));
	}
      }
      if     (n.size() == 3) { return libxwing2::Cost::ByBase(n[0],n[1],n[2]); }
      else if(n.size() == 4) { return libxwing2::Cost::ByBase(n[0],n[1],n[2],n[3]); }
    }

    else if(toks[0][0] == 'I') {
      std::vector<uint16_t> n;
      for(const std::string &tok : toks) {
	std::smatch sm;
	if(std::regex_match(tok, sm, std::regex(R"(^I\d: (\d+)$)"))) {
	  n.push_back(std::stoi(sm[1]));
	}
      }
      if     (n.size() == 7) { return libxwing2::Cost::ByInit(n[0],n[1],n[2],n[3],n[4],n[5],n[6]); }
      else if(n.size() == 9) { return libxwing2::Cost::ByInit(n[0],n[1],n[2],n[3],n[4],n[5],n[6],n[7],n[8]); }
    }

    return libxwing2::Cost::ByCost(0);
  }
}


// class
FFGDS::FFGDS()
  : DataSrc("FFG") {
}



void FFGDS::ParseFile(std::vector<std::string> lines) {
  enum class Expected {
    Unknown,
    Pilot,
    Upgrade,
    Huge
  };

  Expected expected = Expected::Unknown;
  std::string curFaction;
  std::string curShip;
  Parser parser;
  std::map<int,std::ostringstream> partials;

  // for each line...
  for(int i=0; i<lines.size(); i++) {
    std::smatch sm;

    std::string line = lines[i];
    DBGV("%s%03d:%s%s%s\n", LNUMCOLOR, i+1, LINECOLOR, line.substr(0, line.size()-1).c_str(), NORMCOLOR);

    // empty line
    if(std::regex_match(line, sm, RX_BLANK)) {
      DBG("Empty line\n");
    }
    // "*FACTION* Pilots"
    else if(std::regex_match(line, sm, RX_PLTS)) {
      std::string f = sm[1];
      DBGV("Pilots - Faction:'%s'\n", f.c_str());
      curFaction = f;
    }
    // "*FACTION* Upgrades"
    else if(std::regex_match(line, sm, RX_UPGS)) {
      std::string f = sm[1];
      DBGV("Upgrades - Faction:'%s'\n", f.c_str());
      curFaction = f;
    }
    // Huge Ships
    else if(std::regex_match(line, sm, RX_HPLTS)) {
      DBG("Huge Ships\n");
    }
    // Huge Ship & Epic Battles
    else if(std::regex_match(line, sm, RX_HUPGS)) {
      DBG("Huge Ship & Epic Battles\n");
    }
    // version
    else if(std::regex_match(line, sm, RX_VER)) {
      std::string v = sm[1];
      std::string d = sm[2];
      DBGV("Version:'%s' Description:'%s'\n", v.c_str(), d.c_str());
      if(this->version == "") {
	this->version = v;
      } else if(this->version != v) {
	printf("VERSION DIFFERENCE: %s vs %s\n", this->version.c_str(), v.c_str());
      }
    }
    // pilot header
    else if(std::regex_match(line, sm, RX_PLTHDR)) {
      expected = Expected::Pilot;
      DBG("Pilot Header\n");
      parser.Init(line);
      std::vector<Parser::Entry> entries = parser.GetEntries();
      DBG("<Entries>\n");
      //for(const Parser::Entry& entry : entries) { DBGV("  %d: '%s'\n", entry.position, entry.name.c_str()); }
      DBG("</Entries>\n");
    }
    // upgrade header
    else if(std::regex_match(line, sm, RX_UPGHDR)) {
      expected = Expected::Upgrade;
      DBG("Upgrade Header\n");
      parser.Init(line);
      std::vector<Parser::Entry> entries = parser.GetEntries();
      DBG("<Entries>\n");
      //for(const Parser::Entry& entry : entries) { DBGV("  %d: '%s'\n", entry.position, entry.name.c_str()); }
      DBG("</Entries>\n");
    }
    // huge pilot header
    else if(std::regex_match(line, sm, RX_HPLTHDR)) {
      expected = Expected::Huge;
      DBG("Huge Ship Header\n");
      parser.Init(line);
      std::vector<Parser::Entry> entries = parser.GetEntries();
      DBG("<Entries>\n");
      //for(const Parser::Entry& entry : entries) { DBGV("  %d: '%s'\n", entry.position, entry.name.c_str()); }
      DBG("</Entries>\n");
    }

    // *** after pilot header ***
    else if(expected == Expected::Pilot) {
      std::vector<std::string> x = parser.ParseLine(line);
      /*
	std::cout << "<Entries>" << std::endl;
	std::cout << "name: " << x[0] << std::endl;
	std::cout << "subt: " << x[1] << std::endl;
	std::cout << "cost: " << x[2] << std::endl;
	std::cout << "ubar: " << x[3] << std::endl;
	std::cout << "  hs: " << x[4] << std::endl;
	std::cout << " ext: " << x[5] << std::endl;
	std::cout << "keyw: " << x[6] << std::endl;
	std::cout << "</Entries>" << std::endl;
      */
      for(int i=0; i<7; i++) {
	if(partials[i].str() != "") {
	  partials[i] << " ";
	}
	partials[i] << x[i];
      }

      // see if the first token is a ship
      bool newShip = false;
      std::string tmpShip = CleanupName(x[0]);
      for(const libxwing2::Ship& ship : libxwing2::Ship::GetAllShips()) {
	if(ship.GetName() == tmpShip) {
	  curShip = tmpShip;
	  newShip = true;
	}
      }
      if(newShip) {
	DBGV("Ship:'%s'\n", curShip.c_str());
	partials.clear();
	continue;
      }

      // if there is a value for 'ext', assume we have all there is
      if(x[5] != "") {
	std::string strName = partials[0].str();
	std::string strSubt = partials[1].str();
	std::string strCost = partials[2].str();
	std::string strUBar = partials[3].str();
	std::string strHS   = partials[4].str();
	std::string strExt  = partials[5].str();
	std::string strKeyw = partials[6].str();

	DBGV("Pilot:'%s' Subtitle:'%s' Cost:'%s' UpgradeBar:'%s' HS:'%s' Ext:'%s' Keywords:'%s'\n",
	     strName.c_str(), strSubt.c_str(), strCost.c_str(), strUBar.c_str(),
	     strHS.c_str(), strExt.c_str(), strKeyw.c_str());

	std::string cFaction = CleanupFaction(curFaction);
	std::string cShip    = CleanupName(curShip);
	std::string cPilot   = CleanupName(strName);
	std::string cTitle   = CleanupName(strSubt);

	std::optional<libxwing2::Plt> oplt = FindPlt(cFaction, cShip, cPilot, cTitle);
	if(!oplt) {
	  DBGV("%sPilot not found (%s / %s / %s / %s)%s\n",
	       DATAERROR, cFaction.c_str(), cShip.c_str(), cPilot.c_str(), cTitle.c_str(), NORMCOLOR);
	} else {
	  DBGV("%sOK%s\n", DATAOK, NORMCOLOR);
	  this->pltDb[*oplt][DP::Cost] = strCost;
	  this->pltDb[*oplt][DP::Hype] = std::to_string(GetBool(strHS));
	  this->pltDb[*oplt][DP::Limi] = std::to_string(GetLimited(strName));
	  this->pltDb[*oplt][DP::UBar] = GetStr(GetUpgradeBar(strUBar));
	  this->pltDb[*oplt][DP::Kwds] = GetStr(GetKeywords(strKeyw));
	}
	partials.clear();
      }
      else {
	DBGV("%sNo MATCHES!%s\n", LINEERROR, NORMCOLOR);
      }
    }

    // *** after upgrade header ***
    // upgrade data
    else if(expected == Expected::Upgrade) {
      std::vector<std::string> x = parser.ParseLine(line);
      /*
	std::cout << "<Entries>" << std::endl;
	std::cout << "name: " << x[0] << std::endl;
	std::cout << "type: " << x[1] << std::endl;
	std::cout << "cost: " << x[2] << std::endl;
	std::cout << "varc: " << x[3] << std::endl;
	std::cout << "rest: " << x[4] << std::endl;
	std::cout << "  hs: " << x[5] << std::endl;
	std::cout << " ext: " << x[6] << std::endl;
	std::cout << "</Entries>" << std::endl;
      */
      for(int i=0; i<7; i++) {
	if(partials[i].str() != "") {
	  partials[i] << " ";
	}
	partials[i] << x[i];
      }

      // if there is a value for 'ext', assume we have all there is
      if(partials[6].str() != "") {
	std::string strName = partials[0].str();
	std::string strType = partials[1].str();
	std::string strCost = partials[2].str();
	std::string strVarC = partials[3].str();
	std::string strRest = partials[4].str();
	std::string strHS   = partials[5].str();
	std::string strExt  = partials[6].str();

	DBGV("Upgrade:'%s' Type:'%s' Cost:'%s' VarCost:'%s' Restrictions:'%s' HS:'%s' Ext:'%s'\n",
	     strName.c_str(), strType.c_str(), strCost.c_str(), strVarC.c_str(),
	     strRest.c_str(), strHS.c_str(), strExt.c_str());
	std::string name = CleanupName(strName);

	//printf("NAME IS '%s'\n", name.c_str());

	if(strType.find('/') != std::string::npos) {
	  strType = strType.substr(0, strType.find('/')-1);
	}
	std::vector<libxwing2::UpT> slots = CleanupType(strType);



	//printf("SLOTS:"); for(libxwing2::UpT t : slots) { printf("[%s]", libxwing2::UpgradeType::GetUpgradeType(t).GetName().c_str()); } printf("\n");


	

	
	libxwing2::Cost cost = GetCost(strCost,strVarC);
	//printf("SLOTS:"); for(const libxwing2::UpT upt : slots) { printf(" %s", libxwing2::UpgradeType::GetUpgradeType(upt).GetName().c_str()); } printf("\n");
	std::optional<libxwing2::Upg> oupg = FindUpg(slots, name, CleanupFaction(curFaction));
	if(!oupg) {
	  DBGV("%sUpgrade not found%s\n", DATAERROR, NORMCOLOR);
	} else {
	  DBGV("%sOK%s\n", DATAOK, NORMCOLOR);
	  this->upgDb[*oupg][DP::Cost] = GetStr(cost);
	  this->upgDb[*oupg][DP::Hype] = std::to_string(GetBool(strHS));
	}

	partials.clear();
      }
    }

    // *** after huge ship ***
    else if(expected == Expected::Huge) {
      std::vector<std::string> x = parser.ParseLine(line);
      /*
	std::cout << "<Entries>" << std::endl;
	std::cout << "name: " << x[0] << std::endl;
	std::cout << "fact: " << x[1] << std::endl;
	std::cout << "cost: " << x[2] << std::endl;
	std::cout << "ubar: " << x[3] << std::endl;
	std::cout << "  hs: " << x[4] << std::endl;
	std::cout << " ext: " << x[5] << std::endl;
	std::cout << "keyw: " << x[6] << std::endl;
	std::cout << "</Entries>" << std::endl;
      */
      for(int i=0; i<7; i++) {
	if(partials[i].str() != "") {
	  partials[i] << " ";
	}
	partials[i] << x[i];
      }

      // see if the first token is a ship
      bool newShip = false;
      std::string tmpShip = CleanupName(x[0]);
      for(const libxwing2::Ship& ship : libxwing2::Ship::GetAllShips()) {
	if(ship.GetName() == tmpShip) {
	  curShip = tmpShip;
	  newShip = true;
	}
      }
      if(newShip) {
	DBGV("Ship:'%s'\n", curShip.c_str());
	partials.clear();
	continue;
      }

      // if there is a value for 'ext', assume we have all there is
      if(x[5] != "") {
	std::string strName = partials[0].str();
	std::string strFact = partials[1].str();
	std::string strCost = partials[2].str();
	std::string strUBar = partials[3].str();
	std::string strHS   = partials[4].str();
	std::string strExt  = partials[5].str();
	std::string strKeyw = partials[6].str();

	DBGV("Pilot:'%s' Faction:'%s' Cost:'%s' UpgradeBar:'%s' HS:'%s' Ext:'%s' Keywords:'%s'\n",
	     strName.c_str(), strFact.c_str(), strCost.c_str(), strUBar.c_str(),
	     strHS.c_str(), strExt.c_str(), strKeyw.c_str());

	std::string cFaction = CleanupFaction(strFact);
	std::string cShip    = CleanupName(curShip);
	std::string cPilot   = CleanupName(strName);

	std::optional<libxwing2::Plt> oplt = FindPlt(cFaction, cShip, cPilot, "");
	if(!oplt) {
	  DBGV("%sHuge pilot not found (%s / %s / %s / %s)%s\n",
	       DATAERROR, cFaction.c_str(), cShip.c_str(), cPilot.c_str(), "", NORMCOLOR);
	} else {
	  DBGV("%sOK%s\n", DATAOK, NORMCOLOR);
	  this->pltDb[*oplt][DP::Cost] = strCost;
	  this->pltDb[*oplt][DP::Hype] = std::to_string(GetBool(strHS));
	  this->pltDb[*oplt][DP::Limi] = std::to_string(GetLimited(strName));
	  this->pltDb[*oplt][DP::UBar] = GetStr(GetUpgradeBar(strUBar));
	}

	partials.clear();
      }
      else {
	DBGV("%sNo MATCHES!%s\n", LINEERROR, NORMCOLOR);
      }
    }

    // ???
    else {
      DBGV("%sNo MATCHES!%s\n", LINEERROR, NORMCOLOR);
    }
  }
}



std::vector<std::string> FFGDS::Load() {
  std::vector<std::string> ret;
#if 0
  std::vector<std::string> lines = {

  };
  this->ParseFile(lines);
#else
  std::vector<std::string> files = strhlp::GetFilePaths("./ffg/", ".pdf");
  for(const std::string& filename : files) {
    DBGV("%sFILE [%s]%s\n", FILECOLOR, filename.c_str(), NORMCOLOR);
    std::vector<std::string> lines = GetPdfText(filename);
    this->ParseFile(lines);
  }
#endif



  this->PopulateDps();
  return ret;
}

}
